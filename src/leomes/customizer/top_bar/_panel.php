<?php
$panel    = 'top_bar';
$priority = 1;

Insight_Kirki::add_section( 'top_bar', array(
	'title'    => esc_html__( 'General', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Insight_Kirki::add_section( 'top_bar_style_01', array(
	'title'    => esc_html__( 'Top Bar Style 01', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
