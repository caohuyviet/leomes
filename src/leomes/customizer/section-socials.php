<?php
$section  = 'socials';
$priority = 1;
$prefix   = 'social_';

Insight_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'social_link_target',
	'label'    => esc_html__( 'Open link in a new tab.', 'leomes' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '1',
	'choices'  => array(
		'0' => esc_html__( 'No', 'leomes' ),
		'1' => esc_html__( 'Yes', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'      => 'repeater',
	'settings'  => 'social_link',
	'section'   => $section,
	'priority'  => $priority ++,
	'choices'   => array(
		'labels' => array(
			'add-new-row' => esc_html__( 'Add new social network', 'leomes' ),
		),
	),
	'row_label' => array(
		'type'  => 'field',
		'field' => 'tooltip',
	),
	'default'   => array(
		array(
			'tooltip'    => esc_html__( 'Twitter', 'leomes' ),
			'icon_class' => 'ion-social-twitter',
			'link_url'   => 'https://twitter.com',
		),
		array(
			'tooltip'    => esc_html__( 'Facebook', 'leomes' ),
			'icon_class' => 'ion-social-facebook',
			'link_url'   => 'https://facebook.com',
		),
		array(
			'tooltip'    => esc_html__( 'Instagram', 'leomes' ),
			'icon_class' => 'ion-social-instagram',
			'link_url'   => 'https://www.instagram.com',
		),
		array(
			'tooltip'    => esc_html__( 'Linkedin', 'leomes' ),
			'icon_class' => 'ion-social-linkedin',
			'link_url'   => 'https://www.linkedin.com',
		),
	),
	'fields'    => array(
		'tooltip'    => array(
			'type'        => 'text',
			'label'       => esc_html__( 'Tooltip', 'leomes' ),
			'description' => esc_html__( 'Enter your hint text for your icon', 'leomes' ),
			'default'     => '',
		),
		'icon_class' => array(
			'type'        => 'text',
			'label'       => esc_html__( 'Icon Class', 'leomes' ),
			'description' => esc_html__( 'This will be the icon class for your link', 'leomes' ),
			'default'     => '',
		),
		'link_url'   => array(
			'type'        => 'text',
			'label'       => esc_html__( 'Link URL', 'leomes' ),
			'description' => esc_html__( 'This will be the link URL', 'leomes' ),
			'default'     => '',
		),
	),
) );
