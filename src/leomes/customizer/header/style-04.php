<?php
$section  = 'header_style_04';
$priority = 1;
$prefix   = 'header_style_04_';

Insight_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'overlay',
	'label'    => esc_html__( 'Header Overlay', 'leomes' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '1',
	'choices'  => array(
		'0' => esc_html__( 'No', 'leomes' ),
		'1' => esc_html__( 'Yes', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'logo',
	'label'    => esc_html__( 'Logo', 'leomes' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'light',
	'choices'  => array(
		'light' => esc_html__( 'Light', 'leomes' ),
		'dark'  => esc_html__( 'Dark', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => $prefix . 'search_enable',
	'label'       => esc_html__( 'Search Button', 'leomes' ),
	'description' => esc_html__( 'Controls the display of search button in the header.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Hide', 'leomes' ),
		'1' => esc_html__( 'Show', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => $prefix . 'cart_enable',
	'label'       => esc_html__( 'Mini Cart', 'leomes' ),
	'description' => esc_html__( 'Controls the display of mini cart in the header', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0'             => esc_html__( 'Hide', 'leomes' ),
		'1'             => esc_html__( 'Show', 'leomes' ),
		'hide_on_empty' => esc_html__( 'Hide On Empty', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'border_color',
	'label'       => esc_html__( 'Border Bottom Color', 'leomes' ),
	'description' => esc_html__( 'Controls the border bottom color.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '',
	'output'      => array(
		array(
			'element'  => '.header-04 .page-header-inner',
			'property' => 'border-bottom-color',
		),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => $prefix . 'box_shadow',
	'label'       => esc_html__( 'Box Shadow', 'leomes' ),
	'description' => esc_html__( 'Input box shadow for header. For ex: 0 0 5px #ccc', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'output'      => array(
		array(
			'element'  => '.header-04 .page-header-inner',
			'property' => 'box-shadow',
		),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'background',
	'settings'    => $prefix . 'background',
	'label'       => esc_html__( 'Background', 'leomes' ),
	'description' => esc_html__( 'Controls the background of header.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => array(
		'background-color'      => 'rgba(0, 0, 0, 0)',
		'background-image'      => '',
		'background-repeat'     => 'no-repeat',
		'background-size'       => 'cover',
		'background-attachment' => 'scroll',
		'background-position'   => 'center center',
	),
	'output'      => array(
		array(
			'element' => '.header-04 .page-header-inner',
		),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'header_icon_color',
	'label'       => esc_html__( 'Icon Color', 'leomes' ),
	'description' => esc_html__( 'Controls the color of icons on header.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => Insight::THIRD_COLOR,
	'output'      => array(
		array(
			'element'  => '
				.header-04 .page-open-mobile-menu i,
				.header-04 .page-open-main-menu i,
				.header-04 .popup-search-wrap i,
				.header-04 .mini-cart .mini-cart-icon,
				.header-04 .header-social-networks a',
			'property' => 'color',
		),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'header_icon_hover_color',
	'label'       => esc_html__( 'Icon Hover Color', 'leomes' ),
	'description' => esc_html__( 'Controls the color when hover of icons on header.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#fff',
	'output'      => array(
		array(
			'element'  => '
				.header-04 .page-open-mobile-menu:hover i,
				.header-04 .page-open-main-menu:hover i,
				.header-04 .popup-search-wrap:hover i,
				.header-04 .mini-cart .mini-cart-icon:hover,
				.header-04 .header-social-networks a:hover',
			'property' => 'color',
		),
	),
) );

/*--------------------------------------------------------------
# Navigation
--------------------------------------------------------------*/

Insight_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Main Menu Level 1', 'leomes' ) . '</div>',
) );

Insight_Kirki::add_field( 'theme', array(
	'type'      => 'spacing',
	'settings'  => $prefix . 'navigation_margin',
	'label'     => esc_html__( 'Menu Margin', 'leomes' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => array(
		'top'    => '0px',
		'bottom' => '0px',
		'left'   => '0px',
		'right'  => '20px',
	),
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => array(
				'.desktop-menu .header-04 .menu__container',
			),
			'property' => 'margin',
		),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'      => 'spacing',
	'settings'  => $prefix . 'navigation_item_padding',
	'label'     => esc_html__( 'Item Padding', 'leomes' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => array(
		'top'    => '60px',
		'bottom' => '60px',
		'left'   => '14px',
		'right'  => '14px',
	),
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => array(
				'.desktop-menu .header-04 .menu--primary .menu__container > li > a',
			),
			'property' => 'padding',
		),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'      => 'spacing',
	'settings'  => $prefix . 'navigation_item_margin',
	'label'     => esc_html__( 'Item Margin', 'leomes' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => array(
		'top'    => '0px',
		'bottom' => '0px',
		'left'   => '0px',
		'right'  => '0px',
	),
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => array(
				'.desktop-menu .header-04  .menu--primary .menu__container > li',
			),
			'property' => 'margin',
		),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'kirki_typography',
	'settings'    => $prefix . 'navigation_typography',
	'label'       => esc_html__( 'Typography', 'leomes' ),
	'description' => esc_html__( 'These settings control the typography for menu items.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => array(
		'font-family'    => Insight::PRIMARY_FONT,
		'variant'        => '700',
		'line-height'    => '1.2',
		'letter-spacing' => '0em',
		'subsets'        => array( 'latin-ext' ),
		'text-transform' => 'none',
	),
	'output'      => array(
		array(
			'element' => '.header-04 .menu--primary a',
		),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'slider',
	'settings'    => $prefix . 'navigation_item_font_size',
	'label'       => esc_html__( 'Font Size', 'leomes' ),
	'description' => esc_html__( 'Controls the font size for main menu items.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 16,
	'transport'   => 'auto',
	'choices'     => array(
		'min'  => 10,
		'max'  => 50,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element'  => '.header-04 .menu--primary a',
			'property' => 'font-size',
			'units'    => 'px',
		),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'color',
	'settings'    => $prefix . 'navigation_link_color',
	'label'       => esc_html__( 'Color', 'leomes' ),
	'description' => esc_html__( 'Controls the color for main menu items.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => Insight::THIRD_COLOR,
	'output'      => array(
		array(
			'element'  => '.header-04 .menu--primary a',
			'property' => 'color',
		),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'color',
	'settings'    => $prefix . 'navigation_link_hover_color',
	'label'       => esc_html__( 'Hover Color', 'leomes' ),
	'description' => esc_html__( 'Controls the color when hover for main menu items.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#fff',
	'output'      => array(
		array(
			'element'  => '
            .header-04 .menu--primary li:hover > a,
            .header-04 .menu--primary > ul > li > a:hover,
            .header-04 .menu--primary > ul > li > a:focus,
            .header-04 .menu--primary .current-menu-ancestor > a,
            .header-04 .menu--primary .current-menu-item > a',
			'property' => 'color',
		),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'navigation_link_background_color',
	'label'       => esc_html__( 'Background Color', 'leomes' ),
	'description' => esc_html__( 'Controls the background color for main menu items.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '',
	'output'      => array(
		array(
			'element'  => '.header-04 .menu--primary .menu__container > li > a',
			'property' => 'background-color',
		),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'navigation_link_hover_background_color',
	'label'       => esc_html__( 'Hover Background Color', 'leomes' ),
	'description' => esc_html__( 'Controls the background color when hover for main menu items.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '',
	'output'      => array(
		array(
			'element'  => '
            .header-04 .menu--primary .menu__container > li > a:hover,
            .header-04 .menu--primary .menu__container > li.current-menu-item > a',
			'property' => 'background-color',
		),
	),
) );

/*--------------------------------------------------------------
# Header Button
--------------------------------------------------------------*/

Insight_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Button', 'leomes' ) . '</div>',
) );

Insight_Kirki::add_field( 'theme', array(
	'type'     => 'select',
	'settings' => $prefix . 'button_style',
	'label'    => esc_html__( 'Button Style', 'leomes' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'flat',
	'choices'  => Insight_Helper::get_header_button_style_list(),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'     => 'text',
	'settings' => $prefix . 'button_text',
	'label'    => esc_html__( 'Button Text', 'leomes' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => esc_html__( 'Get started', 'leomes' ),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'     => 'text',
	'settings' => $prefix . 'button_link',
	'label'    => esc_html__( 'Button link', 'leomes' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '#',
) );

Insight_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'button_link_target',
	'label'    => esc_html__( 'Open link in a new tab.', 'leomes' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '0',
	'choices'  => array(
		'0' => esc_html__( 'No', 'leomes' ),
		'1' => esc_html__( 'Yes', 'leomes' ),
	),
) );
