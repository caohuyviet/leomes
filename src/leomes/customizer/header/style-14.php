<?php
$section  = 'header_style_14';
$priority = 1;
$prefix   = 'header_style_14_';

Insight_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'overlay',
	'label'    => esc_html__( 'Header Overlay', 'leomes' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '0',
	'choices'  => array(
		'0' => esc_html__( 'No', 'leomes' ),
		'1' => esc_html__( 'Yes', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'logo',
	'label'    => esc_html__( 'Logo', 'leomes' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'dark',
	'choices'  => array(
		'light' => esc_html__( 'Light', 'leomes' ),
		'dark'  => esc_html__( 'Dark', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => $prefix . 'search_enable',
	'label'       => esc_html__( 'Search Button', 'leomes' ),
	'description' => esc_html__( 'Controls the display of search button in the header.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Hide', 'leomes' ),
		'1' => esc_html__( 'Show', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => $prefix . 'cart_enable',
	'label'       => esc_html__( 'Mini Cart', 'leomes' ),
	'description' => esc_html__( 'Controls the display of mini cart in the header', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '0',
	'choices'     => array(
		'0'             => esc_html__( 'Hide', 'leomes' ),
		'1'             => esc_html__( 'Show', 'leomes' ),
		'hide_on_empty' => esc_html__( 'Hide On Empty', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => $prefix . 'social_enable',
	'label'       => esc_html__( 'Social Networks', 'leomes' ),
	'description' => esc_html__( 'Controls the display of social networks in the header.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Hide', 'leomes' ),
		'1' => esc_html__( 'Show', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'border_color',
	'label'       => esc_html__( 'Border Bottom Color', 'leomes' ),
	'description' => esc_html__( 'Controls the border bottom color.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => 'rgba(0, 0, 0, 0)',
	'output'      => array(
		array(
			'element'  => '.header-14 .page-header-inner',
			'property' => 'border-bottom-color',
		),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => $prefix . 'box_shadow',
	'label'       => esc_html__( 'Box Shadow', 'leomes' ),
	'description' => esc_html__( 'Input box shadow for header. For ex: 0 0 5px #ccc', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'output'      => array(
		array(
			'element'  => '.header-14 .page-header-inner',
			'property' => 'box-shadow',
		),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'background',
	'settings'    => $prefix . 'background',
	'label'       => esc_html__( 'Background', 'leomes' ),
	'description' => esc_html__( 'Controls the background of header.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => array(
		'background-color'      => 'rgba(255, 255, 255, 1)',
		'background-image'      => '',
		'background-repeat'     => 'no-repeat',
		'background-size'       => 'cover',
		'background-attachment' => 'scroll',
		'background-position'   => 'center center',
	),
	'output'      => array(
		array(
			'element' => '.header-14 .page-header-inner',
		),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'header_icon_color',
	'label'       => esc_html__( 'Icon Color', 'leomes' ),
	'description' => esc_html__( 'Controls the color of icons on header.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => Insight::THIRD_COLOR,
	'output'      => array(
		array(
			'element'  => '
				.header-14 .page-open-mobile-menu i,
				.header-14 .page-open-main-menu i,
				.header-14 .popup-search-wrap i,
				.header-14 .mini-cart .mini-cart-icon,
				.header-14 .header-social-networks a',
			'property' => 'color',
		),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'header_icon_hover_color',
	'label'       => esc_html__( 'Icon Hover Color', 'leomes' ),
	'description' => esc_html__( 'Controls the color when hover of icons on header.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => Insight::HEADING_COLOR,
	'output'      => array(
		array(
			'element'  => '
				.header-14 .page-open-mobile-menu:hover i,
				.header-14 .page-open-main-menu:hover i,
				.header-14 .popup-search-wrap:hover i,
				.header-14 .mini-cart .mini-cart-icon:hover,
				.header-14 .header-social-networks a:before',
			'property' => 'color',
		),
	),
) );

/*--------------------------------------------------------------
# Header Button
--------------------------------------------------------------*/

Insight_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Button', 'leomes' ) . '</div>',
) );

Insight_Kirki::add_field( 'theme', array(
	'type'     => 'select',
	'settings' => $prefix . 'button_style',
	'label'    => esc_html__( 'Button Style', 'leomes' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'flat',
	'choices'  => Insight_Helper::get_header_button_style_list(),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'     => 'text',
	'settings' => $prefix . 'button_text',
	'label'    => esc_html__( 'Button Text', 'leomes' ),
	'section'  => $section,
	'priority' => $priority ++,
) );

Insight_Kirki::add_field( 'theme', array(
	'type'     => 'text',
	'settings' => $prefix . 'button_link',
	'label'    => esc_html__( 'Button link', 'leomes' ),
	'section'  => $section,
	'priority' => $priority ++,
) );

Insight_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'button_link_target',
	'label'    => esc_html__( 'Open link in a new tab.', 'leomes' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '0',
	'choices'  => array(
		'0' => esc_html__( 'No', 'leomes' ),
		'1' => esc_html__( 'Yes', 'leomes' ),
	),
) );
