<?php
$section  = 'header';
$priority = 1;
$prefix   = 'header_';

$headers = Insight_Helper::get_header_list( true );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'global_header',
	'label'       => esc_html__( 'Default Header', 'leomes' ),
	'description' => esc_html__( 'Select default header type for your site.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '02',
	'choices'     => Insight_Helper::get_header_list(),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'single_page_header_type',
	'label'       => esc_html__( 'Single Page', 'leomes' ),
	'description' => esc_html__( 'Select default header type that displays on all single pages.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '',
	'choices'     => $headers,
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'single_post_header_type',
	'label'       => esc_html__( 'Single Blog', 'leomes' ),
	'description' => esc_html__( 'Select default header type that displays on all single blog post pages.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '',
	'choices'     => $headers,
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'single_product_header_type',
	'label'       => esc_html__( 'Single Product', 'leomes' ),
	'description' => esc_html__( 'Select default header type that displays on all single product pages.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '',
	'choices'     => $headers,
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'single_portfolio_header_type',
	'label'       => esc_html__( 'Single Portfolio', 'leomes' ),
	'description' => esc_html__( 'Select default header type that displays on all single portfolio pages.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '01',
	'choices'     => $headers,
) );
