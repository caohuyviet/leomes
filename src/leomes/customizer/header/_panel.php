<?php
$panel    = 'header';
$priority = 1;

Insight_Kirki::add_section( 'header', array(
	'title'    => esc_html__( 'General', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Insight_Kirki::add_section( 'header_sticky', array(
	'title'    => esc_html__( 'Header Sticky', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Insight_Kirki::add_section( 'header_style_01', array(
	'title'    => esc_html__( 'Header Style 01', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Insight_Kirki::add_section( 'header_style_02', array(
	'title'    => esc_html__( 'Header Style 02', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Insight_Kirki::add_section( 'header_style_03', array(
	'title'    => esc_html__( 'Header Style 03', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Insight_Kirki::add_section( 'header_style_04', array(
	'title'    => esc_html__( 'Header Style 04', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Insight_Kirki::add_section( 'header_style_05', array(
	'title'    => esc_html__( 'Header Style 05', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Insight_Kirki::add_section( 'header_style_06', array(
	'title'    => esc_html__( 'Header Style 06', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Insight_Kirki::add_section( 'header_style_07', array(
	'title'    => esc_html__( 'Header Style 07', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Insight_Kirki::add_section( 'header_style_08', array(
	'title'    => esc_html__( 'Header Style 08', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Insight_Kirki::add_section( 'header_style_09', array(
	'title'    => esc_html__( 'Header Style 09', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Insight_Kirki::add_section( 'header_style_10', array(
	'title'    => esc_html__( 'Header Style 10', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Insight_Kirki::add_section( 'header_style_11', array(
	'title'    => esc_html__( 'Header Style 11', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Insight_Kirki::add_section( 'header_style_12', array(
	'title'    => esc_html__( 'Header Style 12', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Insight_Kirki::add_section( 'header_style_13', array(
	'title'    => esc_html__( 'Header Style 13', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Insight_Kirki::add_section( 'header_style_14', array(
	'title'    => esc_html__( 'Header Style 14', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Insight_Kirki::add_section( 'header_style_15', array(
	'title'    => esc_html__( 'Header Style 15', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Insight_Kirki::add_section( 'header_style_16', array(
	'title'    => esc_html__( 'Header Style 16', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Insight_Kirki::add_section( 'header_style_17', array(
	'title'    => esc_html__( 'Header Style 17', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Insight_Kirki::add_section( 'header_style_18', array(
	'title'    => esc_html__( 'Header Style 18', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
