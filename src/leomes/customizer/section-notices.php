<?php
$section  = 'notices';
$priority = 1;
$prefix   = 'notice_';

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'toggle',
	'settings'    => 'notice_cookie_enable',
	'label'       => esc_html__( 'Cookie Notice', 'leomes' ),
	'description' => esc_html__( 'The notice about cookie auto show when a user visits the site.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 1,
) );
