<?php
$section  = 'blog_single';
$priority = 1;
$prefix   = 'single_post_';

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_feature_enable',
	'label'       => esc_html__( 'Featured Image', 'leomes' ),
	'description' => esc_html__( 'Turn on to display featured image on blog single posts.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'leomes' ),
		'1' => esc_html__( 'On', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_title_enable',
	'label'       => esc_html__( 'Post Title', 'leomes' ),
	'description' => esc_html__( 'Turn on to display the post title.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'leomes' ),
		'1' => esc_html__( 'On', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_categories_enable',
	'label'       => esc_html__( 'Categories', 'leomes' ),
	'description' => esc_html__( 'Turn on to display the categories on blog single posts.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'leomes' ),
		'1' => esc_html__( 'On', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_tags_enable',
	'label'       => esc_html__( 'Tags', 'leomes' ),
	'description' => esc_html__( 'Turn on to display the tags on blog single posts.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'leomes' ),
		'1' => esc_html__( 'On', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_date_enable',
	'label'       => esc_html__( 'Post Meta Date', 'leomes' ),
	'description' => esc_html__( 'Turn on to display the date on blog single posts.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'leomes' ),
		'1' => esc_html__( 'On', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_like_enable',
	'label'       => esc_html__( 'Post Like', 'leomes' ),
	'description' => esc_html__( 'Turn on to display the like button on blog single posts.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'leomes' ),
		'1' => esc_html__( 'On', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_view_enable',
	'label'       => esc_html__( 'Post View', 'leomes' ),
	'description' => esc_html__( 'Turn on to display the view button on blog single posts.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'leomes' ),
		'1' => esc_html__( 'On', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_author_enable',
	'label'       => esc_html__( 'Author Meta', 'leomes' ),
	'description' => esc_html__( 'Turn on to display the author meta on blog single posts.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'leomes' ),
		'1' => esc_html__( 'On', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_share_enable',
	'label'       => esc_html__( 'Post Sharing', 'leomes' ),
	'description' => esc_html__( 'Turn on to display the social sharing on blog single posts.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'leomes' ),
		'1' => esc_html__( 'On', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_author_box_enable',
	'label'       => esc_html__( 'Author Info Box', 'leomes' ),
	'description' => esc_html__( 'Turn on to display the author info box on blog single posts.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'leomes' ),
		'1' => esc_html__( 'On', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_pagination_enable',
	'label'       => esc_html__( 'Previous/Next Pagination', 'leomes' ),
	'description' => esc_html__( 'Turn on to display the previous/next post pagination on blog single posts.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'leomes' ),
		'1' => esc_html__( 'On', 'leomes' ),
	),
) );


Insight_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => 'single_post_pagination_return_link',
	'label'       => esc_html__( 'Return button url', 'leomes' ),
	'description' => esc_html__( 'Controls the url when you click on pagination center button', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '#',
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_related_enable',
	'label'       => esc_html__( 'Related', 'leomes' ),
	'description' => esc_html__( 'Turn on to display related posts on blog single posts.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '0',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'leomes' ),
		'1' => esc_html__( 'On', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'            => 'number',
	'settings'        => 'single_post_related_number',
	'label'           => esc_html__( 'Number of related posts item', 'leomes' ),
	'section'         => $section,
	'priority'        => $priority ++,
	'default'         => 10,
	'choices'         => array(
		'min'  => 0,
		'max'  => 50,
		'step' => 1,
	),
	'active_callback' => array(
		array(
			'setting'  => 'single_post_related_enable',
			'operator' => '==',
			'value'    => '1',
		),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_comment_enable',
	'label'       => esc_html__( 'Comments', 'leomes' ),
	'description' => esc_html__( 'Turn on to display comments on blog single posts.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'leomes' ),
		'1' => esc_html__( 'On', 'leomes' ),
	),
) );
