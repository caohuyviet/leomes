<?php
$section  = 'blog_archive';
$priority = 1;
$prefix   = 'blog_archive_';

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => $prefix . 'style',
	'label'       => esc_html__( 'Blog Style', 'leomes' ),
	'description' => esc_html__( 'Select blog style that display for archive pages.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'1' => esc_html__( 'Large Image', 'leomes' ),
		'2' => esc_html__( 'Grid Classic', 'leomes' ),
		'3' => esc_html__( 'Grid Masonry', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => $prefix . 'columns',
	'label'       => esc_html__( 'Grid Layout Columns', 'leomes' ),
	'description' => esc_html__( 'Select columns for blog.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '2',
	'choices'     => array(
		'2' => esc_html__( '2 Columns', 'leomes' ),
		'3' => esc_html__( '3 Columns', 'leomes' ),
		'4' => esc_html__( '4 Columns', 'leomes' ),
	),
) );
