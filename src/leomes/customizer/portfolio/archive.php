<?php
$section  = 'archive_portfolio';
$priority = 1;
$prefix   = 'archive_portfolio_';

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'archive_portfolio_style',
	'label'       => esc_html__( 'Portfolio Style', 'leomes' ),
	'description' => esc_html__( 'Select portfolio style that display for archive pages.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'grid',
	'choices'     => array(
		'grid'    => esc_attr__( 'Grid Classic', 'leomes' ),
		'metro'   => esc_attr__( 'Grid Metro', 'leomes' ),
		'masonry' => esc_attr__( 'Grid Masonry', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'     => 'select',
	'settings' => 'archive_portfolio_thumbnail_size',
	'label'    => esc_html__( 'Thumbnail Size', 'leomes' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '480x480',
	'choices'  => array(
		'480x480' => esc_attr__( '480x480', 'leomes' ),
		'480x311' => esc_attr__( '480x311', 'leomes' ),
		'481x325' => esc_attr__( '481x325', 'leomes' ),
		'500x324' => esc_attr__( '500x324', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'     => 'number',
	'settings' => 'archive_portfolio_gutter',
	'label'    => esc_html__( 'Gutter', 'leomes' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 30,
	'choices'  => array(
		'min'  => 0,
		'step' => 1,
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'     => 'text',
	'settings' => 'archive_portfolio_columns',
	'label'    => esc_html__( 'Columns', 'leomes' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'xs:1;sm:2;md:3;lg:3',
) );

Insight_Kirki::add_field( 'theme', array(
	'type'     => 'select',
	'settings' => 'archive_portfolio_overlay_style',
	'label'    => esc_html__( 'Columns', 'leomes' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'faded-light',
	'choices'  => array(
		'none'        => esc_attr__( 'None', 'leomes' ),
		'modern'      => esc_attr__( 'Modern', 'leomes' ),
		'zoom'        => esc_attr__( 'Image zoom - content below', 'leomes' ),
		'zoom2'       => esc_attr__( 'Zoom and Move Up - content below', 'leomes' ),
		'faded'       => esc_attr__( 'Faded', 'leomes' ),
		'faded-light' => esc_attr__( 'Faded - Light', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'archive_portfolio_animation',
	'label'       => esc_html__( 'CSS Animation', 'leomes' ),
	'description' => esc_html__( 'Select type of animation for element to be animated when it "enters" the browsers viewport (Note: works only in modern browsers).', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'scale-up',
	'choices'     => array(
		'none'             => esc_attr__( 'None', 'leomes' ),
		'fade-in'          => esc_attr__( 'Fade In', 'leomes' ),
		'move-up'          => esc_attr__( 'Move Up', 'leomes' ),
		'scale-up'         => esc_attr__( 'Scale Up', 'leomes' ),
		'fall-perspective' => esc_attr__( 'Fall Perspective', 'leomes' ),
		'fly'              => esc_attr__( 'Fly', 'leomes' ),
		'flip'             => esc_attr__( 'Flip', 'leomes' ),
		'helix'            => esc_attr__( 'Helix', 'leomes' ),
		'pop-up'           => esc_attr__( 'Pop Up', 'leomes' ),
	),
) );
