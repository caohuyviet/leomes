<?php
$panel    = 'portfolio';
$priority = 1;

Insight_Kirki::add_section( 'archive_portfolio', array(
	'title'    => esc_html__( 'Portfolio Archive', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Insight_Kirki::add_section( 'single_portfolio', array(
	'title'    => esc_html__( 'Portfolio Single', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
