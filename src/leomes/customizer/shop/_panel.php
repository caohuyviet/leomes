<?php
$panel    = 'shop';
$priority = 1;

Insight_Kirki::add_section( 'shop_archive', array(
	'title'    => esc_html__( 'Shop Archive', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Insight_Kirki::add_section( 'shop_single', array(
	'title'    => esc_html__( 'Shop Single', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Insight_Kirki::add_section( 'shopping_cart', array(
	'title'    => esc_html__( 'Shopping Cart', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
