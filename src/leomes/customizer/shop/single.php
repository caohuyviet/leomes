<?php
$section  = 'shop_single';
$priority = 1;
$prefix   = 'single_product_';

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_product_categories_enable',
	'label'       => esc_html__( 'Categories', 'leomes' ),
	'description' => esc_html__( 'Turn on to display the categories.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'leomes' ),
		'1' => esc_html__( 'On', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_product_tags_enable',
	'label'       => esc_html__( 'Tags', 'leomes' ),
	'description' => esc_html__( 'Turn on to display the tags.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'leomes' ),
		'1' => esc_html__( 'On', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_product_sharing_enable',
	'label'       => esc_html__( 'Sharing', 'leomes' ),
	'description' => esc_html__( 'Turn on to display the sharing.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'leomes' ),
		'1' => esc_html__( 'On', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_product_up_sells_enable',
	'label'       => esc_html__( 'Up-sells products', 'leomes' ),
	'description' => esc_html__( 'Turn on to display the up-sells products section.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'leomes' ),
		'1' => esc_html__( 'On', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_product_related_enable',
	'label'       => esc_html__( 'Related products', 'leomes' ),
	'description' => esc_html__( 'Turn on to display the related products section.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'leomes' ),
		'1' => esc_html__( 'On', 'leomes' ),
	),
) );
