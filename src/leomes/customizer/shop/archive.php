<?php
$section  = 'shop_archive';
$priority = 1;
$prefix   = 'shop_archive_';

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'shop_archive_new_days',
	'label'       => esc_html__( 'New Badge (Days)', 'leomes' ),
	'description' => esc_html__( 'If the product was published within the newness time frame display the new badge.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '90',
	'choices'     => array(
		'0'  => esc_html__( 'None', 'leomes' ),
		'1'  => esc_html__( '1 day', 'leomes' ),
		'2'  => esc_html__( '2 days', 'leomes' ),
		'3'  => esc_html__( '3 days', 'leomes' ),
		'4'  => esc_html__( '4 days', 'leomes' ),
		'5'  => esc_html__( '5 days', 'leomes' ),
		'6'  => esc_html__( '6 days', 'leomes' ),
		'7'  => esc_html__( '7 days', 'leomes' ),
		'8'  => esc_html__( '8 days', 'leomes' ),
		'9'  => esc_html__( '9 days', 'leomes' ),
		'10' => esc_html__( '10 days', 'leomes' ),
		'15' => esc_html__( '15 days', 'leomes' ),
		'20' => esc_html__( '20 days', 'leomes' ),
		'25' => esc_html__( '25 days', 'leomes' ),
		'30' => esc_html__( '30 days', 'leomes' ),
		'60' => esc_html__( '60 days', 'leomes' ),
		'90' => esc_html__( '90 days', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'shop_archive_quick_view',
	'label'       => esc_html__( 'Quick View', 'leomes' ),
	'description' => esc_html__( 'Turn on to display quick view button', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'leomes' ),
		'1' => esc_html__( 'On', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'shop_archive_compare',
	'label'       => esc_html__( 'Compare', 'leomes' ),
	'description' => esc_html__( 'Turn on to display compare button', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'leomes' ),
		'1' => esc_html__( 'On', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'shop_archive_wishlist',
	'label'       => esc_html__( 'Wishlist', 'leomes' ),
	'description' => esc_html__( 'Turn on to display love button', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'leomes' ),
		'1' => esc_html__( 'On', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'shop_archive_hover_image',
	'label'       => esc_html__( 'Hover Image', 'leomes' ),
	'description' => esc_html__( 'Turn on to show first gallery image when hover', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'leomes' ),
		'1' => esc_html__( 'On', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'number',
	'settings'    => 'shop_archive_number_item',
	'label'       => esc_html__( 'Number items', 'leomes' ),
	'description' => esc_html__( 'Controls the number of products display on shop archive page', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 8,
	'choices'     => array(
		'min'  => 1,
		'max'  => 30,
		'step' => 1,
	),
) );
