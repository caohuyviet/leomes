<?php
$panel    = 'title_bar';
$priority = 1;

Insight_Kirki::add_section( 'title_bar', array(
	'title'    => esc_html__( 'General', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Insight_Kirki::add_section( 'title_bar_01', array(
	'title'    => esc_html__( 'Style 01', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
