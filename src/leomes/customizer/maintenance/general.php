<?php
$section  = 'general';
$priority = 1;
$prefix   = 'general_';

Insight_Kirki::add_field( 'theme', array(
	'type'     => 'select',
	'settings' => 'maintenance_page',
	'label'    => esc_html__( 'Page', 'leomes' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '',
	'choices'  => Insight_Maintenance::get_maintenance_pages(),
) );
