<?php
$section  = 'maintenance';
$priority = 1;
$prefix   = 'maintenance_';

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'image',
	'settings'    => $prefix . 'single_image',
	'label'       => esc_html__( 'Single Image', 'leomes' ),
	'description' => esc_html__( 'Select an image file for right image.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => INSIGHT_THEME_IMAGE_URI . '/maintenance-01-image.png',
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => $prefix . 'progress_bar',
	'label'       => esc_html__( 'Progress bar', 'leomes' ),
	'description' => esc_html__( 'Turn on to show progress bar form in maintenance mode', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Hide', 'leomes' ),
		'1' => esc_html__( 'Show', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'     => 'slider',
	'settings' => $prefix . 'percent',
	'label'    => esc_attr__( 'Percent Done', 'leomes' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 85,
	'choices'  => array(
		'min'  => '1',
		'max'  => '100',
		'step' => '1',
	),
	'output'   => array(
		array(
			'element'  => '
			.maintenance-progress-labels,
			.maintenance-progress',
			'property' => 'width',
			'units'    => '%',
		),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'     => 'text',
	'settings' => $prefix . 'title',
	'label'    => esc_html__( 'Title', 'leomes' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => esc_html__( 'Site maintenance', 'leomes' ),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'textarea',
	'settings'    => $prefix . 'text',
	'label'       => esc_html__( 'Text', 'leomes' ),
	'description' => esc_html__( 'Controls the text that display below title.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'We sincerely apologize for the inconvenience. Our site is currently undergoing scheduled maintenance and upgrades, but will return shortly after.
', 'leomes' ),
) );
