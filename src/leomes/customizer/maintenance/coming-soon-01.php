<?php
$section  = 'coming_soon_01';
$priority = 1;
$prefix   = 'coming_soon_01_';

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'background',
	'settings'    => $prefix . 'background',
	'label'       => esc_html__( 'Background', 'leomes' ),
	'description' => esc_html__( 'Select an image file for background.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => array(
		'background-image'      => INSIGHT_THEME_IMAGE_URI . '/coming-soon-01-bg.jpg',
		'background-repeat'     => 'no-repeat',
		'background-size'       => 'cover',
		'background-attachment' => 'fixed',
		'background-position'   => 'left bottom',
	),
	'output'      => array(
		array(
			'element' => '.page-template-coming-soon-01',
		),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'     => 'text',
	'settings' => $prefix . 'title',
	'label'    => esc_html__( 'Title', 'leomes' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => wp_kses( __( 'Something really good is coming very soon!', 'leomes' ), array(
		'a'    => array(
			'href'   => array(),
			'target' => array(),
		),
		'mark' => array(),
	) ),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'     => 'date',
	'settings' => $prefix . 'countdown',
	'label'    => esc_html__( 'Countdown', 'leomes' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => Insight_Helper::get_coming_soon_demo_date(),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'mailchimp_enable',
	'label'    => esc_html__( 'Mailchimp Form', 'leomes' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '1',
	'choices'  => array(
		'0' => esc_html__( 'Hide', 'leomes' ),
		'1' => esc_html__( 'Show', 'leomes' ),
	),
) );
