<?php
$section  = 'advanced';
$priority = 1;
$prefix   = 'advanced_';

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'toggle',
	'settings'    => 'smooth_scroll_enable',
	'label'       => esc_html__( 'Smooth Scroll', 'leomes' ),
	'description' => esc_html__( 'Smooth scrolling experience for websites.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 0,
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'toggle',
	'settings'    => 'scroll_top_enable',
	'label'       => esc_html__( 'Go To Top Button', 'leomes' ),
	'description' => esc_html__( 'Turn on to show go to top button.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 1,
) );

Insight_Kirki::add_field( 'theme', array(
	'type'     => 'toggle',
	'settings' => 'lazy_load_images',
	'label'    => esc_html__( 'Lazy Load Images', 'leomes' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 0,
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => 'google_api_key',
	'label'       => esc_html__( 'Google Api Key', 'leomes' ),
	'description' => sprintf( wp_kses( __( 'Follow <a href="%s" target="_blank">this link</a> and click <strong>GET A KEY</strong> button.', 'leomes' ), array(
		'a'      => array(
			'href'   => array(),
			'target' => array(),
		),
		'strong' => array(),
	) ), esc_url( 'https://developers.google.com/maps/documentation/javascript/get-api-key#get-an-api-key' ) ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'AIzaSyAtDP8prBDw5K5nKJqHImggyL9UCq8m8zo',
	'transport'   => 'postMessage',
) );
