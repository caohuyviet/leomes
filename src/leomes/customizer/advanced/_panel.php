<?php
$panel    = 'advanced';
$priority = 1;

Insight_Kirki::add_section( 'advanced', array(
	'title'    => esc_html__( 'Advanced', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Insight_Kirki::add_section( 'pre_loader', array(
	'title'    => esc_html__( 'Pre Loader', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Insight_Kirki::add_section( 'light_gallery', array(
	'title'    => esc_html__( 'Light Gallery', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
