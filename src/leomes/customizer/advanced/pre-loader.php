<?php
$section  = 'pre_loader';
$priority = 1;
$prefix   = 'pre_loader_';

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'toggle',
	'settings'    => $prefix . 'enable',
	'label'       => esc_html__( 'Preloader', 'leomes' ),
	'description' => esc_html__( 'Turn on to enable preloader.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 0,
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'background_color',
	'label'       => esc_html__( 'Background Color', 'leomes' ),
	'description' => esc_html__( 'Controls the background color for pre loader', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => 'rgba(0, 0, 0, .85)',
	'output'      => array(
		array(
			'element'  => '.page-loading',
			'property' => 'background-color',
		),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'shape_color',
	'label'       => esc_html__( 'Shape Color', 'leomes' ),
	'description' => esc_html__( 'Controls the shape color', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => Insight::SECONDARY_COLOR,
	'output'      => array(
		array(
			'element'  => '.page-loading .sk-child',
			'property' => 'background-color',
		),
	),
) );
