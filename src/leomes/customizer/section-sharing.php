<?php
$section  = 'social_sharing';
$priority = 1;
$prefix   = 'social_sharing_';

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'multicheck',
	'settings'    => $prefix . 'item_enable',
	'label'       => esc_attr__( 'Sharing Links', 'leomes' ),
	'description' => esc_html__( 'Check to the box to enable social share links.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	//'default'     => array( 'facebook', 'twitter', 'linkedin', 'google_plus', 'tumblr', 'email' ),
	'choices'     => array(
		'facebook'    => esc_attr__( 'Facebook', 'leomes' ),
		'twitter'     => esc_attr__( 'Twitter', 'leomes' ),
		'linkedin'    => esc_attr__( 'Linkedin', 'leomes' ),
		'google_plus' => esc_attr__( 'Google+', 'leomes' ),
		'tumblr'      => esc_attr__( 'Tumblr', 'leomes' ),
		'email'       => esc_attr__( 'Email', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'sortable',
	'settings'    => $prefix . 'order',
	'label'       => esc_attr__( 'Order', 'leomes' ),
	'description' => esc_html__( 'Controls the order of social share links.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => array(
		'facebook',
		'twitter',
		'google_plus',
		'tumblr',
		'linkedin',
		'email',
	),
	'choices'     => array(
		'facebook'    => esc_attr__( 'Facebook', 'leomes' ),
		'twitter'     => esc_attr__( 'Twitter', 'leomes' ),
		'google_plus' => esc_attr__( 'Google+', 'leomes' ),
		'tumblr'      => esc_attr__( 'Tumblr', 'leomes' ),
		'linkedin'    => esc_attr__( 'Linkedin', 'leomes' ),
		'email'       => esc_attr__( 'Email', 'leomes' ),
	),
) );
