<?php
$section  = 'layout';
$priority = 1;
$prefix   = 'site_';

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => $prefix . 'layout',
	'label'       => esc_html__( 'Layout', 'leomes' ),
	'description' => esc_html__( 'Controls the site layout.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'wide',
	'choices'     => array(
		'boxed' => esc_html__( 'Boxed', 'leomes' ),
		'wide'  => esc_html__( 'Wide', 'leomes' ),
	),
) );

Insight_Kirki::add_field( 'theme', array(
	'type'        => 'dimension',
	'settings'    => $prefix . 'width',
	'label'       => esc_html__( 'Site Width', 'leomes' ),
	'description' => esc_html__( 'Controls the overall site width. Enter value including any valid CSS unit, ex: 1200px.', 'leomes' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1200px',
) );
