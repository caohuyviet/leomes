<?php
$panel    = 'navigation';
$priority = 1;

Insight_Kirki::add_section( 'navigation', array(
	'title'    => esc_html__( 'Desktop Menu', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Insight_Kirki::add_section( 'navigation_minimal', array(
	'title'    => esc_html__( 'Off Canvas Menu', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Insight_Kirki::add_section( 'navigation_mobile', array(
	'title'    => esc_html__( 'Mobile Menu', 'leomes' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
