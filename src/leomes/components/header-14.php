<?php
$header_classes  = 'page-header header-14';
$_overlay_enable = Insight::setting( 'header_style_14_overlay' );

if ( $_overlay_enable === '1' ) {
	$header_classes .= ' header-layout-fixed';
}

$_logo          = Insight::setting( 'header_style_14_logo' );
$header_classes .= " {$_logo}-logo-version";

$cart_enable   = Insight::setting( 'header_style_14_cart_enable' );
$search_enable = Insight::setting( 'header_style_14_search_enable' );
?>

<header id="page-header" class="<?php echo esc_attr( $header_classes ); ?>">
	<div id="page-header-inner" class="page-header-inner" data-has-fixed-section="1">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12">
					<div class="header-wrap">
						<?php get_template_part( 'components/branding' ); ?>

						<div class="header-right">

							<?php Insight_Woo::render_mini_cart( $cart_enable ); ?>

							<?php Insight_Templates::header_search_button( $search_enable ); ?>

							<?php Insight_Templates::header_open_mobile_menu_button(); ?>

							<?php Insight_Templates::header_button( '14' ); ?>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="header-left-fixed" class="header-fixed-section left page-sidebar-fixed page-sidebar">
		<div class="inner">
			<?php Insight_Templates::header_open_canvas_menu_button( array( 'menu_title' => true ) ); ?>
		</div>
	</div>

	<div id="header-right-fixed" class="header-fixed-section right">
		<div class="inner">
			<?php Insight_Templates::header_social_networks( '14' ); ?>

			<?php Insight_Woo::render_mini_cart( $cart_enable ); ?>

			<?php Insight_Templates::header_open_mobile_menu_button(); ?>

			<?php Insight_Templates::header_button( '14' ); ?>
		</div>
	</div>

	<?php get_template_part( 'components/off-canvas' ); ?>
</header>
