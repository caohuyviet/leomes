<?php

$header_classes = 'page-header header-08';

$_overlay_enable = Insight::setting( 'header_style_08_overlay' );

if ( $_overlay_enable === '1' ) {
	$header_classes .= ' header-layout-fixed';
}

$_logo          = Insight::setting( 'header_style_08_logo' );
$header_classes .= " {$_logo}-logo-version";

$cart_enable   = Insight::setting( 'header_style_08_cart_enable' );
$search_enable = Insight::setting( 'header_style_08_search_enable' );
?>

<header id="page-header" class="<?php echo esc_attr( $header_classes ); ?>">
	<div id="page-header-inner" class="page-header-inner" data-sticky="1">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12">
					<div class="header-wrap">

						<?php get_template_part( 'components/branding' ); ?>

						<?php get_template_part( 'components/navigation' ); ?>

						<div class="header-right-wrap">
							<div class="header-right">

								<?php Insight_Templates::header_search_form( $search_enable ); ?>

								<?php Insight_Woo::render_mini_cart( $cart_enable ); ?>

								<?php Insight_Templates::header_open_mobile_menu_button(); ?>

								<?php Insight_Templates::header_button( '08' ); ?>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
