<?php
$header_classes  = 'page-header header-11';
$_overlay_enable = Insight::setting( 'header_style_11_overlay' );

if ( $_overlay_enable === '1' ) {
	$header_classes .= ' header-layout-fixed';
}

$_logo          = Insight::setting( 'header_style_11_logo' );
$header_classes .= " {$_logo}-logo-version";

$cart_enable   = Insight::setting( 'header_style_11_cart_enable' );
$search_enable = Insight::setting( 'header_style_11_search_enable' );
?>

<header id="page-header" class="<?php echo esc_attr( $header_classes ); ?>">
	<div id="page-header-inner" class="page-header-inner" data-has-fixed-section="1">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12">
					<div class="header-wrap">

						<div class="branding-wrap">
							<?php get_template_part( 'components/branding' ); ?>
						</div>

						<div class="header-right-wrap">

							<div class="header-left">

								<?php Insight_Templates::header_search_form( $search_enable ); ?>

							</div>

							<?php get_template_part( 'components/navigation' ); ?>

							<div class="header-right">

								<?php Insight_Templates::header_social_networks( '11' ); ?>

								<?php Insight_Woo::render_mini_cart( $cart_enable ); ?>

								<?php Insight_Templates::header_open_mobile_menu_button(); ?>

								<?php Insight_Templates::header_button( '11' ); ?>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="header-left-fixed" class="header-fixed-section page-sidebar-fixed">
		<div class="inner" data-slim-scroll="1">
			<div class="page-sidebar">
				<div class="page-sidebar-content">
					<?php Insight_Templates::generated_sidebar( 'left_header_widget' ); ?>
				</div>
			</div>
		</div>
	</div>
</header>
