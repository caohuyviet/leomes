<?php
/**
 * The template for displaying all single portfolio posts.
 *
 * @package TM Leomes
 * @since   1.0
 */
$style = Insight_Helper::get_post_meta( 'portfolio_layout_style', '' );
if ( $style === '' ) {
	$style = Insight::setting( 'single_portfolio_style' );
}

if ( $style === 'fullscreen' ) {
	get_template_part( 'components/content-single-portfolio', 'fullscreen' );
} else {
	get_template_part( 'components/content-single-portfolio' );
}
