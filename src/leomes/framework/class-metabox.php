<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Insight_Metabox' ) ) {
	class Insight_Metabox {

		/**
		 * Insight_Metabox constructor.
		 */
		public function __construct() {
			add_filter( 'insight_core_meta_boxes', array( $this, 'register_meta_boxes' ) );
		}

		/**
		 * Register Metabox
		 *
		 * @param $meta_boxes
		 *
		 * @return array
		 */
		public function register_meta_boxes( $meta_boxes ) {
			$page_registered_sidebars = Insight_Helper::get_registered_sidebars( true );

			$general_options = array(
				array(
					'title'  => esc_attr__( 'Layout', 'leomes' ),
					'fields' => array(
						array(
							'id'      => 'site_layout',
							'type'    => 'select',
							'title'   => esc_attr__( 'Layout', 'leomes' ),
							'desc'    => esc_attr__( 'Controls the layout of this page.', 'leomes' ),
							'options' => array(
								''      => esc_html__( 'Default', 'leomes' ),
								'boxed' => esc_html__( 'Boxed', 'leomes' ),
								'wide'  => esc_html__( 'Wide', 'leomes' ),
							),
							'default' => '',
						),
						array(
							'id'    => 'site_width',
							'type'  => 'text',
							'title' => esc_attr__( 'Site Width', 'leomes' ),
							'desc'  => esc_attr__( 'Controls the site width for this page. Enter value including any valid CSS unit, ex: 1200px. Leave blank to use global setting.', 'leomes' ),
						),
						array(
							'id'      => 'content_padding',
							'type'    => 'switch',
							'title'   => esc_attr__( 'Page Content Padding', 'leomes' ),
							'default' => '1',
							'options' => array(
								'0' => esc_html__( 'No Padding', 'leomes' ),
								'1' => esc_html__( 'Default', 'leomes' ),
							),
						),
					),
				),
				array(
					'title'  => esc_attr__( 'Background', 'leomes' ),
					'fields' => array(
						array(
							'id'      => 'site_background_message',
							'type'    => 'message',
							'title'   => esc_attr__( 'Info', 'leomes' ),
							'message' => esc_attr__( 'These options controls the background on boxed mode.', 'leomes' ),
						),
						array(
							'id'    => 'site_background_color',
							'type'  => 'color',
							'title' => esc_attr__( 'Background Color', 'leomes' ),
							'desc'  => esc_attr__( 'Controls the background color of the outer background area in boxed mode of this page.', 'leomes' ),
						),
						array(
							'id'    => 'site_background_image',
							'type'  => 'media',
							'title' => esc_attr__( 'Background Image', 'leomes' ),
							'desc'  => esc_attr__( 'Controls the background image of the outer background area in boxed mode of this page.', 'leomes' ),
						),
						array(
							'id'      => 'site_background_repeat',
							'type'    => 'select',
							'title'   => esc_attr__( 'Background Repeat', 'leomes' ),
							'desc'    => esc_attr__( 'Controls the background repeat of the outer background area in boxed mode of this page.', 'leomes' ),
							'options' => array(
								'no-repeat' => esc_html__( 'No repeat', 'leomes' ),
								'repeat'    => esc_html__( 'Repeat', 'leomes' ),
								'repeat-x'  => esc_html__( 'Repeat X', 'leomes' ),
								'repeat-y'  => esc_html__( 'Repeat Y', 'leomes' ),
							),
						),
						array(
							'id'      => 'site_background_attachment',
							'type'    => 'select',
							'title'   => esc_attr__( 'Background Attachment', 'leomes' ),
							'desc'    => esc_attr__( 'Controls the background attachment of the outer background area in boxed mode of this page.', 'leomes' ),
							'options' => array(
								''       => esc_html__( 'Default', 'leomes' ),
								'fixed'  => esc_html__( 'Fixed', 'leomes' ),
								'scroll' => esc_html__( 'Scroll', 'leomes' ),
							),
						),
						array(
							'id'    => 'site_background_position',
							'type'  => 'text',
							'title' => esc_html__( 'Background Position', 'leomes' ),
							'desc'  => esc_attr__( 'Controls the background position of the outer background area in boxed mode of this page.', 'leomes' ),
						),
						array(
							'id'      => 'content_background_message',
							'type'    => 'message',
							'title'   => esc_attr__( 'Info', 'leomes' ),
							'message' => esc_attr__( 'These options controls the background of main content on this page.', 'leomes' ),
						),
						array(
							'id'    => 'content_background_color',
							'type'  => 'color',
							'title' => esc_attr__( 'Background Color', 'leomes' ),
							'desc'  => esc_attr__( 'Controls the background color of main content on this page.', 'leomes' ),
						),
						array(
							'id'    => 'content_background_image',
							'type'  => 'media',
							'title' => esc_attr__( 'Background Image', 'leomes' ),
							'desc'  => esc_attr__( 'Controls the background image of main content on this page.', 'leomes' ),
						),
						array(
							'id'      => 'content_background_repeat',
							'type'    => 'select',
							'title'   => esc_attr__( 'Background Repeat', 'leomes' ),
							'desc'    => esc_attr__( 'Controls the background repeat of main content on this page.', 'leomes' ),
							'options' => array(
								'no-repeat' => esc_html__( 'No repeat', 'leomes' ),
								'repeat'    => esc_html__( 'Repeat', 'leomes' ),
								'repeat-x'  => esc_html__( 'Repeat X', 'leomes' ),
								'repeat-y'  => esc_html__( 'Repeat Y', 'leomes' ),
							),
						),
						array(
							'id'    => 'content_background_position',
							'type'  => 'text',
							'title' => esc_html__( 'Background Position', 'leomes' ),
							'desc'  => esc_attr__( 'Controls the background position of main content on this page.', 'leomes' ),
						),
					),
				),
				array(
					'title'  => esc_attr__( 'Header', 'leomes' ),
					'fields' => array(
						array(
							'id'      => 'top_bar_type',
							'type'    => 'select',
							'title'   => esc_attr__( 'Top Bar Type', 'leomes' ),
							'desc'    => esc_attr__( 'Select top bar type that displays on this page.', 'leomes' ),
							'default' => '',
							'options' => array(
								''     => esc_html__( 'Default', 'leomes' ),
								'none' => esc_html__( 'None', 'leomes' ),
								'01'   => esc_html__( 'Top Bar 01', 'leomes' ),
							),
						),
						array(
							'id'      => 'header_type',
							'type'    => 'select',
							'title'   => esc_attr__( 'Header Type', 'leomes' ),
							'desc'    => esc_attr__( 'Select header type that displays on this page.', 'leomes' ),
							'default' => '',
							'options' => Insight_Helper::get_header_list( true ),
						),
						array(
							'id'      => 'menu_display',
							'type'    => 'select',
							'title'   => esc_attr__( 'Primary menu', 'leomes' ),
							'desc'    => esc_attr__( 'Select which menu displays on this page.', 'leomes' ),
							'default' => '',
							'options' => Insight_Helper::get_all_menus(),
						),
						array(
							'id'      => 'menu_one_page',
							'type'    => 'switch',
							'title'   => esc_attr__( 'One Page Menu', 'leomes' ),
							'default' => '0',
							'options' => array(
								'0' => esc_html__( 'Disable', 'leomes' ),
								'1' => esc_html__( 'Enable', 'leomes' ),
							),
						),
						array(
							'id'      => 'custom_logo',
							'type'    => 'media',
							'title'   => esc_attr__( 'Custom Logo', 'leomes' ),
							'desc'    => esc_attr__( 'Select custom logo for this page.', 'leomes' ),
							'default' => '',
						),
						array(
							'id'      => 'custom_logo_width',
							'type'    => 'text',
							'title'   => esc_attr__( 'Custom Logo Width', 'leomes' ),
							'desc'    => esc_attr__( 'Controls the width of logo. For ex: 150px', 'leomes' ),
							'default' => '',
						),
						array(
							'id'      => 'custom_sticky_logo',
							'type'    => 'media',
							'title'   => esc_attr__( 'Custom Sticky Logo', 'leomes' ),
							'desc'    => esc_attr__( 'Select custom sticky logo for this page.', 'leomes' ),
							'default' => '',
						),
						array(
							'id'      => 'custom_sticky_logo_width',
							'type'    => 'text',
							'title'   => esc_attr__( 'Custom Sticky Logo Width', 'leomes' ),
							'desc'    => esc_attr__( 'Controls the width of sticky logo. For ex: 150px', 'leomes' ),
							'default' => '',
						),
					),
				),
				array(
					'title'  => esc_attr__( 'Page Title Bar', 'leomes' ),
					'fields' => array(
						array(
							'id'      => 'page_title_bar_layout',
							'type'    => 'switch',
							'title'   => esc_attr__( 'Layout', 'leomes' ),
							'default' => 'default',
							'options' => array(
								'default' => esc_html__( 'Default', 'leomes' ),
								'none'    => esc_html__( 'Hide', 'leomes' ),
								'01'      => esc_html__( 'Style 01', 'leomes' ),
							),
						),
						array(
							'id'      => 'page_title_bar_background_color',
							'type'    => 'color',
							'title'   => esc_attr__( 'Background Color', 'leomes' ),
							'default' => '',
						),
						array(
							'id'      => 'page_title_bar_background',
							'type'    => 'media',
							'title'   => esc_attr__( 'Background Image', 'leomes' ),
							'default' => '',
						),
						array(
							'id'      => 'page_title_bar_background_overlay',
							'type'    => 'color',
							'title'   => esc_attr__( 'Background Overlay', 'leomes' ),
							'default' => '',
						),
						array(
							'id'    => 'page_title_bar_custom_heading',
							'type'  => 'text',
							'title' => esc_attr__( 'Custom Heading Text', 'leomes' ),
							'desc'  => esc_attr__( 'Insert custom heading for the page title bar. Leave blank to use default.', 'leomes' ),
						),
					),
				),
				array(
					'title'  => esc_attr__( 'Sidebars', 'leomes' ),
					'fields' => array(
						array(
							'id'      => 'page_sidebar_1',
							'type'    => 'select',
							'title'   => esc_html__( 'Sidebar 1', 'leomes' ),
							'desc'    => esc_html__( 'Select sidebar 1 that will display on this page.', 'leomes' ),
							'default' => 'default',
							'options' => $page_registered_sidebars,
						),
						array(
							'id'      => 'page_sidebar_2',
							'type'    => 'select',
							'title'   => esc_html__( 'Sidebar 2', 'leomes' ),
							'desc'    => esc_html__( 'Select sidebar 2 that will display on this page.', 'leomes' ),
							'default' => 'default',
							'options' => $page_registered_sidebars,
						),
						array(
							'id'      => 'page_sidebar_position',
							'type'    => 'switch',
							'title'   => esc_html__( 'Sidebar Position', 'leomes' ),
							'default' => 'default',
							'options' => Insight_Helper::get_list_sidebar_positions( true ),
						),
					),
				),
				array(
					'title'  => esc_attr__( 'Sliders', 'leomes' ),
					'fields' => array(
						array(
							'id'      => 'revolution_slider',
							'type'    => 'select',
							'title'   => esc_attr__( 'Revolution Slider', 'leomes' ),
							'desc'    => esc_attr__( 'Select the unique name of the slider.', 'leomes' ),
							'options' => Insight_Helper::get_list_revslider(),
						),
						array(
							'id'      => 'slider_position',
							'type'    => 'select',
							'title'   => esc_attr__( 'Slider Position', 'leomes' ),
							'default' => 'below',
							'options' => array(
								'above' => esc_attr__( 'Above Header', 'leomes' ),
								'below' => esc_attr__( 'Below Header', 'leomes' ),
							),
						),
					),
				),
				array(
					'title'  => esc_attr__( 'Footer', 'leomes' ),
					'fields' => array(
						array(
							'id'      => 'footer_page',
							'type'    => 'select',
							'title'   => esc_attr__( 'Footer Page', 'leomes' ),
							'default' => 'default',
							'options' => Insight_Footer::get_list_footers( true ),
						),
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'insight_page_options',
				'title'      => esc_html__( 'Page Options', 'leomes' ),
				'post_types' => array( 'page' ),
				'context'    => 'normal',
				'priority'   => 'high',
				'fields'     => array(
					array(
						'type'  => 'tabpanel',
						'items' => $general_options,
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'insight_post_options',
				'title'      => esc_html__( 'Page Options', 'leomes' ),
				'post_types' => array( 'post' ),
				'context'    => 'normal',
				'priority'   => 'high',
				'fields'     => array(
					array(
						'type'  => 'tabpanel',
						'items' => array_merge( array(
							array(
								'title'  => esc_attr__( 'Post', 'leomes' ),
								'fields' => array(
									array(
										'id'    => 'post_gallery',
										'type'  => 'gallery',
										'title' => esc_attr__( 'Gallery Format', 'leomes' ),
									),
									array(
										'id'    => 'post_video',
										'type'  => 'textarea',
										'title' => esc_html__( 'Video Format', 'leomes' ),
									),
									array(
										'id'    => 'post_audio',
										'type'  => 'textarea',
										'title' => esc_html__( 'Audio Format', 'leomes' ),
									),
									array(
										'id'    => 'post_quote_text',
										'type'  => 'text',
										'title' => esc_html__( 'Quote Format - Source Text', 'leomes' ),
									),
									array(
										'id'    => 'post_quote_name',
										'type'  => 'text',
										'title' => esc_html__( 'Quote Format - Source Name', 'leomes' ),
									),
									array(
										'id'    => 'post_quote_url',
										'type'  => 'text',
										'title' => esc_html__( 'Quote Format - Source Url', 'leomes' ),
									),
									array(
										'id'    => 'post_link',
										'type'  => 'text',
										'title' => esc_html__( 'Link Format', 'leomes' ),
									),
								),
							),
						), $general_options ),
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'insight_product_options',
				'title'      => esc_html__( 'Page Options', 'leomes' ),
				'post_types' => array( 'product' ),
				'context'    => 'normal',
				'priority'   => 'high',
				'fields'     => array(
					array(
						'type'  => 'tabpanel',
						'items' => $general_options,
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'insight_portfolio_options',
				'title'      => esc_html__( 'Page Options', 'leomes' ),
				'post_types' => array( 'portfolio' ),
				'context'    => 'normal',
				'priority'   => 'high',
				'fields'     => array(
					array(
						'type'  => 'tabpanel',
						'items' => array_merge( array(
							array(
								'title'  => esc_attr__( 'Portfolio', 'leomes' ),
								'fields' => array(
									array(
										'id'      => 'portfolio_layout_style',
										'type'    => 'select',
										'title'   => esc_attr__( 'Single Portfolio Style', 'leomes' ),
										'desc'    => esc_attr__( 'Select style of this single portfolio post page.', 'leomes' ),
										'default' => '',
										'options' => array(
											''             => esc_html__( 'Default', 'leomes' ),
											'left_details' => esc_html__( 'Left Details', 'leomes' ),
											'flat'         => esc_html__( 'Flat', 'leomes' ),
											'slider'       => esc_html__( 'Image Slider', 'leomes' ),
											'video'        => esc_html__( 'Video', 'leomes' ),
											'fullscreen'   => esc_html__( 'Fullscreen', 'leomes' ),
										),
									),
									array(
										'id'    => 'portfolio_gallery',
										'type'  => 'gallery',
										'title' => esc_attr__( 'Gallery', 'leomes' ),
									),
									array(
										'id'    => 'portfolio_video_url',
										'type'  => 'textarea',
										'title' => esc_html__( 'Video Url', 'leomes' ),
									),
									array(
										'id'    => 'portfolio_client',
										'type'  => 'text',
										'title' => esc_html__( 'Client', 'leomes' ),
									),
									array(
										'id'    => 'portfolio_date',
										'type'  => 'text',
										'title' => esc_html__( 'Date', 'leomes' ),
									),
									array(
										'id'    => 'portfolio_awards',
										'type'  => 'editor',
										'title' => esc_html__( 'Awards', 'leomes' ),
									),
									array(
										'id'    => 'portfolio_team',
										'type'  => 'editor',
										'title' => esc_html__( 'Team', 'leomes' ),
									),
									array(
										'id'    => 'portfolio_url',
										'type'  => 'text',
										'title' => esc_html__( 'Url', 'leomes' ),
									),
								),
							),
						), $general_options ),
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'insight_testimonial_options',
				'title'      => esc_html__( 'Testimonial Options', 'leomes' ),
				'post_types' => array( 'testimonial' ),
				'context'    => 'normal',
				'priority'   => 'high',
				'fields'     => array(
					array(
						'type'  => 'tabpanel',
						'items' => array(
							array(
								'title'  => esc_html__( 'Testimonial Details', 'leomes' ),
								'fields' => array(
									array(
										'id'      => 'by_line',
										'type'    => 'text',
										'title'   => esc_html__( 'By Line', 'leomes' ),
										'desc'    => esc_html__( 'Enter a byline for the customer giving this testimonial (for example: "CEO of Thememove").', 'leomes' ),
										'default' => '',
									),
									array(
										'id'      => 'url',
										'type'    => 'text',
										'title'   => esc_html__( 'Url', 'leomes' ),
										'desc'    => esc_html__( 'Enter a URL that applies to this customer (for example: http://www.thememove.com/).', 'leomes' ),
										'default' => '',
									),
									array(
										'id'      => 'rating',
										'type'    => 'select',
										'title'   => esc_attr__( 'Rating', 'leomes' ),
										'default' => '',
										'options' => array(
											''  => esc_html__( 'Select a rating', 'leomes' ),
											'1' => esc_html__( '1 Star', 'leomes' ),
											'2' => esc_html__( '2 Stars', 'leomes' ),
											'3' => esc_html__( '3 Stars', 'leomes' ),
											'4' => esc_html__( '4 Stars', 'leomes' ),
											'5' => esc_html__( '5 Stars', 'leomes' ),
										),
									),
								),
							),
						),
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'insight_footer_options',
				'title'      => esc_html__( 'Footer Options', 'leomes' ),
				'post_types' => array( 'ic_footer' ),
				'context'    => 'normal',
				'priority'   => 'high',
				'fields'     => array(
					array(
						'type'  => 'tabpanel',
						'items' => array(
							array(
								'title'  => esc_html__( 'Effect', 'leomes' ),
								'fields' => array(
									array(
										'id'      => 'effect',
										'type'    => 'switch',
										'title'   => esc_attr__( 'Footer Effect', 'leomes' ),
										'default' => '',
										'options' => array(
											''         => esc_html__( 'Normal', 'leomes' ),
											'parallax' => esc_html__( 'Parallax', 'leomes' ),
										),
									),
								),
							),
							array(
								'title'  => esc_html__( 'Styling', 'leomes' ),
								'fields' => array(
									array(
										'id'      => 'widget_title_color',
										'type'    => 'color',
										'title'   => esc_attr__( 'Widget Title Color', 'leomes' ),
										'desc'    => esc_attr__( 'Controls the color of widget title.', 'leomes' ),
										'default' => '#cccccc',
									),
									array(
										'id'      => 'text_color',
										'type'    => 'color',
										'title'   => esc_attr__( 'Text Color', 'leomes' ),
										'desc'    => esc_attr__( 'Controls the color of footer text.', 'leomes' ),
										'default' => '#7e7e7e',
									),
									array(
										'id'      => 'link_color',
										'type'    => 'color',
										'title'   => esc_attr__( 'Link Color', 'leomes' ),
										'desc'    => esc_attr__( 'Controls the color of footer links.', 'leomes' ),
										'default' => '#7e7e7e',
									),
									array(
										'id'      => 'link_hover_color',
										'type'    => 'color',
										'title'   => esc_attr__( 'Link Hover Color', 'leomes' ),
										'desc'    => esc_attr__( 'Controls the color when hover of footer links.', 'leomes' ),
										'default' => Insight::SECONDARY_COLOR,
									),
								),
							),
						),
					),
				),
			);

			return $meta_boxes;
		}

	}

	new Insight_Metabox();
}
