<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Plugin installation and activation for WordPress themes
 */
if ( ! class_exists( 'Insight_Register_Plugins' ) ) {
	class Insight_Register_Plugins {

		public function __construct() {
			add_filter( 'insight_core_tgm_plugins', array( $this, 'register_required_plugins' ) );
		}

		public function register_required_plugins() {
			/*
			 * Array of plugin arrays. Required keys are name and slug.
			 * If the source is NOT from the .org repo, then source is also required.
			 */
			$plugins = array(
				array(
					'name'     => esc_html__( 'Insight Core', 'leomes' ),
					'slug'     => 'insight-core',
					'source'   => 'https://www.dropbox.com/s/tnj86jmjzu18yia/insight-core-1.5.2.4.zip?dl=1',
					'version'  => '1.5.2.4',
					'required' => true,
				),
				array(
					'name'     => esc_html__( 'Revolution Slider', 'leomes' ),
					'slug'     => 'revslider',
					'source'   => 'https://www.dropbox.com/s/rbsgbsorod0bu2x/revslider-5.4.6.3.1.zip?dl=1',
					'version'  => '5.4.6.3.1',
					'required' => true,
				),
				array(
					'name'     => esc_html__( 'WPBakery Page Builder', 'leomes' ),
					'slug'     => 'js_composer',
					'source'   => 'https://www.dropbox.com/s/ql7cvir2c5x64rg/js_composer-5.4.5.zip?dl=1',
					'version'  => '5.4.5',
					'required' => true,
				),
				array(
					'name'    => esc_html__( 'WPBakery Page Builder (Visual Composer) Clipboard', 'leomes' ),
					'slug'    => 'vc_clipboard',
					'source'  => 'https://www.dropbox.com/s/blxfmhb3aep0sat/vc_clipboard-4.1.1.zip?dl=1',
					'version' => '4.1.1',
				),
				array(
					'name' => esc_html__( 'Contact Form 7', 'leomes' ),
					'slug' => 'contact-form-7',
				),
				array(
					'name' => esc_html__( 'MailChimp for WordPress', 'leomes' ),
					'slug' => 'mailchimp-for-wp',
				),
				array(
					'name' => esc_html__( 'WP-PostViews', 'leomes' ),
					'slug' => 'wp-postviews',
				),
				array(
					'name' => esc_html__( 'Image Hotspot by DevVN', 'leomes' ),
					'slug' => 'devvn-image-hotspot',
				),
				array(
					'name' => esc_html__( 'WooCommerce', 'leomes' ),
					'slug' => 'woocommerce',
				),
				array(
					'name'     => esc_html__( 'YITH WooCommerce Compare', 'leomes' ),
					'slug'     => 'yith-woocommerce-compare',
					'required' => false,
				),
				array(
					'name'     => esc_html__( 'YITH WooCommerce Wishlist', 'leomes' ),
					'slug'     => 'yith-woocommerce-wishlist',
					'required' => false,
				),
			);

			return $plugins;
		}

	}

	new Insight_Register_Plugins();
}
