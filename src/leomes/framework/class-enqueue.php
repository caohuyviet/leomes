<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Enqueue scripts and styles.
 */
if ( ! class_exists( 'Insight_Enqueue' ) ) {
	class Insight_Enqueue {

		protected static $instance = null;

		public function __construct() {
			add_filter( 'style_loader_tag', array( $this, 'remove_type_attr' ), 9999, 1 );
			add_filter( 'script_loader_tag', array( $this, 'remove_type_attr' ), 9999, 1 );

			// Remove WordPress version from any enqueued scripts.
			add_filter( 'style_loader_src', array( $this, 'at_remove_wp_ver_css_js' ), 9999 );
			add_filter( 'script_loader_src', array( $this, 'at_remove_wp_ver_css_js' ), 9999 );

			add_filter( 'stylesheet_uri', array( $this, 'use_minify_stylesheet' ), 10, 2 );

			add_action( 'wp_enqueue_scripts', array(
				$this,
				'enqueue',
			) );

			add_action( 'wp_enqueue_scripts', array(
				$this,
				'custom_css',
			) );

			// Add custom JS.
			add_action( 'wp_footer', array( $this, 'custom_js' ), 99 );

			add_filter( 'wpcf7_load_js', '__return_false' );
			add_filter( 'wpcf7_load_css', '__return_false' );

			add_action( 'init', array( $this, 'remove_hint_from_swatches' ), 99 );

			add_action( 'wp_enqueue_scripts', array( $this, 'dequeue_woocommerce_styles_scripts' ), 99 );
		}

		/**
		 * @param $src
		 *
		 * @return mixed|string
		 */
		public function at_remove_wp_ver_css_js( $src ) {
			$override = apply_filters( 'pre_at_remove_wp_ver_css_js', false, $src );
			if ( $override !== false ) {
				return $override;
			}

			if ( strpos( $src, 'ver=' ) ) {
				$src = remove_query_arg( 'ver', $src );
			}

			return $src;
		}

		function use_minify_stylesheet( $stylesheet, $stylesheet_dir ) {
			if ( file_exists( get_template_directory_uri() . '/style.min.css' ) ) {
				$stylesheet = get_template_directory_uri() . '/style.min.css';
			}

			return $stylesheet;
		}

		function remove_type_attr( $tag ) {
			return preg_replace( "/type=['\"]text\/(javascript|css)['\"]/", '', $tag );
		}

		function dequeue_woocommerce_styles_scripts() {
			if ( function_exists( 'is_woocommerce' ) ) {
				if ( ! is_woocommerce() && ! is_cart() && ! is_checkout() ) {
					# Styles
					/*wp_dequeue_style( 'woocommerce-general' );
					wp_dequeue_style( 'woocommerce-layout' );
					wp_dequeue_style( 'woocommerce-smallscreen' );
					wp_dequeue_style( 'woocommerce_frontend_styles' );
					wp_dequeue_style( 'woocommerce_fancybox_styles' );
					wp_dequeue_style( 'woocommerce_chosen_styles' );
					wp_dequeue_style( 'woocommerce_prettyPhoto_css' );*/
					# Scripts
					/*wp_dequeue_script( 'wc_price_slider' );
					wp_dequeue_script( 'wc-single-product' );
					wp_dequeue_script( 'wc-add-to-cart' );
					wp_dequeue_script( 'wc-cart-fragments' );
					wp_dequeue_script( 'wc-checkout' );
					wp_dequeue_script( 'wc-add-to-cart-variation' );
					wp_dequeue_script( 'wc-single-product' );
					wp_dequeue_script( 'wc-cart' );
					wp_dequeue_script( 'wc-chosen' );
					wp_dequeue_script( 'woocommerce' );
					wp_dequeue_script( 'prettyPhoto' );
					wp_dequeue_script( 'prettyPhoto-init' );
					wp_dequeue_script( 'jquery-blockui' );
					wp_dequeue_script( 'jquery-placeholder' );
					wp_dequeue_script( 'fancybox' );
					wp_dequeue_script( 'jqueryui' );*/


					// Dequeue scripts & styles YITH Compare
					wp_dequeue_style( 'jquery-colorbox' );
					wp_dequeue_script( 'yith-woocompare-main' );
					wp_dequeue_script( 'jquery-colorbox' );

					// Dequeue scripts & styles YITH Wishlist
					wp_dequeue_script( 'jquery-yith-wcwl' );
					wp_dequeue_script( 'jquery-yith-wcwl-user' );
				}
			}
		}

		function enqueue_woocommerce_styles_scripts() {
			wp_enqueue_script( 'jquery-yith-wcwl' );
			wp_enqueue_script( 'jquery-yith-wcwl-user' );

			wp_enqueue_style( 'jquery-colorbox' );
			wp_enqueue_script( 'yith-woocompare-main' );
			wp_enqueue_script( 'jquery-colorbox' );
		}

		public static function instance() {
			if ( null === self::$instance ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		public function remove_hint_from_swatches() {
			add_action( 'wp_enqueue_scripts', array(
				$this,
				'remove_hint',
			) );

		}

		public function remove_hint() {
			wp_dequeue_style( 'hint' );
		}

		/**
		 * Enqueue scripts & styles.
		 *
		 * @access public
		 */
		public function enqueue() {
			$post_type = get_post_type();
			$min       = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG == true ? '' : '.min';

			// Remove prettyPhoto, default light box of woocommerce.
			wp_dequeue_script( 'prettyPhoto' );
			wp_dequeue_script( 'prettyPhoto-init' );
			wp_dequeue_style( 'woocommerce_prettyPhoto_css' );

			// Prevent enqueue ihotspot on all pages then only enqueue when use.
			wp_dequeue_script( 'ihotspot-js' );
			wp_dequeue_style( 'ihotspot' );

			// Remove font awesome from Yith Wishlist plugin.
			wp_dequeue_style( 'yith-wcwl-font-awesome' );

			wp_register_style( 'font-ion', INSIGHT_THEME_URI . '/assets/fonts/ion/font-ion.min.css', null, null );

			wp_register_style( 'justifiedGallery', INSIGHT_THEME_URI . '/assets/libs/justifiedGallery/justifiedGallery.min.css', null, '3.6.3' );
			wp_register_script( 'justifiedGallery', INSIGHT_THEME_URI . '/assets/libs/justifiedGallery/jquery.justifiedGallery.min.js', array( 'jquery' ), '3.6.3', true );

			wp_register_style( 'lightgallery', INSIGHT_THEME_URI . '/assets/libs/lightGallery/css/lightgallery.min.css', null, '1.6.4' );
			wp_register_script( 'lightgallery', INSIGHT_THEME_URI . "/assets/libs/lightGallery/js/lightgallery-all{$min}.js", array(
				'jquery',
				'picturefill',
				'mousewheel',
			), null, true );

			wp_register_style( 'swiper', INSIGHT_THEME_URI . '/assets/libs/swiper/css/swiper.min.css', null, '4.0.3' );
			wp_register_script( 'swiper', INSIGHT_THEME_URI . "/assets/libs/swiper/js/swiper{$min}.js", array( 'jquery' ), '4.0.3', true );

			wp_register_style( 'magnific-popup', INSIGHT_THEME_URI . '/assets/libs/magnific-popup/magnific-popup.css' );
			wp_register_script( 'magnific-popup', INSIGHT_THEME_URI . '/assets/libs/magnific-popup/jquery.magnific-popup.js', array( 'jquery' ), INSIGHT_THEME_VERSION, true );

			wp_register_style( 'growl', INSIGHT_THEME_URI . '/assets/libs/growl/css/jquery.growl.min.css', null, '1.3.3' );
			wp_register_script( 'growl', INSIGHT_THEME_URI . "/assets/libs/growl/js/jquery.growl{$min}.js", array( 'jquery' ), '1.3.3', true );

			/*
			 * Begin Register scripts to be enqueued later using the wp_enqueue_script() function.
			 */

			wp_register_script( 'slimscroll', INSIGHT_THEME_URI . '/assets/libs/slimscroll/jquery.slimscroll.min.js', array( 'jquery' ), '1.3.8', true );
			wp_register_script( 'easing', INSIGHT_THEME_URI . '/assets/libs/easing/jquery.easing.min.js', array( 'jquery' ), '1.3', true );
			wp_register_script( 'matchheight', INSIGHT_THEME_URI . '/assets/libs/matchHeight/jquery.matchHeight-min.js', array( 'jquery' ), INSIGHT_THEME_VERSION, true );
			wp_register_script( 'gmap3', INSIGHT_THEME_URI . '/assets/libs/gmap3/gmap3.min.js', array( 'jquery' ), INSIGHT_THEME_VERSION, true );
			wp_register_script( 'countdown', INSIGHT_THEME_URI . '/assets/libs/jquery.countdown/js/jquery.countdown.min.js', array( 'jquery' ), INSIGHT_THEME_VERSION, true );
			wp_register_script( 'easy-pie-chart', INSIGHT_THEME_URI . '/assets/libs/ease-pie-chart/jquery.easypiechart.min.js', array( 'jquery' ), null, true );
			wp_register_script( 'typed', INSIGHT_THEME_URI . '/assets/js/typed.min.js', array( 'jquery' ), null, true );
			wp_register_script( 'insight-pie-chart', INSIGHT_THEME_URI . '/assets/js/pie_chart.js', array(
				'jquery',
				'waypoints',
			), null, true );

			wp_register_script( 'sticky-kit', INSIGHT_THEME_URI . '/assets/js/jquery.sticky-kit.min.js', array(
				'jquery',
				'insight-main',
			), INSIGHT_THEME_VERSION, true );

			wp_register_script( 'smooth-scroll', INSIGHT_THEME_URI . '/assets/libs/smooth-scroll-for-web/SmoothScroll.min.js', array(
				'jquery',
			), '1.4.6', true );

			wp_register_script( 'picturefill', INSIGHT_THEME_URI . '/assets/libs/picturefill/picturefill.min.js', array( 'jquery' ), null, true );

			wp_register_script( 'mousewheel', INSIGHT_THEME_URI . "/assets/libs/mousewheel/jquery.mousewheel{$min}.js", array( 'jquery' ), INSIGHT_THEME_VERSION, true );

			wp_register_script( 'lazyload', INSIGHT_THEME_URI . "/assets/libs/lazyload/lazyload{$min}.js", array(), INSIGHT_THEME_VERSION, true );

			wp_register_script( 'tween-max', INSIGHT_THEME_URI . '/assets/libs/tween-max/TweenMax.min.js', array(
				'jquery',
			), INSIGHT_THEME_VERSION, true );

			wp_register_script( 'firefly', INSIGHT_THEME_URI . '/assets/js/firefly.js', array(
				'jquery',
			), INSIGHT_THEME_VERSION, true );

			wp_register_script( 'wavify', INSIGHT_THEME_URI . '/assets/js/wavify.js', array(
				'jquery',
				'tween-max',
			), INSIGHT_THEME_VERSION, true );

			wp_register_script( 'odometer', INSIGHT_THEME_URI . '/assets/libs/odometer/odometer.min.js', array(
				'jquery',
			), INSIGHT_THEME_VERSION, true );

			wp_register_script( 'counter-up', INSIGHT_THEME_URI . '/assets/libs/counterup/jquery.counterup.min.js', array(
				'jquery',
			), INSIGHT_THEME_VERSION, true );

			wp_register_script( 'counter', INSIGHT_THEME_URI . '/assets/js/counter.js', array(
				'jquery',
			), INSIGHT_THEME_VERSION, true );

			wp_register_script( 'circle-progress', INSIGHT_THEME_URI . '/assets/libs/circle-progress/circle-progress.min.js', array( 'jquery' ), null, true );

			wp_register_script( 'insight-pricing', INSIGHT_THEME_URI . '/assets/js/pricing.js', array(
				'jquery',
				'matchheight',
			), null, true );

			wp_register_script( 'insight-accordion', INSIGHT_THEME_URI . '/assets/js/accordion.js', array( 'jquery' ), INSIGHT_THEME_VERSION, true );

			/*
			 * Enqueue the theme's style.css.
			 * This is recommended because we can add inline styles there
			 * and some plugins use it to do exactly that.
			 */
			wp_enqueue_style( 'insight-style', get_stylesheet_uri() );
			wp_enqueue_style( 'font-ion' );
			wp_enqueue_style( 'swiper' );

			/*
			 * End register scripts
			 */

			if ( Insight::setting( 'header_sticky_enable' ) ) {
				wp_enqueue_script( 'headroom', INSIGHT_THEME_URI . "/assets/js/headroom{$min}.js", array( 'jquery' ), INSIGHT_THEME_VERSION, true );
			}

			if ( Insight::setting( 'smooth_scroll_enable' ) ) {
				wp_enqueue_script( 'smooth-scroll' );
			}

			wp_enqueue_script( 'matchheight' );
			wp_enqueue_script( 'jquery-smooth-scroll', INSIGHT_THEME_URI . '/assets/libs/smooth-scroll/jquery.smooth-scroll.min.js', array( 'jquery' ), INSIGHT_THEME_VERSION, true );
			wp_enqueue_script( 'swiper' );
			wp_enqueue_script( 'imagesloaded' );
			wp_enqueue_script( 'isotope-masonry', INSIGHT_THEME_URI . '/assets/libs/isotope/js/isotope.pkgd.min.js', array( 'jquery' ), INSIGHT_THEME_VERSION, true );
			wp_enqueue_script( 'isotope-packery', INSIGHT_THEME_URI . '/assets/js/packery-mode.pkgd.min.js', array( 'jquery' ), INSIGHT_THEME_VERSION, true );
			wp_enqueue_script( 'waypoints', INSIGHT_THEME_URI . '/assets/libs/waypoints/jquery.waypoints.min.js', array( 'jquery' ), INSIGHT_THEME_VERSION, true );
			wp_enqueue_script( 'smartmenus', INSIGHT_THEME_URI . '/assets/libs/smartmenus/jquery.smartmenus.min.js', array( 'jquery' ), INSIGHT_THEME_VERSION, true );
			wp_enqueue_script( 'slimscroll' );

			if ( Insight::setting( 'notice_cookie_enable' ) && ! isset( $_COOKIE['notice_cookie_confirm'] ) ) {
				wp_enqueue_script( 'growl' );
				wp_enqueue_style( 'growl' );
			}

			if ( Insight::setting( 'lazy_load_images' ) ) {
				wp_enqueue_script( 'lazyload' );
			}

			//  Enqueue styles & scripts for single portfolio pages.
			if ( is_singular() ) {

				switch ( $post_type ) {
					case 'portfolio':
						$single_portfolio_sticky = Insight::setting( 'single_portfolio_sticky_detail_enable' );
						if ( $single_portfolio_sticky == '1' ) {
							wp_enqueue_script( 'sticky-kit' );
						}

						wp_enqueue_style( 'lightgallery' );
						wp_enqueue_script( 'lightgallery' );
						break;

					case 'product':
						wp_enqueue_style( 'lightgallery' );
						wp_enqueue_script( 'lightgallery' );
						break;
				}
			}

			/*
			 * The comment-reply script.
			 */
			if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
				if ( $post_type === 'post' ) {
					if ( Insight::setting( 'single_post_comment_enable' ) === '1' ) {
						wp_enqueue_script( 'comment-reply' );
					}
				} elseif ( $post_type === 'portfolio' ) {
					if ( Insight::setting( 'single_portfolio_comment_enable' ) === '1' ) {
						wp_enqueue_script( 'comment-reply' );
					}
				} else {
					wp_enqueue_script( 'comment-reply' );
				}
			}

			$maintenance_templates = Insight_Maintenance::get_maintenance_templates_dir();

			if ( is_page_template( $maintenance_templates ) ) {
				wp_enqueue_script( 'countdown' );
				wp_enqueue_script( 'insight-maintenance', INSIGHT_THEME_URI . '/assets/js/maintenance.js', array( 'jquery' ), INSIGHT_THEME_VERSION, true );
			}

			wp_enqueue_script( 'wpb_composer_front_js' );

			/*
			 * Enqueue main JS
			 */
			wp_enqueue_script( 'insight-main', INSIGHT_THEME_URI . "/assets/js/main{$min}.js", array(
				'jquery',
			), INSIGHT_THEME_VERSION, true );

			if ( class_exists( 'WooCommerce' ) ) {
				if ( Insight::setting( 'shop_archive_quick_view' ) === '1' && ( is_shop() || is_cart() || is_product() || ( function_exists( 'is_product_taxonomy' ) && is_product_taxonomy() ) ) ) {
					wp_enqueue_style( 'magnific-popup' );
					wp_enqueue_script( 'magnific-popup' );
				}

				wp_enqueue_script( 'insight-woo', INSIGHT_THEME_URI . "/assets/js/woo{$min}.js", array(
					'insight-main',
				), INSIGHT_THEME_VERSION, true );
			}

			if ( is_page_template( 'templates/one-page-scroll.php' ) ) {
				wp_enqueue_script( 'full-page', INSIGHT_THEME_URI . '/assets/js/jquery.fullPage.js', array( 'jquery' ), null, true );
			}

			/*
			 * Enqueue custom variable JS
			 */

			$js_variables = array(
				'templateUrl'               => INSIGHT_THEME_URI,
				'ajaxurl'                   => admin_url( 'admin-ajax.php' ),
				'primary_color'             => Insight::setting( 'primary_color' ),
				'header_sticky_enable'      => Insight::setting( 'header_sticky_enable' ),
				'header_sticky_height'      => Insight::setting( 'header_sticky_height' ),
				'scroll_top_enable'         => Insight::setting( 'scroll_top_enable' ),
				'lazyLoadImages'            => Insight::setting( 'lazy_load_images' ),
				'light_gallery_auto_play'   => Insight::setting( 'light_gallery_auto_play' ),
				'light_gallery_download'    => Insight::setting( 'light_gallery_download' ),
				'light_gallery_full_screen' => Insight::setting( 'light_gallery_full_screen' ),
				'light_gallery_zoom'        => Insight::setting( 'light_gallery_zoom' ),
				'light_gallery_thumbnail'   => Insight::setting( 'light_gallery_thumbnail' ),
				'light_gallery_share'       => Insight::setting( 'light_gallery_share' ),
				'mobile_menu_breakpoint'    => Insight::setting( 'mobile_menu_breakpoint' ),
				'isSingleProduct'           => is_singular( 'product' ),
				'noticeCookieEnable'        => Insight::setting( 'notice_cookie_enable' ),
				'noticeCookieConfirm'       => isset( $_COOKIE['notice_cookie_confirm'] ) ? 'yes' : 'no',
				'noticeCookieMessages'      => wp_kses( __( 'We use cookies to ensure that we give you the best experience on our website. If you continue to use this site we will assume that you are happy with it. <a id="tm-button-cookie-notice-ok" class="tm-button tm-button-xs tm-button-full-wide tm-button-secondary style-outline">OK, GOT IT</a>', 'leomes' ), array(
					'a' => array(
						'id'    => array(),
						'class' => array(),
					),
				) ),
				'noticeCookieOKMessages'    => esc_html__( 'Thank you! Hope you have the best experience on our website.', 'leomes' ),
				'like'                      => esc_html__( 'Like', 'leomes' ),
				'unlike'                    => esc_html__( 'Unlike', 'leomes' ),
			);
			wp_localize_script( 'insight-main', '$insight', $js_variables );
		}

		/**
		 * Enqueue custom style
		 */
		public function custom_css() {
			if ( Insight::setting( 'custom_css_enable' ) ) {
				wp_add_inline_style( 'insight-style', html_entity_decode( Insight::setting( 'custom_css' ), ENT_QUOTES ) );
			}
		}

		/**
		 * Load custom JS
		 */
		public function custom_js() {
			if ( Insight::setting( 'custom_js_enable' ) == 1 ) {
				echo '<script>' . html_entity_decode( Insight::setting( 'custom_js' ) ) . '</script>';
			}
		}
	}

	new Insight_Enqueue();
}
