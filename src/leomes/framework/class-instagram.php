<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Insight_Instagram' ) ) {

	class Insight_Instagram {

		public function __construct() {

		}

		/**
		 * Quick-and-dirty Instagram web scrape
		 * based on https://gist.github.com/cosmocatalano/4544576
		 *
		 * @param     $username
		 * @param int $slice
		 *
		 * @return array|WP_Error
		 */
		public static function scrape_instagram( $username, $slice ) {
			$username      = trim( strtolower( $username ) );
			$cached_images = get_transient( 'instagram-media-new-' . sanitize_title_with_dashes( $username ) );

			// If the photos is cached, use them & early exit.
			if ( ! empty( $cached_images ) ) {
				return $cached_images;
			}

			switch ( substr( $username, 0, 1 ) ) {
				case '#':
					$url = 'https://instagram.com/explore/tags/' . str_replace( '#', '', $username );
					break;
				default:
					$url = 'https://instagram.com/' . str_replace( '@', '', $username );
					break;
			}

			$remote = wp_remote_get( $url );

			if ( is_wp_error( $remote ) ) {
				return new WP_Error( 'site_down', esc_html__( 'Unable to communicate with Instagram.', 'leomes' ) );
			}

			if ( 200 != wp_remote_retrieve_response_code( $remote ) ) {
				return new WP_Error( 'invalid_response', esc_html__( 'Instagram did not return a 200.', 'leomes' ) );
			}

			$shards      = explode( 'window._sharedData = ', $remote['body'] );
			$insta_json  = explode( ';</script>', $shards[1] );
			$insta_array = json_decode( $insta_json[0], true );

			if ( ! $insta_array ) {
				return new WP_Error( 'bad_json', esc_html__( 'Instagram has returned invalid data.', 'leomes' ) );
			}

			if ( isset( $insta_array['entry_data']['ProfilePage'][0]['user']['media']['nodes'] ) ) {
				$images = $insta_array['entry_data']['ProfilePage'][0]['user']['media']['nodes'];
			} elseif ( isset( $insta_array['entry_data']['TagPage'][0]['tag']['media']['nodes'] ) ) {
				$images = $insta_array['entry_data']['TagPage'][0]['tag']['media']['nodes'];
			} else {
				return new WP_Error( 'bad_json_2', esc_html__( 'Instagram has returned invalid data.', 'leomes' ) );
			}

			if ( ! is_array( $images ) ) {
				return new WP_Error( 'bad_array', esc_html__( 'Instagram has returned invalid data.', 'leomes' ) );
			}

			$photos_array = array();

			foreach ( $images as $image ) {
				$image['thumbnail_src'] = preg_replace( '/^https?\:/i', '', $image['thumbnail_src'] );
				$image['display_src']   = preg_replace( '/^https?\:/i', '', $image['display_src'] );

				// handle both types of CDN url.
				if ( ( strpos( $image['thumbnail_src'], 's640x640' ) !== false ) ) {
					$image['thumbnail'] = str_replace( 's640x640', 's160x160', $image['thumbnail_src'] );
					$image['small']     = str_replace( 's640x640', 's240x240', $image['thumbnail_src'] );
					$image['medium']    = str_replace( 's640x640', 's320x320', $image['thumbnail_src'] );
					$image['large']     = str_replace( 's640x640', 's480x480', $image['thumbnail_src'] );
				} else {
					$url_parts  = wp_parse_url( $image['thumbnail_src'] );
					$path_parts = explode( '/', $url_parts['path'] );
					array_splice( $path_parts, 3, 0, array( 's160x160' ) );
					$image['thumbnail'] = '//' . $url_parts['host'] . implode( '/', $path_parts );
					$path_parts[3]      = 's320x320';
					$image['small']     = '//' . $url_parts['host'] . implode( '/', $path_parts );
				}

				$photos_array[] = array(
					'description' => ! empty( $image['caption'] ) ? $image['caption'] : __( 'Instagram Image', 'leomes' ),
					'link'        => trailingslashit( '//instagram.com/p/' . $image['code'] ),
					'time'        => $image['date'],
					'comments'    => self::roundNumber( $image['comments']['count'] ),
					'likes'       => self::roundNumber( $image['likes']['count'] ),
					'thumbnail'   => $image['thumbnail'],
					'small'       => $image['small'],
					'medium'      => $image['medium'],
					'large'       => $image['large'],
					'extra_large' => $image['thumbnail_src'],
					'original'    => $image['display_src'],
					'type'        => 'image',
				);
			}

			if ( empty( $photos_array ) ) {
				return new WP_Error( 'no_images', esc_html__( 'Instagram did not return any images.', 'leomes' ) );
			}

			$photos_array = array_slice( $photos_array, 0, $slice );

			set_transient( 'instagram-media-new-' . sanitize_title_with_dashes( $username ), $photos_array, apply_filters( 'null_instagram_cache_time', HOUR_IN_SECONDS * 2 ) );

			return $photos_array;
		}

		/**
		 * Generate rounded number
		 * Example: 11200 --> 11K
		 *
		 * @param $number
		 *
		 * @return string
		 */
		public static function roundNumber( $number ) {
			if ( $number > 999 && $number <= 999999 ) {
				$result = floor( $number / 1000 ) . ' K';
			} elseif ( $number > 999999 ) {
				$result = floor( $number / 1000000 ) . ' M';
			} else {
				$result = $number;
			}

			return $result;
		}
	}

	new Insight_Instagram();
}
