<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Insight_Portfolio' ) ) {
	class Insight_Portfolio {

		public function __construct() {
			add_action( 'wp_ajax_portfolio_infinite_load', array( $this, 'infinite_load' ) );
			add_action( 'wp_ajax_nopriv_portfolio_infinite_load', array( $this, 'infinite_load' ) );
		}

		public static function is_taxonomy() {
			return is_tax( get_object_taxonomies( 'portfolio' ) );
		}

		public static function get_categories( $args = array() ) {
			$defaults = array(
				'all' => true,
			);
			$args     = wp_parse_args( $args, $defaults );
			$terms    = get_terms( array(
				'taxonomy' => 'portfolio_category',
			) );
			$results  = array();

			if ( $args['all'] === true ) {
				$results['-1'] = esc_html__( 'All', 'leomes' );
			}

			if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
				foreach ( $terms as $term ) {
					$results[ $term->slug ] = $term->name;
				}
			}

			return $results;
		}

		public static function get_tags( $args = array() ) {
			$defaults = array(
				'all' => true,
			);
			$args     = wp_parse_args( $args, $defaults );
			$terms    = get_terms( array(
				'taxonomy' => 'portfolio_tags',
			) );
			$results  = array();

			if ( $args['all'] === true ) {
				$results['-1'] = esc_html__( 'All', 'leomes' );
			}

			if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
				foreach ( $terms as $term ) {
					$results[ $term->slug ] = $term->name;
				}
			}

			return $results;
		}

		public function infinite_load() {
			$args = array(
				'post_type'      => $_POST['post_type'],
				'posts_per_page' => $_POST['posts_per_page'],
				'orderby'        => $_POST['orderby'],
				'order'          => $_POST['order'],
				'paged'          => $_POST['paged'],
				'post_status'    => 'publish',
			);

			if ( ! empty( $_POST['taxonomies'] ) ) {
				$args = Insight_VC::get_tax_query_of_taxonomies( $args, $_POST['taxonomies'] );
			}

			$style = '1';
			if ( isset( $_POST['style'] ) ) {
				$style = $_POST['style'];
			}
			$overlay_style  = $_POST['overlay_style'];
			$thumbnail_size = $_POST['thumbnail_size'];
			$count          = $_POST['count'];
			$insight_query  = new WP_Query( $args );

			if ( $insight_query->have_posts() ) : ?>
				<?php if ( $style === '1' ) { ?>
					<?php
					while ( $insight_query->have_posts() ) :
						$insight_query->the_post();
						$classes = array( 'portfolio-item grid-item' );
						?>
						<div <?php post_class( implode( ' ', $classes ) ); ?>>
							<div class="post-item-wrapper">
								<div class="post-thumbnail">
									<a href="<?php the_permalink(); ?>">
										<?php
										if ( has_post_thumbnail() ) {
											the_post_thumbnail( $thumbnail_size );
										} else {
											switch ( $thumbnail_size ) {
												case 'insight-grid-classic-square' :
													Insight_Templates::image_placeholder( 600, 600 );
													break;
												case 'insight-grid-classic-2' :
													Insight_Templates::image_placeholder( 600, 463 );
													break;
												default :
													Insight_Templates::image_placeholder( 500, 675 );
													break;
											}
										}
										?>
									</a>
									<?php if ( $overlay_style !== '' ) : ?>
										<?php get_template_part( 'loop/portfolio/overlay', $overlay_style ); ?>
									<?php endif; ?>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
				<?php } elseif ( $style === '2' ) { ?>
					<?php
					$metro_layout       = array(
						'',
						'grid-item--width2 grid-item--height2',
						'grid-item--height2',
						'',
						'',
						'grid-item--width2 grid-item--height2',
						'',
						'grid-item--height2',
						'',
						'',
					);
					$metro_layout_count = count( $metro_layout );
					$metro_item_count   = 0;
					while ( $insight_query->have_posts() ) :
						$insight_query->the_post();
						$classes    = array( 'portfolio-item grid-item' );
						$categories = get_the_term_list( get_the_ID(), 'portfolio_category', '', ', ', '' );


						$_image_size              = 'insight-grid-metro';
						$_image_placeholdit_width = 400;
						$_image_placeholdit_heigh = 400;


						$classes[] = $metro_layout[ $metro_item_count ];
						if ( $metro_layout[ $metro_item_count ] === 'grid-item--height2' ) {
							$_image_size               = 'insight-grid-metro-height-2';
							$_image_placeholdit_height = 800;
						} elseif ( $metro_layout[ $metro_item_count ] === 'grid-item--width2 grid-item--height2' ) {
							$_image_placeholdit_width  = 800;
							$_image_placeholdit_height = 800;
							$_image_size               = 'insight-grid-metro-width-2-height-2';
						}
						?>
						<div <?php post_class( implode( ' ', $classes ) ); ?>
							<?php if ( in_array( $metro_layout[ $metro_item_count ], array(
								'grid-item--width2',
								'grid-item--width2 grid-item--height2',
							), true ) ) : ?>
								data-width="2"
							<?php endif; ?>
							<?php if ( in_array( $metro_layout[ $metro_item_count ], array(
								'grid-item--height2',
								'grid-item--width2 grid-item--height2',
							), true ) ) : ?>
								data-height="2"
							<?php endif; ?>
						>
							<div class="post-item-wrapper">
								<div class="post-thumbnail">
									<?php
									if ( has_post_thumbnail() ) {
										the_post_thumbnail( $_image_size );
									} else {
										Insight_Templates::image_placeholder( $_image_placeholdit_width, 570 );
									}
									?>
									<?php if ( $overlay_style !== '' ) : ?>
										<?php get_template_part( 'loop/portfolio/overlay', $overlay_style ); ?>
									<?php endif; ?>
								</div>
							</div>
						</div>
						<?php
						$metro_item_count ++;
						if ( $metro_item_count == $count || $metro_layout_count == $metro_item_count ) {
							$metro_item_count = 0;
						}
						?>
					<?php endwhile; ?>
				<?php } elseif ( $style === '3' ) { ?>
					<?php
					while ( $insight_query->have_posts() ) :
						$insight_query->the_post();
						$classes = array( 'portfolio-item grid-item' );
						?>
						<div <?php post_class( implode( ' ', $classes ) ); ?>>
							<div class="post-item-wrapper">
								<div class="post-thumbnail">
									<?php
									if ( has_post_thumbnail() ) {
										the_post_thumbnail( 'insight-grid-masonry' );
									} else {
										Insight_Templates::image_placeholder( 570, 570 );
									}
									?>
									<?php if ( $overlay_style !== '' ) : ?>
										<?php get_template_part( 'loop/portfolio/overlay', $overlay_style ); ?>
									<?php endif; ?>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
				<?php } elseif ( $style === '4' ) { ?>
					<?php
					while ( $insight_query->have_posts() ) :
						$insight_query->the_post();
						$classes = array( 'portfolio-item grid-item swiper-slide' );
						?>
						<div <?php post_class( implode( ' ', $classes ) ); ?>>
							<div class="post-item-wrapper">
								<div class="post-thumbnail">
									<?php
									if ( has_post_thumbnail() ) {
										the_post_thumbnail( $thumbnail_size );
									} else {
										switch ( $thumbnail_size ) {
											case 'insight-grid-classic-square' :
												Insight_Templates::image_placeholder( 600, 600 );
												break;
											case 'insight-grid-classic-2' :
												Insight_Templates::image_placeholder( 600, 463 );
												break;
											default :
												Insight_Templates::image_placeholder( 500, 675 );
												break;
										}
									}
									?>
									<?php if ( $overlay_style !== '' ) : ?>
										<?php get_template_part( 'loop/portfolio/overlay', $overlay_style ); ?>
									<?php endif; ?>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
				<?php } elseif ( $style === '5' ) { ?>
					<?php
					while ( $insight_query->have_posts() ) :
						$insight_query->the_post();
						$classes = array( 'portfolio-item grid-item' );
						?>
						<div <?php post_class( implode( ' ', $classes ) ); ?>>

							<a href="<?php the_permalink(); ?>">
								<?php
								if ( has_post_thumbnail() ) {
									the_post_thumbnail( 'insight-grid-masonry' );
								} else {
									Insight_Templates::image_placeholder( 600, 600 );
								}
								?>
							</a>
							<div class="post-thumbnail">
								<?php if ( $overlay_style !== '' ) : ?>
									<?php get_template_part( 'loop/portfolio/overlay', $overlay_style ); ?>
								<?php endif; ?>
							</div>
						</div>
					<?php endwhile; ?>
				<?php } elseif ( $style === '6' ) { ?>
					<?php
					while ( $insight_query->have_posts() ) :
						$insight_query->the_post();
						$classes = array( 'portfolio-item list-item' );
						?>
						<div <?php post_class( implode( ' ', $classes ) ); ?>>
							<h5 class="post-title">
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h5>
						</div>
					<?php endwhile; ?>
				<?php } ?>
				<?php
			endif;
			wp_reset_postdata();
			wp_die();
		}
	}

	new Insight_Portfolio();
}
