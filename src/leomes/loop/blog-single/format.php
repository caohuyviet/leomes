<?php if ( has_post_thumbnail() ) { ?>
	<?php
	global $insight_vars;
	$insight_thumbnail_w = 970;
	$insight_thumbnail_h = 9999;
	if ( $insight_vars->has_sidebar ) {
		$insight_thumbnail_w = 770;
	}

	$full_image_size = get_the_post_thumbnail_url( null, 'full' );
	?>
	<div class="post-feature post-thumbnail">
		<?php
		Insight_Helper::get_lazy_load_image( array(
			'url'    => $full_image_size,
			'width'  => $insight_thumbnail_w,
			'height' => $insight_thumbnail_h,
			'crop'   => false,
			'echo'   => true,
			'alt'    => get_the_title(),
		) );
		?>

	</div>
<?php } ?>
