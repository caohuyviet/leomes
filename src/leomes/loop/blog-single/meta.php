<div class="post-meta">
	<?php if ( Insight::setting( 'single_post_author_enable' ) === '1' ) : ?>
		<div class="post-author-meta">
			<span class="ion-ios-person"></span>
			<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php the_author(); ?></a>
		</div>
	<?php endif; ?>

	<?php if ( Insight::setting( 'single_post_view_enable' ) === '1' && function_exists( 'the_views' ) ) : ?>
		<div class="post-view">
			<span class="ion-eye"></span>
			<?php the_views(); ?>
		</div>
	<?php endif; ?>

	<?php if ( Insight::setting( 'single_post_like_enable' ) === '1' ) : ?>
		<span class="post-likes">
				<?php
				$insight_post_like = new Insight_Post_Like();
				$insight_post_like->get_simple_likes_button( get_the_ID() );
				?>
			</span>
	<?php endif; ?>

	<?php if ( Insight::setting( 'single_post_date_enable' ) === '1' ) : ?>
		<div class="post-date">
			<span class="ion-android-alarm-clock"></span>
			<?php echo get_the_date(); ?></div>
	<?php endif; ?>
</div>
