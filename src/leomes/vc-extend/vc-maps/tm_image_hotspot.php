<?php

class WPBakeryShortCode_TM_Image_Hotspot extends WPBakeryShortCode {

}

vc_map( array(
	'name'                      => esc_html__( 'Image Hotspot', 'leomes' ),
	'base'                      => 'tm_image_hotspot',
	'category'                  => INSIGHT_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-blockquote',
	'allowed_container_element' => 'vc_row',
	'params'                    => array(
		array(
			'heading'    => esc_html__( 'Image Hotspot', 'leomes' ),
			'type'       => 'dropdown',
			'param_name' => 'hotspot',
			'value'      => Insight_Helper::get_list_hotspot(),
		),
	),
) );
