<?php

class WPBakeryShortCode_TM_Button extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		global $insight_shortcode_lg_css;
		global $insight_shortcode_md_css;
		global $insight_shortcode_sm_css;
		global $insight_shortcode_xs_css;

		$wrapper_tmp     = '';
		$button_tmp      = $button_hover_tmp = '';
		$primary_color   = Insight::setting( 'primary_color' );
		$secondary_color = Insight::setting( 'secondary_color' );

		if ( $atts['align'] !== '' ) {
			$wrapper_tmp .= "text-align: {$atts['align']};";
		}

		if ( $atts['md_align'] !== '' ) {
			$insight_shortcode_md_css .= "$selector { text-align: {$atts['md_align']} }";
		}

		if ( $atts['sm_align'] !== '' ) {
			$insight_shortcode_sm_css .= "$selector { text-align: {$atts['sm_align']} }";
		}

		if ( $atts['xs_align'] !== '' ) {
			$insight_shortcode_xs_css .= "$selector { text-align: {$atts['xs_align']} }";
		}

		if ( $atts['rounded'] !== '' ) {
			$button_tmp .= Insight_Helper::get_css_prefix( 'border-radius', $atts['rounded'] );
		}

		if ( $atts['size'] === 'custom' ) {
			if ( $atts['width'] !== '' ) {
				$button_tmp .= "min-width: {$atts['width']}px;";
			}

			if ( $atts['height'] !== '' ) {
				$button_tmp   .= "min-height: {$atts['height']}px;";
				$_line_height = $atts['height'];
				if ( $atts['border_width'] !== '' ) {
					$_line_height = $_line_height - ( $atts['border_width'] * 2 );
					$button_tmp   .= "border-width: {$atts['border_width']}px;";
				}

				$button_tmp .= "line-height: {$_line_height}px;";
			}
		}

		if ( isset( $atts['icon_font_size'] ) ) {
			Insight_VC::get_responsive_css( array(
				'element' => "$selector .button-icon",
				'atts'    => array(
					'font-size' => array(
						'media_str' => $atts['icon_font_size'],
						'unit'      => 'px',
					),
				),
			) );
		}

		if ( $atts['color'] === 'custom' ) {
			if ( $atts['button_bg_color'] === 'primary' ) {
				$button_tmp .= "background-color: {$primary_color};";
			} elseif ( $atts['button_bg_color'] === 'secondary' ) {
				$button_tmp .= "background-color: {$secondary_color};";
			} elseif ( $atts['button_bg_color'] === 'custom' ) {
				if ( $atts['custom_button_bg_color'] !== '' ) {
					$button_tmp .= "background-color: {$atts['custom_button_bg_color']};";
				} else {
					$button_tmp .= "background-color: transparent;";
				}
			}

			if ( $atts['font_color'] === 'primary' ) {
				$button_tmp .= "color: {$primary_color};";
			} elseif ( $atts['font_color'] === 'secondary' ) {
				$button_tmp .= "color: {$secondary_color};";
			} elseif ( $atts['font_color'] === 'custom' ) {
				if ( $atts['custom_font_color'] !== '' ) {
					$button_tmp .= "color: {$atts['custom_font_color']};";
				} else {
					$button_tmp .= "color: transparent;";
				}
			}

			if ( $atts['button_border_color'] === 'primary' ) {
				$button_tmp .= "border-color: {$primary_color};";
			} elseif ( $atts['button_border_color'] === 'secondary' ) {
				$button_tmp .= "border-color: {$secondary_color};";
			} elseif ( $atts['button_border_color'] === 'custom' ) {
				if ( $atts['custom_button_border_color'] !== '' ) {
					$button_tmp .= "border-color: {$atts['custom_button_border_color']};";
				} else {
					$button_tmp .= "border-color: transparent;";
				}
			}

			// Hover.
			if ( $atts['button_bg_color_hover'] === 'primary' ) {
				$button_hover_tmp .= "background-color: {$primary_color};";
			} elseif ( $atts['button_bg_color_hover'] === 'secondary' ) {
				$button_hover_tmp .= "background-color: {$secondary_color};";
			} elseif ( $atts['button_bg_color_hover'] === 'custom' ) {
				if ( $atts['custom_button_bg_color_hover'] !== '' ) {
					$button_hover_tmp .= "background-color: {$atts['custom_button_bg_color_hover']};";
				} else {
					$button_hover_tmp .= "background-color: transparent;";
				}
			}

			if ( $atts['font_color_hover'] === 'primary' ) {
				$button_hover_tmp .= "color: {$primary_color};";
			} elseif ( $atts['font_color_hover'] === 'secondary' ) {
				$button_hover_tmp .= "color: {$secondary_color};";
			} elseif ( $atts['font_color_hover'] === 'custom' ) {
				if ( $atts['custom_font_color_hover'] !== '' ) {
					$button_hover_tmp .= "color: {$atts['custom_font_color_hover']};";
				} else {
					$button_hover_tmp .= "color: transparent;";
				}
			}

			if ( $atts['button_border_color_hover'] === 'primary' ) {
				$button_hover_tmp .= "border-color: {$primary_color} !important;";
			} elseif ( $atts['button_border_color_hover'] === 'secondary' ) {
				$button_hover_tmp .= "border-color: {$secondary_color} !important;";
			} elseif ( $atts['button_border_color_hover'] === 'custom' ) {
				if ( $atts['custom_button_border_color_hover'] !== '' ) {
					$button_hover_tmp .= "border-color: {$atts['custom_button_border_color_hover']};";
				} else {
					$button_hover_tmp .= "border-color: transparent;";
				}
			}
		}

		if ( $wrapper_tmp !== '' ) {
			$insight_shortcode_lg_css .= "$selector { $wrapper_tmp }";
		}

		if ( $button_tmp !== '' ) {
			$insight_shortcode_lg_css .= "$selector .tm-button{ $button_tmp }";
		}

		if ( $button_hover_tmp !== '' ) {
			if ( $atts['style'] === 'text' ) {
				$insight_shortcode_lg_css .= "$selector .tm-button:hover span { $button_hover_tmp }";
			} else {
				$insight_shortcode_lg_css .= "$selector .tm-button:hover{ $button_hover_tmp }";
			}
		}

		$insight_shortcode_lg_css .= Insight_VC::get_vc_spacing_css( $selector, $atts );
	}
}

$styling_tab = esc_html__( 'Styling', 'leomes' );

vc_map( array(
	'name'     => esc_html__( 'Button', 'leomes' ),
	'base'     => 'tm_button',
	'category' => INSIGHT_VC_SHORTCODE_CATEGORY,
	'icon'     => 'insight-i insight-i-button',
	'params'   => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'leomes' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Flat', 'leomes' )    => 'flat',
				esc_html__( 'Outline', 'leomes' ) => 'outline',
				esc_html__( 'Text', 'leomes' )    => 'text',
			),
			'std'         => 'flat',
		),
		array(
			'heading'     => esc_html__( 'Size', 'leomes' ),
			'type'        => 'dropdown',
			'param_name'  => 'size',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Large', 'leomes' )       => 'lg',
				esc_html__( 'Normal', 'leomes' )      => 'nm',
				esc_html__( 'Small', 'leomes' )       => 'sm',
				esc_html__( 'Extra Small', 'leomes' ) => 'xs',
				esc_html__( 'Custom', 'leomes' )      => 'custom',
			),
			'std'         => 'nm',
		),
		array(
			'heading'     => esc_html__( 'Width', 'leomes' ),
			'description' => esc_html__( 'Controls the width of button.', 'leomes' ),
			'type'        => 'number',
			'suffix'      => 'px',
			'param_name'  => 'width',
			'dependency'  => array( 'element' => 'size', 'value' => 'custom' ),
		),
		array(
			'heading'     => esc_html__( 'Height', 'leomes' ),
			'description' => esc_html__( 'Controls the height of button.', 'leomes' ),
			'type'        => 'number',
			'suffix'      => 'px',
			'param_name'  => 'height',
			'dependency'  => array( 'element' => 'size', 'value' => 'custom' ),
		),
		array(
			'heading'     => esc_html__( 'Border Width', 'leomes' ),
			'description' => esc_html__( 'Controls the border width of button.', 'leomes' ),
			'type'        => 'number',
			'suffix'      => 'px',
			'param_name'  => 'border_width',
			'dependency'  => array( 'element' => 'size', 'value' => 'custom' ),
		),
		array(
			'heading'     => esc_html__( 'Force Full Width', 'leomes' ),
			'description' => esc_html__( 'Make button full wide.', 'leomes' ),
			'type'        => 'checkbox',
			'param_name'  => 'full_wide',
			'value'       => array( esc_html__( 'Yes', 'leomes' ) => '1' ),
		),
		array(
			'heading'     => esc_html__( 'Rounded', 'leomes' ),
			'type'        => 'textfield',
			'param_name'  => 'rounded',
			'description' => esc_html__( 'Input a valid radius. Fox Ex: 10px. Leave blank to use default.', 'leomes' ),
		),
		array(
			'heading'    => esc_html__( 'Button', 'leomes' ),
			'type'       => 'vc_link',
			'param_name' => 'button',
			'value'      => esc_html__( 'Button', 'leomes' ),
		),
		array(
			'group'      => esc_html__( 'Icon', 'leomes' ),
			'heading'    => esc_html__( 'Icon Align', 'leomes' ),
			'type'       => 'dropdown',
			'param_name' => 'icon_align',
			'value'      => array(
				esc_html__( 'Left', 'leomes' )  => 'left',
				esc_html__( 'Right', 'leomes' ) => 'right',
			),
			'std'        => 'right',
		),
		array(
			'heading'     => esc_html__( 'Button Action', 'leomes' ),
			'description' => esc_html__( 'To make smooth scroll action work then input button url like this: #about-us-section. )', 'leomes' ),
			'type'        => 'dropdown',
			'param_name'  => 'action',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Default', 'leomes' )                    => '',
				esc_html__( 'Smooth scroll to a section', 'leomes' ) => 'smooth_scroll',
				esc_html__( 'Open link as popup video', 'leomes' )   => 'popup_video',

			),
			'std'         => '',
		),
	), Insight_VC::get_alignment_fields(), array(
		Insight_VC::get_animation_field(),
		Insight_VC::extra_class_field(),
		Insight_VC::extra_id_field(),
	), Insight_VC::icon_libraries( array(
		'allow_none' => true,
	) ), array(
		array(
			'group'       => $styling_tab,
			'heading'     => esc_html__( 'Icon Font Size', 'leomes' ),
			'type'        => 'number_responsive',
			'param_name'  => 'icon_font_size',
			'min'         => 8,
			'suffix'      => 'px',
			'media_query' => array(
				'lg' => '',
				'md' => '',
				'sm' => '',
				'xs' => '',
			),
		),
		array(
			'group'       => $styling_tab,
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'Color', 'leomes' ),
			'param_name'  => 'color',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Primary', 'leomes' )   => 'primary',
				esc_html__( 'Secondary', 'leomes' ) => 'secondary',
				esc_html__( 'Grey', 'leomes' )      => 'grey',
				esc_html__( 'Custom', 'leomes' )    => 'custom',
			),
			'std'         => 'primary',
		),
		array(
			'group'            => $styling_tab,
			'type'             => 'dropdown',
			'heading'          => esc_html__( 'Background color', 'leomes' ),
			'param_name'       => 'button_bg_color',
			'value'            => array(
				esc_html__( 'Default', 'leomes' )   => '',
				esc_html__( 'Primary', 'leomes' )   => 'primary',
				esc_html__( 'Secondary', 'leomes' ) => 'secondary',
				esc_html__( 'Custom', 'leomes' )    => 'custom',
			),
			'std'              => 'default',
			'dependency'       => array(
				'element' => 'color',
				'value'   => 'custom',
			),
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $styling_tab,
			'type'             => 'colorpicker',
			'heading'          => esc_html__( 'Custom background color', 'leomes' ),
			'param_name'       => 'custom_button_bg_color',
			'dependency'       => array(
				'element' => 'button_bg_color',
				'value'   => 'custom',
			),
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $styling_tab,
			'type'             => 'dropdown',
			'heading'          => esc_html__( 'Text color', 'leomes' ),
			'param_name'       => 'font_color',
			'value'            => array(
				esc_html__( 'Default', 'leomes' )   => '',
				esc_html__( 'Primary', 'leomes' )   => 'primary',
				esc_html__( 'Secondary', 'leomes' ) => 'secondary',
				esc_html__( 'Custom', 'leomes' )    => 'custom',
			),
			'std'              => 'default',
			'dependency'       => array(
				'element' => 'color',
				'value'   => 'custom',
			),
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $styling_tab,
			'type'             => 'colorpicker',
			'heading'          => esc_html__( 'Custom text color', 'leomes' ),
			'param_name'       => 'custom_font_color',
			'dependency'       => array(
				'element' => 'font_color',
				'value'   => 'custom',
			),
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $styling_tab,
			'type'             => 'dropdown',
			'heading'          => esc_html__( 'Border color', 'leomes' ),
			'param_name'       => 'button_border_color',
			'value'            => array(
				esc_html__( 'Default', 'leomes' )   => '',
				esc_html__( 'Primary', 'leomes' )   => 'primary',
				esc_html__( 'Secondary', 'leomes' ) => 'secondary',
				esc_html__( 'Custom', 'leomes' )    => 'custom',
			),
			'std'              => 'default',
			'dependency'       => array(
				'element' => 'color',
				'value'   => 'custom',
			),
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $styling_tab,
			'type'             => 'colorpicker',
			'heading'          => esc_html__( 'Custom Border color', 'leomes' ),
			'param_name'       => 'custom_button_border_color',
			'dependency'       => array(
				'element' => 'button_border_color',
				'value'   => 'custom',
			),
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $styling_tab,
			'type'             => 'dropdown',
			'heading'          => esc_html__( 'Background color (on hover)', 'leomes' ),
			'param_name'       => 'button_bg_color_hover',
			'value'            => array(
				esc_html__( 'Default', 'leomes' )   => '',
				esc_html__( 'Primary', 'leomes' )   => 'primary',
				esc_html__( 'Secondary', 'leomes' ) => 'secondary',
				esc_html__( 'Custom', 'leomes' )    => 'custom',
			),
			'std'              => 'default',
			'dependency'       => array(
				'element' => 'color',
				'value'   => 'custom',
			),
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $styling_tab,
			'type'             => 'colorpicker',
			'heading'          => esc_html__( 'Custom background color (on hover)', 'leomes' ),
			'param_name'       => 'custom_button_bg_color_hover',
			'dependency'       => array(
				'element' => 'button_bg_color_hover',
				'value'   => 'custom',
			),
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $styling_tab,
			'type'             => 'dropdown',
			'heading'          => esc_html__( 'Text color (on hover)', 'leomes' ),
			'param_name'       => 'font_color_hover',
			'value'            => array(
				esc_html__( 'Default', 'leomes' )   => '',
				esc_html__( 'Primary', 'leomes' )   => 'primary',
				esc_html__( 'Secondary', 'leomes' ) => 'secondary',
				esc_html__( 'Custom', 'leomes' )    => 'custom',
			),
			'std'              => 'default',
			'dependency'       => array(
				'element' => 'color',
				'value'   => 'custom',
			),
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $styling_tab,
			'type'             => 'colorpicker',
			'heading'          => esc_html__( 'Custom Text color (on hover)', 'leomes' ),
			'param_name'       => 'custom_font_color_hover',
			'dependency'       => array(
				'element' => 'font_color_hover',
				'value'   => 'custom',
			),
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $styling_tab,
			'type'             => 'dropdown',
			'heading'          => esc_html__( 'Border color (on hover)', 'leomes' ),
			'param_name'       => 'button_border_color_hover',
			'value'            => array(
				esc_html__( 'Default', 'leomes' )   => '',
				esc_html__( 'Primary', 'leomes' )   => 'primary',
				esc_html__( 'Secondary', 'leomes' ) => 'secondary',
				esc_html__( 'Custom', 'leomes' )    => 'custom',
			),
			'std'              => 'default',
			'dependency'       => array(
				'element' => 'color',
				'value'   => 'custom',
			),
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $styling_tab,
			'type'             => 'colorpicker',
			'heading'          => esc_html__( 'Custom Border color (on hover)', 'leomes' ),
			'param_name'       => 'custom_button_border_color_hover',
			'dependency'       => array(
				'element' => 'button_border_color_hover',
				'value'   => 'custom',
			),
			'edit_field_class' => 'vc_col-sm-6',
		),
	),

		Insight_VC::get_vc_spacing_tab() ),
) );
