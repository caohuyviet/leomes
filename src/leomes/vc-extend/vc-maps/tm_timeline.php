<?php

class WPBakeryShortCode_TM_Timeline extends WPBakeryShortCode {

}

vc_map( array(
	'name'                      => esc_html__( 'Timeline', 'leomes' ),
	'base'                      => 'tm_timeline',
	'category'                  => INSIGHT_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-timeline',
	'allowed_container_element' => 'vc_row',
	'params'                    => array(
		array(
			'group'      => esc_html__( 'Items', 'leomes' ),
			'heading'    => esc_html__( 'Items', 'leomes' ),
			'type'       => 'param_group',
			'param_name' => 'items',
			'params'     => array(
				array(
					'heading'    => esc_html__( 'Image', 'leomes' ),
					'type'       => 'attach_image',
					'param_name' => 'image',
				),
				array(
					'heading'     => esc_html__( 'Title', 'leomes' ),
					'type'        => 'textfield',
					'param_name'  => 'title',
					'admin_label' => true,
				),
				array(
					'heading'    => esc_html__( 'Time', 'leomes' ),
					'type'       => 'textfield',
					'param_name' => 'time',
				),
				array(
					'heading'    => esc_html__( 'Text', 'leomes' ),
					'type'       => 'textarea',
					'param_name' => 'text',
				),
			),

		),
	),
) );
