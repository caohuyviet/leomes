<?php

class WPBakeryShortCode_TM_Mailchimp_Form extends WPBakeryShortCode {

}

vc_map( array(
	'name'                      => esc_html__( 'Mailchimp Form', 'leomes' ),
	'base'                      => 'tm_mailchimp_form',
	'category'                  => INSIGHT_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-mailchimp-form',
	'allowed_container_element' => 'vc_row',
	'params'                    => array(
		array(
			'heading'     => esc_html__( 'Form Id', 'leomes' ),
			'description' => esc_html__( 'Input the id of form. Leave blank to show default form.', 'leomes' ),
			'type'        => 'textfield',
			'param_name'  => 'form_id',
		),
		array(
			'heading'     => esc_html__( 'Style', 'leomes' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( '1', 'leomes' ) => '1',
				esc_html__( '2', 'leomes' ) => '2',
				esc_html__( '3', 'leomes' ) => '3',
			),
			'std'         => '1',
		),
		Insight_VC::extra_class_field(),
	),
) );
