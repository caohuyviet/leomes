<?php

class WPBakeryShortCode_TM_Team_Member extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		global $insight_shortcode_lg_css;

		$insight_shortcode_lg_css .= Insight_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'                      => esc_html__( 'Team Member', 'leomes' ),
	'base'                      => 'tm_team_member',
	'category'                  => INSIGHT_VC_SHORTCODE_CATEGORY,
	'allowed_container_element' => 'vc_row,tm_team_member_group',
	'icon'                      => 'insight-i insight-i-member',
	'params'                    => array_merge( array(
		array(
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'Style', 'leomes' ),
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( '1', 'leomes' ) => '1',
				esc_html__( '2', 'leomes' ) => '2',
			),
			'std'         => '1',
		),
		array(
			'type'        => 'attach_image',
			'heading'     => esc_html__( 'Photo of member', 'leomes' ),
			'param_name'  => 'photo',
			'admin_label' => true,
		),
		array(
			'type'       => 'dropdown',
			'heading'    => esc_html__( 'Photo Effect', 'leomes' ),
			'param_name' => 'photo_effect',
			'value'      => array(
				esc_html__( 'None', 'leomes' )      => '',
				esc_html__( 'Grayscale', 'leomes' ) => 'grayscale',
			),
			'dependency' => array(
				'element' => 'style',
				'value'   => array( '1' ),
			),
			'std'        => '',
		),
		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( 'Name', 'leomes' ),
			'admin_label' => true,
			'param_name'  => 'name',
		),
		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( 'Position', 'leomes' ),
			'param_name'  => 'position',
			'description' => esc_html__( 'Example: CEO/Founder', 'leomes' ),
		),
		array(
			'type'       => 'textarea',
			'heading'    => esc_html__( 'Description', 'leomes' ),
			'param_name' => 'desc',
		),
		array(
			'type'       => 'textfield',
			'heading'    => esc_html__( 'Profile url', 'leomes' ),
			'param_name' => 'profile',
		),
		Insight_VC::get_animation_field(),
		Insight_VC::extra_class_field(),
		array(
			'group'      => esc_html__( 'Social Networks', 'leomes' ),
			'type'       => 'param_group',
			'heading'    => esc_html__( 'Social Networks', 'leomes' ),
			'param_name' => 'social_networks',
			'params'     => array_merge( Insight_VC::icon_libraries( array( 'allow_none' => true ) ), array(
				array(
					'type'        => 'textfield',
					'heading'     => esc_html__( 'Link', 'leomes' ),
					'param_name'  => 'link',
					'admin_label' => true,
				),
			) ),
			'value'      => rawurlencode( wp_json_encode( array(
				array(
					'link'      => '#',
					'icon_type' => 'ion',
					'icon_ion'  => 'ion-social-twitter',
				),
				array(
					'link'      => '#',
					'icon_type' => 'ion',
					'icon_ion'  => 'ion-social-facebook',
				),
				array(
					'link'      => '#',
					'icon_type' => 'ion',
					'icon_ion'  => 'ion-social-googleplus',
				),
				array(
					'link'      => '#',
					'icon_type' => 'ion',
					'icon_ion'  => 'ion-social-linkedin',
				),
			) ) ),
		),
	), Insight_VC::get_vc_spacing_tab() ),
) );
