<?php

class WPBakeryShortCode_TM_Popup_Video extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		global $insight_shortcode_lg_css;

		$insight_shortcode_lg_css .= Insight_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'                      => esc_html__( 'Popup Video', 'leomes' ),
	'base'                      => 'tm_popup_video',
	'category'                  => INSIGHT_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-video',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'leomes' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Poster Style 01', 'leomes' ) => 'poster-01',
				esc_html__( 'Poster Style 02', 'leomes' ) => 'poster-02',
				esc_html__( 'Poster Style 03', 'leomes' ) => 'poster-03',
				esc_html__( 'Button Style 01', 'leomes' ) => 'button',
				esc_html__( 'Button Style 02', 'leomes' ) => 'button-02',
				esc_html__( 'Button Style 03', 'leomes' ) => 'button-03',
			),
			'std'         => 'poster-01',
		),
		array(
			'heading'    => esc_html__( 'Video Url', 'leomes' ),
			'type'       => 'textfield',
			'param_name' => 'video',
		),
		array(
			'heading'    => esc_html__( 'Video Text', 'leomes' ),
			'type'       => 'textfield',
			'param_name' => 'video_text',
			'dependency' => array(
				'element' => 'style',
				'value'   => array( 'button', 'button-02' ),
			),
		),
		array(
			'heading'    => esc_html__( 'Poster Image', 'leomes' ),
			'type'       => 'attach_image',
			'param_name' => 'poster',
			'dependency' => array(
				'element' => 'style',
				'value'   => array( 'poster-01', 'poster-02', 'poster-03' ),
			),
		),
		array(
			'heading'     => esc_html__( 'Poster Image Size', 'leomes' ),
			'description' => esc_html__( 'Controls the size of poster image.', 'leomes' ),
			'type'        => 'dropdown',
			'param_name'  => 'image_size',
			'value'       => array(
				esc_html__( 'Full', 'leomes' )    => 'full',
				esc_html__( '570x364', 'leomes' ) => '570x364',
			),
			'std'         => '570x364',
			'dependency'  => array(
				'element' => 'style',
				'value'   => array( 'poster-01', 'poster-02', 'poster-03' ),
			),
		),
		Insight_VC::extra_class_field(),
	), Insight_VC::get_vc_spacing_tab() ),
) );
