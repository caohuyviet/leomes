<?php
$_color_field                                                = WPBMap::getParam( 'vc_tta_tabs', 'color' );
$_color_field['value'][ esc_html__( 'Primary', 'leomes' ) ] = 'primary';
$_color_field['std']                                         = 'primary';
vc_update_shortcode_param( 'vc_tta_tabs', $_color_field );

vc_update_shortcode_param( 'vc_tta_tabs', array(
	'param_name' => 'style',
	'value'      => array(
		esc_html__( 'leomes 01', 'leomes' ) => 'leomes-01',
		esc_html__( 'leomes 02', 'leomes' ) => 'leomes-02',
		esc_html__( 'leomes 03', 'leomes' ) => 'leomes-03',
		esc_html__( 'Classic', 'leomes' )    => 'classic',
		esc_html__( 'Modern', 'leomes' )     => 'modern',
		esc_html__( 'Flat', 'leomes' )       => 'flat',
		esc_html__( 'Outline', 'leomes' )    => 'outline',
	),
) );
