<?php

class WPBakeryShortCode_TM_Team_Member_Group extends WPBakeryShortCodesContainer {

	public function get_inline_css( $selector = '', $atts ) {
		global $insight_shortcode_lg_css;

		$insight_shortcode_lg_css .= Insight_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'                      => esc_html__( 'Team Member Group', 'leomes' ),
	'base'                      => 'tm_team_member_group',
	'as_parent'                 => array( 'only' => 'tm_team_member' ),
	'content_element'           => true,
	'show_settings_on_create'   => false,
	'is_container'              => true,
	'category'                  => INSIGHT_VC_SHORTCODE_CATEGORY,
	'allowed_container_element' => 'vc_row',
	'icon'                      => 'insight-i insight-i-member',
	'js_view'                   => 'VcColumnView',
	'params'                    => array_merge( array(
		array(
			'type'       => 'dropdown',
			'heading'    => esc_html__( 'Style', 'leomes' ),
			'param_name' => 'style',
			'value'      => array(
				esc_html__( 'Carousel ZigZag', 'leomes' ) => 'carousel_zigzag',
			),
			'std'        => 'carousel_zigzag',
		),
		Insight_VC::extra_class_field(),
	), Insight_VC::get_vc_spacing_tab() ),
) );
