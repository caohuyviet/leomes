<?php

class WPBakeryShortCode_TM_Slider extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		global $insight_shortcode_lg_css;
		$slide_tmp = '';

		if ( isset( $atts['text_align'] ) && $atts['text_align'] !== '' ) {
			$slide_tmp .= "text-align: {$atts['text_align']};";
		}

		if ( $slide_tmp !== '' ) {
			$insight_shortcode_lg_css .= "$selector .swiper-slide { $slide_tmp }";
		}

		$insight_shortcode_lg_css .= Insight_VC::get_vc_spacing_css( $selector, $atts );
	}
}

$slides_tab  = esc_html__( 'Slides', 'leomes' );
$styling_tab = esc_html__( 'Styling', 'leomes' );

vc_map( array(
	'name'                      => esc_html__( 'Slider', 'leomes' ),
	'base'                      => 'tm_slider',
	'category'                  => INSIGHT_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-carousel',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'heading'    => esc_html__( 'Style', 'leomes' ),
			'type'       => 'dropdown',
			'param_name' => 'style',
			'value'      => array(
				esc_html__( '01', 'leomes' ) => '1',
				esc_html__( '02', 'leomes' )  => '2',
			),
			'std' => '1',
		),
		array(
			'heading'    => esc_html__( 'Image Size', 'leomes' ),
			'type'       => 'dropdown',
			'param_name' => 'image_size',
			'value'      => array(
				esc_html__( '1170x560 (1 Column)', 'leomes' ) => '1170x560',
				esc_html__( '600x400 (1 Column)', 'leomes' )  => '600x400',
				esc_html__( '500x338 (3 Columns)', 'leomes' ) => '500x338',
				esc_html__( '500x676 (3 Columns)', 'leomes' ) => '500x676',
				esc_html__( 'Full', 'leomes' )                => 'full',
			),
			'std'        => '500x338',
		),
		array(
			'heading'    => esc_html__( 'Auto Height', 'leomes' ),
			'type'       => 'checkbox',
			'param_name' => 'auto_height',
			'value'      => array( esc_html__( 'Yes', 'leomes' ) => '1' ),
			'std'        => '1',
		),
		array(
			'heading'    => esc_html__( 'Loop', 'leomes' ),
			'type'       => 'checkbox',
			'param_name' => 'loop',
			'value'      => array( esc_html__( 'Yes', 'leomes' ) => '1' ),
			'std'        => '1',
		),
		array(
			'heading'     => esc_html__( 'Auto Play', 'leomes' ),
			'description' => esc_html__( 'Delay between transitions (in ms), ex: 3000. Leave blank to disabled.', 'leomes' ),
			'type'        => 'number',
			'suffix'      => 'ms',
			'param_name'  => 'auto_play',
		),
		array(
			'heading'    => esc_html__( 'Equal Height', 'leomes' ),
			'type'       => 'checkbox',
			'param_name' => 'equal_height',
			'value'      => array( esc_html__( 'Yes', 'leomes' ) => '1' ),
		),
		array(
			'heading'    => esc_html__( 'Vertically Center', 'leomes' ),
			'type'       => 'checkbox',
			'param_name' => 'v_center',
			'value'      => array( esc_html__( 'Yes', 'leomes' ) => '1' ),
		),
		array(
			'heading'    => esc_html__( 'Navigation', 'leomes' ),
			'type'       => 'dropdown',
			'param_name' => 'nav',
			'value'      => Insight_VC::get_slider_navs(),
			'std'        => '',
		),
		array(
			'heading'    => esc_html__( 'Pagination', 'leomes' ),
			'type'       => 'dropdown',
			'param_name' => 'pagination',
			'value'      => Insight_VC::get_slider_dots(),
			'std'        => '',
		),
		array(
			'heading'    => esc_html__( 'Gutter', 'leomes' ),
			'type'       => 'number',
			'param_name' => 'gutter',
			'std'        => 30,
			'min'        => 0,
			'max'        => 50,
			'step'       => 1,
			'suffix'     => 'px',
		),
		array(
			'heading'     => esc_html__( 'Items Display', 'leomes' ),
			'type'        => 'number_responsive',
			'param_name'  => 'items_display',
			'min'         => 1,
			'max'         => 10,
			'suffix'      => 'item (s)',
			'media_query' => array(
				'lg' => 3,
				'md' => 3,
				'sm' => 2,
				'xs' => 1,
			),
		),
		array(
			'heading'    => esc_html__( 'Full-width Image', 'leomes' ),
			'type'       => 'checkbox',
			'param_name' => 'fw_image',
			'value'      => array( esc_html__( 'Yes', 'leomes' ) => '1' ),
		),
		Insight_VC::extra_class_field(),
		array(
			'group'      => $slides_tab,
			'heading'    => esc_html__( 'Slides', 'leomes' ),
			'type'       => 'param_group',
			'param_name' => 'items',
			'params'     => array(
				array(
					'heading'     => esc_html__( 'Image', 'leomes' ),
					'type'        => 'attach_image',
					'param_name'  => 'image',
					'admin_label' => true,
				),
				array(
					'heading'     => esc_html__( 'Sub Title', 'leomes' ),
					'type'        => 'textfield',
					'param_name'  => 'sub_title',
				),
				array(
					'heading'     => esc_html__( 'Title', 'leomes' ),
					'type'        => 'textfield',
					'param_name'  => 'title',
					'admin_label' => true,
				),
				array(
					'heading'    => esc_html__( 'Text', 'leomes' ),
					'type'       => 'textarea',
					'param_name' => 'text',
				),
				array(
					'heading'    => esc_html__( 'Link', 'leomes' ),
					'type'       => 'vc_link',
					'param_name' => 'link',
					'value'      => esc_html__( 'Link', 'leomes' ),
				),
			),
		),
		array(
			'group'      => $styling_tab,
			'heading'    => esc_html__( 'Text Align', 'leomes' ),
			'type'       => 'dropdown',
			'param_name' => 'text_align',
			'value'      => array(
				esc_html__( 'Default', 'leomes' ) => '',
				esc_html__( 'Left', 'leomes' )    => 'left',
				esc_html__( 'Center', 'leomes' )  => 'center',
				esc_html__( 'Right', 'leomes' )   => 'right',
				esc_html__( 'Justify', 'leomes' ) => 'justify',
			),
			'std'        => '',

		),
	), Insight_VC::get_vc_spacing_tab() ),
) );
