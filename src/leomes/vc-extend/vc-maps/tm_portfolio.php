<?php

class WPBakeryShortCode_TM_Portfolio extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		global $insight_shortcode_lg_css;
		global $insight_shortcode_md_css;
		global $insight_shortcode_sm_css;
		global $insight_shortcode_xs_css;
		extract( $atts );

		if ( isset( $atts['carousel_height'] ) && $atts['carousel_height'] !== '' ) {
			$arr = explode( ';', $atts['carousel_height'] );

			foreach ( $arr as $value ) {
				$tmp = explode( ':', $value );
				if ( $tmp['0'] === 'lg' ) {
					$insight_shortcode_lg_css .= "$selector .swiper-slide img { height: {$tmp['1']}px; }";
				} elseif ( $tmp['0'] === 'md' ) {
					$insight_shortcode_md_css .= "$selector .swiper-slide img { height: {$tmp['1']}px; }";
				} elseif ( $tmp['0'] === 'sm' ) {
					$insight_shortcode_sm_css .= "$selector .swiper-slide img { height: {$tmp['1']}px; }";
				} elseif ( $tmp['0'] === 'xs' ) {
					$insight_shortcode_xs_css .= "$selector .swiper-slide img { height: {$tmp['1']}px; }";
				}
			}
		}

		$image_tmp = '';

		if ( $custom_styling_enable === '1' ) {
			Insight_VC::get_responsive_css( array(
				'element' => "$selector .post-overlay-title",
				'atts'    => array(
					'font-size' => array(
						'media_str' => $overlay_title_font_size,
						'unit'      => 'px',
					),
				),
			) );

			if ( isset( $atts['image_rounded'] ) && $atts['image_rounded'] !== '' ) {
				$image_tmp .= Insight_Helper::get_css_prefix( 'border-radius', $atts['image_rounded'] );
			}
		}

		if ( $image_tmp !== '' ) {
			$insight_shortcode_lg_css .= "$selector .post-thumbnail img { {$image_tmp} }";
		}

		$insight_shortcode_lg_css .= Insight_VC::get_vc_spacing_css( $selector, $atts );
	}
}

$carousel_tab = esc_html__( 'Carousel Settings', 'leomes' );
$styling_tab  = esc_html__( 'Styling', 'leomes' );

vc_map( array(
	'name'     => esc_html__( 'Portfolio', 'leomes' ),
	'base'     => 'tm_portfolio',
	'category' => INSIGHT_VC_SHORTCODE_CATEGORY,
	'icon'     => 'insight-i insight-i-portfoliogrid',
	'params'   => array_merge( array(
		array(
			'heading'     => esc_html__( 'Portfolio Style', 'leomes' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Grid Classic', 'leomes' )         => 'grid',
				esc_html__( 'Grid Metro', 'leomes' )           => 'metro',
				esc_html__( 'Grid Masonry', 'leomes' )         => 'masonry',
				esc_html__( 'Carousel Slider', 'leomes' )      => 'carousel',
				esc_html__( 'Full Wide Slider', 'leomes' )     => 'full-wide-slider',
				esc_html__( 'Grid Justify Gallery', 'leomes' ) => 'justified',
			),
			'std'         => 'grid',
		),
		array(
			'heading'    => esc_html__( 'Metro Layout', 'leomes' ),
			'type'       => 'param_group',
			'param_name' => 'metro_layout',
			'params'     => array(
				array(
					'heading'     => esc_html__( 'Item Size', 'leomes' ),
					'type'        => 'dropdown',
					'param_name'  => 'size',
					'admin_label' => true,
					'value'       => array(
						esc_html__( 'Width 1 - Height 1', 'leomes' ) => '1:1',
						esc_html__( 'Width 1 - Height 2', 'leomes' ) => '1:2',
						esc_html__( 'Width 2 - Height 1', 'leomes' ) => '2:1',
						esc_html__( 'Width 2 - Height 2', 'leomes' ) => '2:2',
					),
					'std'         => '1:1',
				),
			),
			'value'      => rawurlencode( wp_json_encode( array(
				array(
					'size' => '1:1',
				),
				array(
					'size' => '2:2',
				),
				array(
					'size' => '1:2',
				),
				array(
					'size' => '1:1',
				),
				array(
					'size' => '1:1',
				),
				array(
					'size' => '2:1',
				),
				array(
					'size' => '1:1',
				),
			) ) ),
			'dependency' => array(
				'element' => 'style',
				'value'   => array( 'metro' ),
			),
		),
		array(
			'heading'     => esc_html__( 'Columns', 'leomes' ),
			'type'        => 'number_responsive',
			'param_name'  => 'columns',
			'min'         => 1,
			'max'         => 6,
			'step'        => 1,
			'suffix'      => '',
			'media_query' => array(
				'lg' => '3',
				'md' => '',
				'sm' => '2',
				'xs' => '1',
			),
			'dependency'  => array(
				'element' => 'style',
				'value'   => array(
					'grid',
					'metro',
					'masonry',
				),
			),
		),
		array(
			'heading'     => esc_html__( 'Grid Gutter', 'leomes' ),
			'description' => esc_html__( 'Controls the gutter of grid.', 'leomes' ),
			'type'        => 'number',
			'param_name'  => 'gutter',
			'std'         => 30,
			'min'         => 0,
			'max'         => 100,
			'step'        => 1,
			'suffix'      => 'px',
			'dependency'  => array(
				'element' => 'style',
				'value'   => array(
					'grid',
					'metro',
					'masonry',
					'justified',
				),
			),
		),
		array(
			'heading'    => esc_html__( 'Image Size', 'leomes' ),
			'type'       => 'dropdown',
			'param_name' => 'image_size',
			'value'      => array(
				esc_html__( '480x480', 'leomes' ) => '480x480',
				esc_html__( '480x311', 'leomes' ) => '480x311',
				esc_html__( '481x325', 'leomes' ) => '481x325',
				esc_html__( '500x324', 'leomes' ) => '500x324',
			),
			'std'        => '480x480',
			'dependency' => array(
				'element' => 'style',
				'value'   => array(
					'grid',
				),
			),
		),
		array(
			'heading'     => esc_html__( 'Row Height', 'leomes' ),
			'description' => esc_html__( 'Controls the height of grid row.', 'leomes' ),
			'type'        => 'number',
			'param_name'  => 'justify_row_height',
			'std'         => 300,
			'min'         => 50,
			'max'         => 500,
			'step'        => 10,
			'suffix'      => 'px',
			'dependency'  => array(
				'element' => 'style',
				'value'   => array( 'justified' ),
			),
		),
		array(
			'heading'     => esc_html__( 'Max Row Height', 'leomes' ),
			'description' => esc_html__( 'Controls the max height of grid row. Leave blank or 0 keep it disabled.', 'leomes' ),
			'type'        => 'number',
			'param_name'  => 'justify_max_row_height',
			'std'         => 0,
			'min'         => 0,
			'max'         => 500,
			'step'        => 10,
			'suffix'      => 'px',
			'dependency'  => array(
				'element' => 'style',
				'value'   => array( 'justified' ),
			),
		),
		array(
			'heading'    => esc_html__( 'Last row alignment', 'leomes' ),
			'type'       => 'dropdown',
			'param_name' => 'justify_last_row_alignment',
			'value'      => array(
				esc_html__( 'Justify', 'leomes' )                              => 'justify',
				esc_html__( 'Left', 'leomes' )                                 => 'nojustify',
				esc_html__( 'Center', 'leomes' )                               => 'center',
				esc_html__( 'Right', 'leomes' )                                => 'right',
				esc_html__( 'Hide ( if row can not be justified )', 'leomes' ) => 'hide',
			),
			'std'        => 'justify',
			'dependency' => array(
				'element' => 'style',
				'value'   => array( 'justified' ),
			),
		),
		array(
			'heading'    => esc_html__( 'Overlay Style', 'leomes' ),
			'type'       => 'dropdown',
			'param_name' => 'overlay_style',
			'value'      => array(
				esc_html__( 'None', 'leomes' )                             => 'none',
				esc_html__( 'Modern', 'leomes' )                           => 'modern',
				esc_html__( 'Image zoom - content below', 'leomes' )       => 'zoom',
				esc_html__( 'Zoom and Move Up - content below', 'leomes' ) => 'zoom2',
				esc_html__( 'Faded', 'leomes' )                            => 'faded',
				esc_html__( 'Faded - Light', 'leomes' )                    => 'faded-light',
			),
			'std'        => 'faded-light',
			'dependency' => array(
				'element' => 'style',
				'value'   => array(
					'grid',
					'metro',
					'masonry',
					'carousel',
					'justified',
				),
			),
		),
		Insight_VC::get_animation_field( array(
			'std'        => 'move-up',
			'dependency' => array(
				'element' => 'style',
				'value'   => array(
					'grid',
					'metro',
					'masonry',
					'justified',
				),
			),
		) ),
		Insight_VC::extra_class_field(),
		array(
			'group'       => $carousel_tab,
			'heading'     => esc_html__( 'Auto Play', 'leomes' ),
			'description' => esc_html__( 'Delay between transitions (in ms), ex: 3000. Leave blank to disabled.', 'leomes' ),
			'type'        => 'number',
			'suffix'      => 'ms',
			'param_name'  => 'carousel_auto_play',
			'dependency'  => array(
				'element' => 'style',
				'value'   => array(
					'carousel',
					'full-wide-slider',
				),
			),
		),
		array(
			'group'      => $carousel_tab,
			'heading'    => esc_html__( 'Navigation', 'leomes' ),
			'type'       => 'dropdown',
			'param_name' => 'carousel_nav',
			'value'      => Insight_VC::get_slider_navs(),
			'std'        => '',
			'dependency' => array(
				'element' => 'style',
				'value'   => array(
					'carousel',
					'full-wide-slider',
				),
			),
		),
		array(
			'group'      => $carousel_tab,
			'heading'    => esc_html__( 'Pagination', 'leomes' ),
			'type'       => 'dropdown',
			'param_name' => 'carousel_pagination',
			'value'      => Insight_VC::get_slider_dots(),
			'std'        => '',
			'dependency' => array(
				'element' => 'style',
				'value'   => array(
					'carousel',
					'full-wide-slider',
				),
			),
		),
		array(
			'group'      => $carousel_tab,
			'heading'    => esc_html__( 'Gutter', 'leomes' ),
			'type'       => 'number',
			'param_name' => 'carousel_gutter',
			'std'        => 30,
			'min'        => 0,
			'max'        => 50,
			'step'       => 1,
			'suffix'     => 'px',
			'dependency' => array(
				'element' => 'style',
				'value'   => array(
					'carousel',
				),
			),
		),
		array(
			'group'       => $carousel_tab,
			'heading'     => esc_html__( 'Items Display', 'leomes' ),
			'type'        => 'number_responsive',
			'param_name'  => 'carousel_items_display',
			'min'         => 1,
			'max'         => 10,
			'suffix'      => 'item (s)',
			'media_query' => array(
				'lg' => 3,
				'md' => 3,
				'sm' => 2,
				'xs' => 1,
			),
			'dependency'  => array(
				'element' => 'style',
				'value'   => 'carousel',
			),
		),
		array(
			'group'      => esc_html__( 'Data Settings', 'leomes' ),
			'type'       => 'hidden',
			'param_name' => 'main_query',
			'std'        => '',
		),
		array(
			'group'       => esc_html__( 'Data Settings', 'leomes' ),
			'heading'     => esc_html__( 'Items per page', 'leomes' ),
			'description' => esc_html__( 'Number of items to show per page.', 'leomes' ),
			'type'        => 'number',
			'param_name'  => 'number',
			'std'         => 9,
			'min'         => 1,
			'max'         => 100,
			'step'        => 1,
		),
		array(
			'group'              => esc_html__( 'Data Settings', 'leomes' ),
			'heading'            => esc_html__( 'Narrow data source', 'leomes' ),
			'description'        => esc_html__( 'Enter categories, tags or custom taxonomies.', 'leomes' ),
			'type'               => 'autocomplete',
			'param_name'         => 'taxonomies',
			'settings'           => array(
				'multiple'       => true,
				'min_length'     => 1,
				'groups'         => true,
				// In UI show results grouped by groups, default false.
				'unique_values'  => true,
				// In UI show results except selected. NB! You should manually check values in backend, default false.
				'display_inline' => true,
				// In UI show results inline view, default false (each value in own line).
				'delay'          => 500,
				// delay for search. default 500.
				'auto_focus'     => true,
				// auto focus input, default true.
			),
			'param_holder_class' => 'vc_not-for-custom',
		),
		array(
			'group'       => esc_html__( 'Data Settings', 'leomes' ),
			'heading'     => esc_html__( 'Order by', 'leomes' ),
			'type'        => 'dropdown',
			'param_name'  => 'orderby',
			'value'       => array(
				esc_html__( 'Date', 'leomes' )                  => 'date',
				esc_html__( 'Post ID', 'leomes' )               => 'ID',
				esc_html__( 'Author', 'leomes' )                => 'author',
				esc_html__( 'Title', 'leomes' )                 => 'title',
				esc_html__( 'Last modified date', 'leomes' )    => 'modified',
				esc_html__( 'Post/page parent ID', 'leomes' )   => 'parent',
				esc_html__( 'Number of comments', 'leomes' )    => 'comment_count',
				esc_html__( 'Menu order/Page Order', 'leomes' ) => 'menu_order',
				esc_html__( 'Meta value', 'leomes' )            => 'meta_value',
				esc_html__( 'Meta value number', 'leomes' )     => 'meta_value_num',
				esc_html__( 'Random order', 'leomes' )          => 'rand',
			),
			'description' => esc_html__( 'Select order type. If "Meta value" or "Meta value Number" is chosen then meta key is required.', 'leomes' ),
			'std'         => 'date',
		),
		array(
			'group'       => esc_html__( 'Data Settings', 'leomes' ),
			'heading'     => esc_html__( 'Sort order', 'leomes' ),
			'type'        => 'dropdown',
			'param_name'  => 'order',
			'value'       => array(
				esc_html__( 'Descending', 'leomes' ) => 'DESC',
				esc_html__( 'Ascending', 'leomes' )  => 'ASC',
			),
			'description' => esc_html__( 'Select sorting order.', 'leomes' ),
			'std'         => 'DESC',
		),
		array(
			'group'       => esc_html__( 'Data Settings', 'leomes' ),
			'heading'     => esc_html__( 'Meta key', 'leomes' ),
			'description' => esc_html__( 'Input meta key for grid ordering.', 'leomes' ),
			'type'        => 'textfield',
			'param_name'  => 'meta_key',
			'dependency'  => array(
				'element' => 'orderby',
				'value'   => array(
					'meta_value',
					'meta_value_num',
				),
			),
		),
		array(
			'group'      => $styling_tab,
			'heading'    => esc_html__( 'Custom Styling Enable', 'leomes' ),
			'type'       => 'checkbox',
			'param_name' => 'custom_styling_enable',
			'value'      => array( esc_html__( 'Yes', 'leomes' ) => '1' ),
		),
		array(
			'group'       => $styling_tab,
			'heading'     => esc_html__( 'Overlay Title Font Size', 'leomes' ),
			'type'        => 'number_responsive',
			'param_name'  => 'overlay_title_font_size',
			'min'         => 8,
			'suffix'      => 'px',
			'media_query' => array(
				'lg' => '',
				'md' => '',
				'sm' => '',
				'xs' => '',
			),
		),
		array(
			'group'       => $styling_tab,
			'heading'     => esc_html__( 'Image Rounded', 'leomes' ),
			'type'        => 'textfield',
			'param_name'  => 'image_rounded',
			'description' => esc_html__( 'Input a valid radius. Fox Ex: 10px. Leave blank to use default.', 'leomes' ),
		),
		array(
			'group'      => esc_html__( 'Filter', 'leomes' ),
			'heading'    => esc_html__( 'Filter Enable', 'leomes' ),
			'type'       => 'checkbox',
			'param_name' => 'filter_enable',
			'value'      => array( esc_html__( 'Enable', 'leomes' ) => '1' ),
			'std'        => '1',
		),
		array(
			'group'      => esc_html__( 'Filter', 'leomes' ),
			'heading'    => esc_html__( 'Filter Counter', 'leomes' ),
			'type'       => 'checkbox',
			'param_name' => 'filter_counter',
			'value'      => array( esc_html__( 'Enable', 'leomes' ) => '1' ),
			'std'        => '1',
		),
		array(
			'group'       => esc_html__( 'Filter', 'leomes' ),
			'heading'     => esc_html__( 'Filter Grid Wrapper', 'leomes' ),
			'description' => esc_html__( 'Wrap filter into grid container.', 'leomes' ),
			'type'        => 'checkbox',
			'param_name'  => 'filter_wrap',
			'value'       => array( esc_html__( 'Enable', 'leomes' ) => '1' ),
		),
		array(
			'group'      => esc_html__( 'Filter', 'leomes' ),
			'heading'    => esc_html__( 'Filter Align', 'leomes' ),
			'type'       => 'dropdown',
			'param_name' => 'filter_align',
			'value'      => array(
				esc_html__( 'Left', 'leomes' )   => 'left',
				esc_html__( 'Center', 'leomes' ) => 'center',
				esc_html__( 'Right', 'leomes' )  => 'right',
			),
			'std'        => 'center',
		),
		array(
			'group'      => esc_html__( 'Pagination', 'leomes' ),
			'heading'    => esc_html__( 'Pagination', 'leomes' ),
			'type'       => 'dropdown',
			'param_name' => 'pagination',
			'value'      => array(
				esc_html__( 'No Pagination', 'leomes' ) => '',
				esc_html__( 'Pagination', 'leomes' )    => 'pagination',
				esc_html__( 'Button', 'leomes' )        => 'loadmore',
				esc_html__( 'Custom Button', 'leomes' ) => 'loadmore_alt',
				esc_html__( 'Infinite', 'leomes' )      => 'infinite',
			),
			'std'        => '',
		),
		array(
			'group'      => esc_html__( 'Pagination', 'leomes' ),
			'heading'    => esc_html__( 'Pagination Align', 'leomes' ),
			'type'       => 'dropdown',
			'param_name' => 'pagination_align',
			'value'      => array(
				esc_html__( 'Left', 'leomes' )   => 'left',
				esc_html__( 'Center', 'leomes' ) => 'center',
				esc_html__( 'Right', 'leomes' )  => 'right',
			),
			'std'        => 'left',
			'dependency' => array(
				'element' => 'pagination',
				'value'   => array( 'pagination', 'infinite', 'loadmore', 'loadmore_alt' ),
			),
		),
		array(
			'group'       => esc_html__( 'Pagination', 'leomes' ),
			'heading'     => esc_html__( 'Button ID', 'leomes' ),
			'description' => esc_html__( 'Input id of custom button to load more posts when click. For EX: #product-load-more-btn', 'leomes' ),
			'type'        => 'el_id',
			'param_name'  => 'pagination_custom_button_id',
			'dependency'  => array(
				'element' => 'pagination',
				'value'   => 'loadmore_alt',
			),
		),
		array(
			'group'      => esc_html__( 'Pagination', 'leomes' ),
			'heading'    => esc_html__( 'Pagination Button Text', 'leomes' ),
			'type'       => 'textfield',
			'param_name' => 'pagination_button_text',
			'std'        => esc_html__( 'Load More', 'leomes' ),
			'dependency' => array(
				'element' => 'pagination',
				'value'   => 'loadmore',
			),
		),
	), Insight_VC::get_vc_spacing_tab() ),
) );

