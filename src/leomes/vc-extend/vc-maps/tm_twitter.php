<?php

class WPBakeryShortCode_TM_Twitter extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		global $insight_shortcode_lg_css;

		$insight_shortcode_lg_css .= Insight_VC::get_vc_spacing_css( $selector, $atts );
	}
}

$_slider_tab = esc_html__( 'Slider Settings', 'leomes' );

vc_map( array(
	'name'                      => esc_html__( 'Twitter', 'leomes' ),
	'base'                      => 'tm_twitter',
	'category'                  => INSIGHT_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-twitter',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'Style', 'leomes' ),
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'List', 'leomes' )         => 'list',
				esc_html__( 'Slider', 'leomes' )       => 'slider',
				esc_html__( 'Slider Quote', 'leomes' ) => 'slider-quote',
			),
			'std'         => 'slider-quote',
		),
		array(
			'heading'    => esc_html__( 'Consumer Key', 'leomes' ),
			'type'       => 'textfield',
			'param_name' => 'consumer_key',
		),
		array(
			'heading'    => esc_html__( 'Consumer Secret', 'leomes' ),
			'type'       => 'textfield',
			'param_name' => 'consumer_secret',
		),
		array(
			'heading'    => esc_html__( 'Access Token', 'leomes' ),
			'type'       => 'textfield',
			'param_name' => 'access_token',
		),
		array(
			'heading'    => esc_html__( 'Access Token Secret', 'leomes' ),
			'type'       => 'textfield',
			'param_name' => 'access_token_secret',
		),
		array(
			'heading'    => esc_html__( 'Twitter Username', 'leomes' ),
			'type'       => 'textfield',
			'param_name' => 'username',
		),
		array(
			'heading'    => esc_html__( 'Number of tweets', 'leomes' ),
			'type'       => 'number',
			'param_name' => 'number_items',
		),
		array(
			'heading'    => esc_html__( 'Heading', 'leomes' ),
			'type'       => 'textfield',
			'param_name' => 'heading',
			'std'        => esc_html__( 'From Twitter', 'leomes' ),
		),
		array(
			'heading'    => esc_html__( 'Show date.', 'leomes' ),
			'type'       => 'checkbox',
			'param_name' => 'show_date',
			'value'      => array(
				esc_html__( 'Yes', 'leomes' ) => '1',
			),
		),
		Insight_VC::extra_class_field(),
		array(
			'group'       => $_slider_tab,
			'heading'     => esc_html__( 'Speed', 'leomes' ),
			'description' => esc_html__( 'Duration of transition between slides (in ms), ex: 1000. Leave blank to use default.', 'leomes' ),
			'type'        => 'number',
			'suffix'      => 'ms',
			'param_name'  => 'carousel_speed',
			'dependency'  => array(
				'element' => 'style',
				'value'   => array(
					'slider',
					'slider-quote',
				),
			),
		),
		array(
			'group'       => $_slider_tab,
			'heading'     => esc_html__( 'Auto Play', 'leomes' ),
			'description' => esc_html__( 'Delay between transitions (in ms), ex: 3000. Leave blank to disabled.', 'leomes' ),
			'type'        => 'number',
			'suffix'      => 'ms',
			'param_name'  => 'carousel_auto_play',
			'dependency'  => array(
				'element' => 'style',
				'value'   => array(
					'slider',
					'slider-quote',
				),
			),
		),
		array(
			'group'      => $_slider_tab,
			'heading'    => esc_html__( 'Navigation', 'leomes' ),
			'type'       => 'dropdown',
			'param_name' => 'carousel_nav',
			'value'      => Insight_VC::get_slider_navs(),
			'std'        => '',
			'dependency' => array(
				'element' => 'style',
				'value'   => array(
					'slider',
					'slider-quote',
				),
			),
		),
		array(
			'group'      => $_slider_tab,
			'heading'    => esc_html__( 'Pagination', 'leomes' ),
			'type'       => 'dropdown',
			'param_name' => 'carousel_pagination',
			'value'      => Insight_VC::get_slider_dots(),
			'std'        => '',
			'dependency' => array(
				'element' => 'style',
				'value'   => array(
					'slider',
					'slider-quote',
				),
			),
		),
	), Insight_VC::get_vc_spacing_tab() ),
) );
