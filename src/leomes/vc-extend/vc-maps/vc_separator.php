<?php

vc_add_params( 'vc_separator', array(
	array(
		'heading'     => esc_html__( 'Position', 'leomes' ),
		'description' => esc_html__( 'Make the separator position absolute with column', 'leomes' ),
		'type'        => 'dropdown',
		'param_name'  => 'position',
		'value'       => array(
			esc_html__( 'None', 'leomes' )   => '',
			esc_html__( 'Top', 'leomes' )    => 'top',
			esc_html__( 'Bottom', 'leomes' ) => 'bottom',
		),
		'std'         => '',
	),
) );
