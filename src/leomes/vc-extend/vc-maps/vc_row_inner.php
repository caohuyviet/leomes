<?php

$styling_tab = esc_html__( 'Styling', 'leomes' );

vc_remove_param( 'vc_row_inner', 'css' );
vc_remove_param( 'vc_row_inner', 'gap' );

vc_add_params( 'vc_row_inner', array_merge( Insight_VC::get_vc_spacing_tab(), array(
	array(
		'heading'     => esc_html__( 'Layer Index', 'leomes' ),
		'description' => esc_html__( 'When content in row or row has negative margin then this controls layer ( z-index ) of row', 'leomes' ),
		'type'        => 'number',
		'param_name'  => 'layer_index',
		'min'         => 0,
		'max'         => 100,
		'step'        => 1,
	),
	array(
		'heading'     => esc_html__( 'Gutter', 'leomes' ),
		'type'        => 'number_responsive',
		'param_name'  => 'gutter',
		'min'         => 0,
		'max'         => 100,
		'step'        => 2,
		'suffix'      => 'px',
		'media_query' => array(
			'lg' => '',
			'md' => '',
			'sm' => '',
			'xs' => '',
		),
	),
	array(
		'heading'     => esc_html__( 'Width', 'leomes' ),
		'description' => esc_html__( 'Input the width for this row.', 'leomes' ),
		'type'        => 'textfield',
		'param_name'  => 'max_width',
	),
	array(
		'heading'    => esc_html__( 'Inner row Alignment Large Device', 'leomes' ),
		'type'       => 'dropdown',
		'param_name' => 'content_alignment',
		'value'      => array(
			esc_html__( 'Left', 'leomes' )   => 'left',
			esc_html__( 'Center', 'leomes' ) => 'center',
			esc_html__( 'Right', 'leomes' )  => 'right',
		),
		'std'        => 'left',
	),
	array(
		'heading'    => esc_html__( 'Inner row Alignment Medium Device', 'leomes' ),
		'type'       => 'dropdown',
		'param_name' => 'md_content_alignment',
		'value'      => array(
			esc_html__( 'Inherit from larger device', 'leomes' ) => '',
			esc_html__( 'Left', 'leomes' )                       => 'left',
			esc_html__( 'Center', 'leomes' )                     => 'center',
			esc_html__( 'Right', 'leomes' )                      => 'right',
		),
		'std'        => '',
	),
	array(
		'heading'    => esc_html__( 'Inner row Alignment Small Device', 'leomes' ),
		'type'       => 'dropdown',
		'param_name' => 'sm_content_alignment',
		'value'      => array(
			esc_html__( 'Inherit from larger device', 'leomes' ) => '',
			esc_html__( 'Left', 'leomes' )                       => 'left',
			esc_html__( 'Center', 'leomes' )                     => 'center',
			esc_html__( 'Right', 'leomes' )                      => 'right',
		),
		'std'        => '',
	),
	array(
		'heading'    => esc_html__( 'Inner row Alignment Extra Small Device', 'leomes' ),
		'type'       => 'dropdown',
		'param_name' => 'xs_content_alignment',
		'value'      => array(
			esc_html__( 'Inherit from larger device', 'leomes' ) => '',
			esc_html__( 'Left', 'leomes' )                       => 'left',
			esc_html__( 'Center', 'leomes' )                     => 'center',
			esc_html__( 'Right', 'leomes' )                      => 'right',
		),
		'std'        => '',
	),
	array(
		'group'       => $styling_tab,
		'heading'     => esc_html__( 'Border Radius', 'leomes' ),
		'description' => esc_html__( 'Ex: 5px or 50%', 'leomes' ),
		'type'        => 'textfield',
		'param_name'  => 'border_radius',
	),
	array(
		'group'       => $styling_tab,
		'heading'     => esc_html__( 'Box Shadow', 'leomes' ),
		'description' => esc_html__( 'Ex: 0 20px 30px #ccc', 'leomes' ),
		'type'        => 'textfield',
		'param_name'  => 'box_shadow',
	),
	array(
		'group'      => $styling_tab,
		'heading'    => esc_html__( 'Row Effect', 'leomes' ),
		'type'       => 'dropdown',
		'param_name' => 'effect',
		'value'      => array(
			esc_html__( 'None', 'leomes' )    => '',
			esc_html__( 'Firefly', 'leomes' ) => 'firefly',
		),
		'std'        => '',
	),
	array(
		'group'            => $styling_tab,
		'heading'          => esc_html__( 'Firefly Color', 'leomes' ),
		'type'             => 'colorpicker',
		'param_name'       => 'firefly_color',
		'dependency'       => array(
			'element' => 'effect',
			'value'   => array( 'firefly' ),
		),
		'std'              => '#fff',
		'edit_field_class' => 'vc_col-sm-3',
	),
	array(
		'group'            => $styling_tab,
		'heading'          => esc_html__( 'Firely Total', 'leomes' ),
		'type'             => 'number',
		'param_name'       => 'firefly_total',
		'value'            => 100,
		'min'              => 0,
		'max'              => 100,
		'step'             => 1,
		'suffix'           => '%',
		'std'              => 30,
		'dependency'       => array(
			'element' => 'effect',
			'value'   => array( 'firefly' ),
		),
		'edit_field_class' => 'vc_col-sm-3',
	),
	array(
		'group'            => $styling_tab,
		'heading'          => esc_html__( 'Firely Min Width', 'leomes' ),
		'type'             => 'number',
		'param_name'       => 'firefly_min_size',
		'value'            => 100,
		'min'              => 0,
		'max'              => 100,
		'step'             => 1,
		'suffix'           => '%',
		'std'              => 1,
		'dependency'       => array(
			'element' => 'effect',
			'value'   => array( 'firefly' ),
		),
		'edit_field_class' => 'vc_col-sm-3',
	),
	array(
		'group'            => $styling_tab,
		'heading'          => esc_html__( 'Firely Max Width', 'leomes' ),
		'type'             => 'number',
		'param_name'       => 'firefly_max_size',
		'value'            => 100,
		'min'              => 0,
		'max'              => 100,
		'step'             => 1,
		'suffix'           => '%',
		'std'              => 3,
		'dependency'       => array(
			'element' => 'effect',
			'value'   => array( 'firefly' ),
		),
		'edit_field_class' => 'vc_col-sm-3',
	),
	array(
		'group'      => $styling_tab,
		'heading'    => esc_html__( 'Background Color', 'leomes' ),
		'type'       => 'dropdown',
		'param_name' => 'background_color',
		'value'      => array(
			esc_html__( 'None', 'leomes' )            => '',
			esc_html__( 'Primary Color', 'leomes' )   => 'primary',
			esc_html__( 'Secondary Color', 'leomes' ) => 'secondary',
			esc_html__( 'Custom Color', 'leomes' )    => 'custom',
			esc_html__( 'Gradient Color', 'leomes' )  => 'gradient',
		),
		'std'        => '',
	),
	array(
		'group'      => $styling_tab,
		'heading'    => esc_html__( 'Custom Background Color', 'leomes' ),
		'type'       => 'colorpicker',
		'param_name' => 'custom_background_color',
		'dependency' => array(
			'element' => 'background_color',
			'value'   => array( 'custom' ),
		),
	),
	array(
		'group'      => $styling_tab,
		'heading'    => esc_html__( 'Background Gradient', 'leomes' ),
		'type'       => 'gradient',
		'param_name' => 'background_gradient',
		'dependency' => array(
			'element' => 'background_color',
			'value'   => array( 'gradient' ),
		),
	),
	array(
		'group'      => $styling_tab,
		'heading'    => esc_html__( 'Background Image', 'leomes' ),
		'type'       => 'attach_image',
		'param_name' => 'background_image',
	),
	array(
		'group'      => $styling_tab,
		'heading'    => esc_html__( 'Hide Background Image', 'leomes' ),
		'type'       => 'dropdown',
		'param_name' => 'hide_background_image',
		'value'      => array(
			esc_html__( 'Always show', 'leomes' )             => '',
			esc_html__( 'Medium Device Down', 'leomes' )      => 'md',
			esc_html__( 'Small Device Down', 'leomes' )       => 'sm',
			esc_html__( 'Extra Small Device Down', 'leomes' ) => 'xs',
		),
		'std'        => '',
		'dependency' => array(
			'element'   => 'background_image',
			'not_empty' => true,
		),
	),
	array(
		'group'      => $styling_tab,
		'heading'    => esc_html__( 'Background Repeat', 'leomes' ),
		'type'       => 'dropdown',
		'param_name' => 'background_repeat',
		'value'      => array(
			esc_html__( 'No repeat', 'leomes' )         => 'no-repeat',
			esc_html__( 'Tile', 'leomes' )              => 'repeat',
			esc_html__( 'Tile Horizontally', 'leomes' ) => 'repeat-x',
			esc_html__( 'Tile Vertically', 'leomes' )   => 'repeat-y',
		),
		'std'        => 'no-repeat',
		'dependency' => array(
			'element'   => 'background_image',
			'not_empty' => true,
		),
	),
	array(
		'group'      => $styling_tab,
		'heading'    => esc_html__( 'Background Size', 'leomes' ),
		'type'       => 'dropdown',
		'param_name' => 'background_size',
		'value'      => array(
			esc_html__( 'Auto', 'leomes' )    => 'auto',
			esc_html__( 'Cover', 'leomes' )   => 'cover',
			esc_html__( 'Contain', 'leomes' ) => 'contain',
		),
		'std'        => 'cover',
		'dependency' => array(
			'element'   => 'background_image',
			'not_empty' => true,
		),
	),
	array(
		'group'       => $styling_tab,
		'heading'     => esc_html__( 'Background Position', 'leomes' ),
		'description' => esc_html__( 'Ex: left center', 'leomes' ),
		'type'        => 'textfield',
		'param_name'  => 'background_position',
		'dependency'  => array(
			'element'   => 'background_image',
			'not_empty' => true,
		),
	),
	array(
		'group'      => $styling_tab,
		'heading'    => esc_html__( 'Scroll Effect', 'leomes' ),
		'type'       => 'dropdown',
		'param_name' => 'background_attachment',
		'value'      => array(
			esc_html__( 'Move with the content', 'leomes' ) => 'scroll',
			esc_html__( 'Fixed at its position', 'leomes' ) => 'fixed',
			esc_html__( 'Marque', 'leomes' )                => 'marque',
		),
		'std'        => 'scroll',
		'dependency' => array(
			'element'   => 'background_image',
			'not_empty' => true,
		),
	),
	array(
		'group'      => $styling_tab,
		'heading'    => esc_html__( 'Marque Direction', 'leomes' ),
		'type'       => 'dropdown',
		'param_name' => 'marque_direction',
		'value'      => array(
			esc_html__( 'To Left', 'leomes' )  => 'to-left',
			esc_html__( 'To Right', 'leomes' ) => 'to-right',
		),
		'std'        => 'to-right',
		'dependency' => array(
			'element' => 'background_attachment',
			'value'   => 'marque',
		),
	),
	array(
		'group'      => $styling_tab,
		'heading'    => esc_html__( 'Marque Pause On Hover.', 'leomes' ),
		'type'       => 'checkbox',
		'param_name' => 'marque_pause_on_hover',
		'value'      => array(
			esc_html__( 'Yes', 'leomes' ) => '1',
		),
		'dependency' => array(
			'element' => 'background_attachment',
			'value'   => 'marque',
		),
	),
	array(
		'group'       => $styling_tab,
		'heading'     => esc_html__( 'Background Overlay', 'leomes' ),
		'description' => esc_html__( 'Choose an overlay background color.', 'leomes' ),
		'type'        => 'dropdown',
		'param_name'  => 'overlay_background',
		'value'       => array(
			esc_html__( 'None', 'leomes' )            => '',
			esc_html__( 'Primary Color', 'leomes' )   => 'primary',
			esc_html__( 'Secondary Color', 'leomes' ) => 'secondary',
			esc_html__( 'Custom Color', 'leomes' )    => 'overlay_custom_background',
		),
	),
	array(
		'group'       => $styling_tab,
		'heading'     => esc_html__( 'Custom Background Overlay', 'leomes' ),
		'description' => esc_html__( 'Choose an custom background color overlay.', 'leomes' ),
		'type'        => 'colorpicker',
		'param_name'  => 'overlay_custom_background',
		'std'         => '#000000',
		'dependency'  => array( 'element' => 'overlay_background', 'value' => array( 'overlay_custom_background' ) ),
	),
	array(
		'group'      => $styling_tab,
		'heading'    => esc_html__( 'Opacity', 'leomes' ),
		'type'       => 'number',
		'param_name' => 'overlay_opacity',
		'value'      => 100,
		'min'        => 0,
		'max'        => 100,
		'step'       => 1,
		'suffix'     => '%',
		'std'        => 80,
		'dependency' => array(
			'element'   => 'overlay_background',
			'not_empty' => true,
		),
	),
) ) );
