<?php

class WPBakeryShortCode_TM_Counter extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		global $insight_shortcode_lg_css;
		$align = 'center';
		$skin  = $number_color = $custom_number_color = $text_color = $custom_text_color = $icon_color = $custom_icon_color = '';
		$tmp   = '';
		extract( $atts );

		$tmp .= "text-align: {$align}";

		if ( $skin === 'custom' ) {
			$number_tmp = $text_tmp = $icon_tmp = '';

			if ( $number_color === 'custom' ) {
				$number_tmp .= "color: $custom_number_color;";
			}

			if ( $number_tmp !== '' ) {
				$insight_shortcode_lg_css .= "$selector .number-wrap{ $number_tmp }";
			}

			if ( $text_color === 'custom' ) {
				$text_tmp .= "color: $custom_text_color;";
			}

			if ( $text_tmp !== '' ) {
				$insight_shortcode_lg_css .= "$selector .text{ $text_tmp }";
			}

			if ( $icon_color === 'custom' ) {
				$icon_tmp .= "color: $custom_icon_color;";
			}

			if ( $icon_tmp !== '' ) {
				$insight_shortcode_lg_css .= "$selector .icon{ $icon_tmp }";
			}
		}

		$insight_shortcode_lg_css .= "$selector { $tmp }";
		$insight_shortcode_lg_css .= Insight_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'                      => esc_html__( 'Counter', 'leomes' ),
	'base'                      => 'tm_counter',
	'category'                  => INSIGHT_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-counter',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'Style', 'leomes' ),
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( '01', 'leomes' ) => '1',
				esc_html__( '02', 'leomes' ) => '2',
			),
			'std'         => '1',
		),
		array(
			'type'       => 'dropdown',
			'heading'    => esc_html__( 'Counter Animation', 'leomes' ),
			'param_name' => 'animation',
			'value'      => array(
				esc_html__( 'Counter Up', 'leomes' ) => 'counter-up',
				esc_html__( 'Odometer', 'leomes' )   => 'odometer',
			),
			'std'        => 'counter-up',
		),
		array(
			'heading'    => esc_html__( 'Text Align', 'leomes' ),
			'type'       => 'dropdown',
			'param_name' => 'align',
			'value'      => array(
				esc_html__( 'Left', 'leomes' )   => 'left',
				esc_html__( 'Center', 'leomes' ) => 'center',
				esc_html__( 'Right', 'leomes' )  => 'right',
			),
			'std'        => 'center',
		),
		array(
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'Skin', 'leomes' ),
			'param_name'  => 'skin',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Dark', 'leomes' )   => 'dark',
				esc_html__( 'Light', 'leomes' )  => 'light',
				esc_html__( 'Custom', 'leomes' ) => 'custom',
			),
			'std'         => 'dark',
		),
		array(
			'type'             => 'dropdown',
			'heading'          => esc_html__( 'Number Color', 'leomes' ),
			'param_name'       => 'number_color',
			'value'            => array(
				esc_html__( 'Default Color', 'leomes' )   => '',
				esc_html__( 'Primary Color', 'leomes' )   => 'primary_color',
				esc_html__( 'Secondary Color', 'leomes' ) => 'secondary_color',
				esc_html__( 'Custom Color', 'leomes' )    => 'custom',
			),
			'std'              => 'primary_color',
			'dependency'       => array(
				'element' => 'skin',
				'value'   => array( 'custom' ),
			),
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'type'             => 'colorpicker',
			'heading'          => esc_html__( 'Custom Number Color', 'leomes' ),
			'param_name'       => 'custom_number_color',
			'dependency'       => array(
				'element' => 'number_color',
				'value'   => array( 'custom' ),
			),
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'type'             => 'dropdown',
			'heading'          => esc_html__( 'Text Color', 'leomes' ),
			'param_name'       => 'text_color',
			'value'            => array(
				esc_html__( 'Default Color', 'leomes' )   => '',
				esc_html__( 'Primary Color', 'leomes' )   => 'primary_color',
				esc_html__( 'Secondary Color', 'leomes' ) => 'secondary_color',
				esc_html__( 'Custom Color', 'leomes' )    => 'custom',
			),
			'std'              => 'custom',
			'dependency'       => array(
				'element' => 'skin',
				'value'   => array( 'custom' ),
			),
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'type'             => 'colorpicker',
			'heading'          => esc_html__( 'Custom Text Color', 'leomes' ),
			'param_name'       => 'custom_text_color',
			'dependency'       => array(
				'element' => 'text_color',
				'value'   => array( 'custom' ),
			),
			'edit_field_class' => 'vc_col-sm-6',
			'std'              => '#a9a9a9',
		),
		array(
			'type'             => 'dropdown',
			'heading'          => esc_html__( 'Icon Color', 'leomes' ),
			'param_name'       => 'icon_color',
			'value'            => array(
				esc_html__( 'Default Color', 'leomes' )   => '',
				esc_html__( 'Primary Color', 'leomes' )   => 'primary_color',
				esc_html__( 'Secondary Color', 'leomes' ) => 'secondary_color',
				esc_html__( 'Custom Color', 'leomes' )    => 'custom',
			),
			'std'              => 'custom',
			'dependency'       => array(
				'element' => 'skin',
				'value'   => array( 'custom' ),
			),
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'type'             => 'colorpicker',
			'heading'          => esc_html__( 'Custom Icon Color', 'leomes' ),
			'param_name'       => 'custom_icon_color',
			'dependency'       => array(
				'element' => 'icon_color',
				'value'   => array( 'custom' ),
			),
			'edit_field_class' => 'vc_col-sm-6',
			'std'              => '#a9a9a9',
		),
		array(
			'group'       => esc_html__( 'Data', 'leomes' ),
			'heading'     => esc_html__( 'Number', 'leomes' ),
			'type'        => 'number',
			'admin_label' => true,
			'param_name'  => 'number',
		),
		array(
			'group'       => esc_html__( 'Data', 'leomes' ),
			'heading'     => esc_html__( 'Number Prefix', 'leomes' ),
			'description' => esc_html__( 'Prefix your number with a symbol or text.', 'leomes' ),
			'type'        => 'textfield',
			'admin_label' => true,
			'param_name'  => 'number_prefix',
		),
		array(
			'group'       => esc_html__( 'Data', 'leomes' ),
			'heading'     => esc_html__( 'Number Suffix', 'leomes' ),
			'description' => esc_html__( 'Suffix your number with a symbol or text.', 'leomes' ),
			'type'        => 'textfield',
			'admin_label' => true,
			'param_name'  => 'number_suffix',
		),
		array(
			'group'       => esc_html__( 'Data', 'leomes' ),
			'type'        => 'textfield',
			'heading'     => esc_html__( 'Text', 'leomes' ),
			'admin_label' => true,
			'param_name'  => 'text',
		),
		Insight_VC::extra_class_field(),
	), Insight_VC::icon_libraries( array( 'allow_none' => true ) ), Insight_VC::get_vc_spacing_tab() ),
) );
