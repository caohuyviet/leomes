<?php

class WPBakeryShortCode_TM_Blog extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		global $insight_shortcode_lg_css;
		extract( $atts );
		$tmp      = '';
		$item_tmp = '';

		$style           = isset( $atts['style'] ) ? $atts['style'] : '';
		$gutter          = isset( $atts['gutter'] ) ? $atts['gutter'] : 0;
		$carousel_gutter = isset( $atts['carousel_gutter'] ) ? $atts['carousel_gutter'] : 0;

		if ( in_array( $style, array( 'grid' ), true ) ) {
			if ( $gutter > 0 ) {
				$item_tmp .= "margin-bottom: {$gutter}px";
			} else {
				$item_tmp .= "margin-bottom: -1px; margin-left: -1px;";
			}
		} elseif ( in_array( $style, array( 'grid_classic' ), true ) ) {
			if ( $gutter > 0 ) {
				$item_tmp .= "margin-bottom: {$gutter}px";
			}
		} elseif ( $style === 'carousel' ) {
			if ( $carousel_gutter == 0 ) {
				$insight_shortcode_lg_css .= "$selector .post-item:not(.swiper-slide-active){ margin-left: -1px; }";
			}
		}

		if ( $tmp !== '' ) {
			$insight_shortcode_lg_css .= "$selector{ {$tmp} }";
		}

		if ( $item_tmp !== '' ) {
			$insight_shortcode_lg_css .= "$selector .post-item { {$item_tmp} }";
		}

		$insight_shortcode_lg_css .= Insight_VC::get_vc_spacing_css( $selector, $atts );
	}
}

$carousel_group = esc_html__( 'Carousel Settings', 'leomes' );

vc_map( array(
	'name'     => esc_html__( 'Blog', 'leomes' ),
	'base'     => 'tm_blog',
	'category' => INSIGHT_VC_SHORTCODE_CATEGORY,
	'icon'     => 'insight-i insight-i-blog',
	'params'   => array_merge( array(
		array(
			'heading'     => esc_html__( 'Blog Style', 'leomes' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'List Large Image', 'leomes' )   => 'list',
				esc_html__( 'List Small Image', 'leomes' )   => 'small_image_list',
				esc_html__( 'Grid Classic', 'leomes' )       => 'grid_classic',
				esc_html__( 'Grid Masonry', 'leomes' )       => 'grid_masonry',
				esc_html__( 'Grid Metro', 'leomes' )         => 'metro',
				esc_html__( 'Grid Overlay Image', 'leomes' ) => 'grid',
				esc_html__( 'Carousel Slider', 'leomes' )    => 'carousel',
			),
			'std'         => 'list',
		),
		array(
			'heading'    => esc_html__( 'Metro Layout', 'leomes' ),
			'type'       => 'param_group',
			'param_name' => 'metro_layout',
			'params'     => array(
				array(
					'heading'     => esc_html__( 'Item Size', 'leomes' ),
					'type'        => 'dropdown',
					'param_name'  => 'size',
					'admin_label' => true,
					'value'       => array(
						esc_html__( 'Width 1 - Height 1', 'leomes' ) => '1:1',
						esc_html__( 'Width 1 - Height 2', 'leomes' ) => '1:2',
						esc_html__( 'Width 2 - Height 1', 'leomes' ) => '2:1',
						esc_html__( 'Width 2 - Height 2', 'leomes' ) => '2:2',
					),
					'std'         => '1:1',
				),
			),
			'value'      => rawurlencode( wp_json_encode( array(
				array(
					'size' => '2:2',
				),
				array(
					'size' => '1:1',
				),
				array(
					'size' => '1:1',
				),
				array(
					'size' => '2:2',
				),
				array(
					'size' => '1:1',
				),
				array(
					'size' => '1:1',
				),
			) ) ),
			'dependency' => array(
				'element' => 'style',
				'value'   => array( 'metro' ),
			),
		),
		array(
			'heading'     => esc_html__( 'Blog Skin', 'leomes' ),
			'type'        => 'dropdown',
			'param_name'  => 'skin',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Default', 'leomes' ) => '',
				esc_html__( 'Light', 'leomes' )   => 'light',
			),
			'std'         => '',
		),
		array(
			'heading'     => esc_html__( 'Columns', 'leomes' ),
			'type'        => 'number_responsive',
			'param_name'  => 'columns',
			'min'         => 1,
			'max'         => 6,
			'step'        => 1,
			'suffix'      => '',
			'media_query' => array(
				'lg' => '3',
				'md' => '',
				'sm' => '2',
				'xs' => '1',
			),
			'dependency'  => array(
				'element' => 'style',
				'value'   => array(
					'grid',
					'grid_masonry',
					'grid_classic',
					'metro',
				),
			),
		),
		array(
			'heading'     => esc_html__( 'Grid Gutter', 'leomes' ),
			'description' => esc_html__( 'Controls the gutter of grid. Default 30px', 'leomes' ),
			'type'        => 'number',
			'param_name'  => 'gutter',
			'std'         => 30,
			'min'         => 0,
			'max'         => 100,
			'step'        => 1,
			'suffix'      => 'px',
		),
		Insight_VC::get_animation_field(),
		Insight_VC::extra_class_field(),
		array(
			'group'       => $carousel_group,
			'heading'     => esc_html__( 'Auto Play', 'leomes' ),
			'description' => esc_html__( 'Delay between transitions (in ms), ex: 3000. Leave blank to disabled.', 'leomes' ),
			'type'        => 'number',
			'suffix'      => 'ms',
			'param_name'  => 'carousel_auto_play',
			'dependency'  => array( 'element' => 'style', 'value' => 'carousel' ),
		),
		array(
			'group'      => $carousel_group,
			'heading'    => esc_html__( 'Navigation', 'leomes' ),
			'type'       => 'dropdown',
			'param_name' => 'carousel_nav',
			'value'      => Insight_VC::get_slider_navs(),
			'std'        => '',
			'dependency' => array( 'element' => 'style', 'value' => 'carousel' ),
		),
		array(
			'group'      => $carousel_group,
			'heading'    => esc_html__( 'Pagination', 'leomes' ),
			'type'       => 'dropdown',
			'param_name' => 'carousel_pagination',
			'value'      => Insight_VC::get_slider_dots(),
			'std'        => '',
			'dependency' => array( 'element' => 'style', 'value' => 'carousel' ),
		),
		array(
			'group'      => $carousel_group,
			'heading'    => esc_html__( 'Gutter', 'leomes' ),
			'type'       => 'number',
			'param_name' => 'carousel_gutter',
			'std'        => 30,
			'min'        => 0,
			'max'        => 50,
			'step'       => 1,
			'suffix'     => 'px',
			'dependency' => array( 'element' => 'style', 'value' => 'carousel' ),
		),
		array(
			'group'       => $carousel_group,
			'heading'     => esc_html__( 'Items Display', 'leomes' ),
			'type'        => 'number_responsive',
			'param_name'  => 'carousel_items_display',
			'min'         => 1,
			'max'         => 10,
			'suffix'      => 'item (s)',
			'media_query' => array(
				'lg' => 3,
				'md' => 3,
				'sm' => 2,
				'xs' => 1,
			),
			'dependency'  => array( 'element' => 'style', 'value' => 'carousel' ),
		),
		array(
			'group'       => esc_html__( 'Data Settings', 'leomes' ),
			'heading'     => esc_html__( 'Items per page', 'leomes' ),
			'description' => esc_html__( 'Number of items to show per page.', 'leomes' ),
			'type'        => 'number',
			'param_name'  => 'number',
			'std'         => 9,
			'min'         => 1,
			'max'         => 100,
			'step'        => 1,
		),
		array(
			'group'              => esc_html__( 'Data Settings', 'leomes' ),
			'heading'            => esc_html__( 'Narrow data source', 'leomes' ),
			'description'        => esc_html__( 'Enter categories, tags or custom taxonomies.', 'leomes' ),
			'type'               => 'autocomplete',
			'param_name'         => 'taxonomies',
			'settings'           => array(
				'multiple'       => true,
				'min_length'     => 1,
				'groups'         => true,
				// In UI show results grouped by groups, default false.
				'unique_values'  => true,
				// In UI show results except selected. NB! You should manually check values in backend, default false.
				'display_inline' => true,
				// In UI show results inline view, default false (each value in own line).
				'delay'          => 500,
				// delay for search. default 500.
				'auto_focus'     => true,
				// auto focus input, default true.
			),
			'param_holder_class' => 'vc_not-for-custom',
		),
		array(
			'group'       => esc_html__( 'Data Settings', 'leomes' ),
			'heading'     => esc_html__( 'Order by', 'leomes' ),
			'type'        => 'dropdown',
			'param_name'  => 'orderby',
			'value'       => array(
				esc_html__( 'Date', 'leomes' )                  => 'date',
				esc_html__( 'Post ID', 'leomes' )               => 'ID',
				esc_html__( 'Author', 'leomes' )                => 'author',
				esc_html__( 'Title', 'leomes' )                 => 'title',
				esc_html__( 'Last modified date', 'leomes' )    => 'modified',
				esc_html__( 'Post/page parent ID', 'leomes' )   => 'parent',
				esc_html__( 'Number of comments', 'leomes' )    => 'comment_count',
				esc_html__( 'Menu order/Page Order', 'leomes' ) => 'menu_order',
				esc_html__( 'Meta value', 'leomes' )            => 'meta_value',
				esc_html__( 'Meta value number', 'leomes' )     => 'meta_value_num',
				esc_html__( 'Random order', 'leomes' )          => 'rand',
			),
			'description' => esc_html__( 'Select order type. If "Meta value" or "Meta value Number" is chosen then meta key is required.', 'leomes' ),
			'std'         => 'date',
		),
		array(
			'group'       => esc_html__( 'Data Settings', 'leomes' ),
			'heading'     => esc_html__( 'Sort order', 'leomes' ),
			'type'        => 'dropdown',
			'param_name'  => 'order',
			'value'       => array(
				esc_html__( 'Descending', 'leomes' ) => 'DESC',
				esc_html__( 'Ascending', 'leomes' )  => 'ASC',
			),
			'description' => esc_html__( 'Select sorting order.', 'leomes' ),
			'std'         => 'DESC',
		),
		array(
			'group'       => esc_html__( 'Data Settings', 'leomes' ),
			'heading'     => esc_html__( 'Meta key', 'leomes' ),
			'description' => esc_html__( 'Input meta key for grid ordering.', 'leomes' ),
			'type'        => 'textfield',
			'param_name'  => 'meta_key',
			'dependency'  => array(
				'element' => 'orderby',
				'value'   => array(
					'meta_value',
					'meta_value_num',
				),
			),
		),
		array(
			'group'      => esc_html__( 'Filter', 'leomes' ),
			'heading'    => esc_html__( 'Filter Enable', 'leomes' ),
			'type'       => 'checkbox',
			'param_name' => 'filter_enable',
			'value'      => array( esc_html__( 'Enable', 'leomes' ) => '1' ),
			'dependency' => array(
				'element' => 'style',
				'value'   => array(
					'grid',
					'grid_masonry',
					'grid_classic',
					'metro',
				),
			),
		),
		array(
			'group'      => esc_html__( 'Filter', 'leomes' ),
			'heading'    => esc_html__( 'Filter Counter', 'leomes' ),
			'type'       => 'checkbox',
			'param_name' => 'filter_counter',
			'value'      => array( esc_html__( 'Enable', 'leomes' ) => '1' ),
			'std'        => '1',
			'dependency' => array(
				'element' => 'style',
				'value'   => array(
					'grid',
					'grid_masonry',
					'grid_classic',
					'metro',
				),
			),
		),
		array(
			'group'       => esc_html__( 'Filter', 'leomes' ),
			'heading'     => esc_html__( 'Filter Grid Wrapper', 'leomes' ),
			'description' => esc_html__( 'Wrap filter into grid container.', 'leomes' ),
			'type'        => 'checkbox',
			'param_name'  => 'filter_wrap',
			'value'       => array( esc_html__( 'Enable', 'leomes' ) => '1' ),
			'dependency'  => array(
				'element' => 'style',
				'value'   => array(
					'grid',
					'grid_masonry',
					'grid_classic',
					'metro',
				),
			),
		),
		array(
			'group'      => esc_html__( 'Filter', 'leomes' ),
			'heading'    => esc_html__( 'Filter Align', 'leomes' ),
			'type'       => 'dropdown',
			'param_name' => 'filter_align',
			'value'      => array(
				esc_html__( 'Left', 'leomes' )   => 'left',
				esc_html__( 'Center', 'leomes' ) => 'center',
				esc_html__( 'Right', 'leomes' )  => 'right',
			),
			'std'        => 'center',
			'dependency' => array(
				'element' => 'style',
				'value'   => array(
					'grid',
					'grid_masonry',
					'grid_classic',
					'metro',
				),
			),
		),
		array(
			'group'      => esc_html__( 'Pagination', 'leomes' ),
			'heading'    => esc_html__( 'Pagination', 'leomes' ),
			'type'       => 'dropdown',
			'param_name' => 'pagination',
			'value'      => array(
				esc_html__( 'No Pagination', 'leomes' ) => '',
				esc_html__( 'Pagination', 'leomes' )    => 'pagination',
				esc_html__( 'Button', 'leomes' )        => 'loadmore',
				esc_html__( 'Custom Button', 'leomes' ) => 'loadmore_alt',
				esc_html__( 'Infinite', 'leomes' )      => 'infinite',
			),
			'std'        => '',
		),
		array(
			'group'      => esc_html__( 'Pagination', 'leomes' ),
			'heading'    => esc_html__( 'Pagination Align', 'leomes' ),
			'type'       => 'dropdown',
			'param_name' => 'pagination_align',
			'value'      => array(
				esc_html__( 'Left', 'leomes' )   => 'left',
				esc_html__( 'Center', 'leomes' ) => 'center',
				esc_html__( 'Right', 'leomes' )  => 'right',
			),
			'std'        => 'left',
			'dependency' => array(
				'element' => 'pagination',
				'value'   => array( 'pagination', 'infinite', 'loadmore', 'loadmore_alt' ),
			),
		),
		array(
			'group'       => esc_html__( 'Pagination', 'leomes' ),
			'heading'     => esc_html__( 'Button ID', 'leomes' ),
			'description' => esc_html__( 'Input id of custom button to load more posts when click. For EX: #product-load-more-btn', 'leomes' ),
			'type'        => 'el_id',
			'param_name'  => 'pagination_custom_button_id',
			'dependency'  => array(
				'element' => 'pagination',
				'value'   => 'loadmore_alt',
			),
		),
		array(
			'group'      => esc_html__( 'Pagination', 'leomes' ),
			'heading'    => esc_html__( 'Pagination Button Text', 'leomes' ),
			'type'       => 'textfield',
			'param_name' => 'pagination_button_text',
			'std'        => esc_html__( 'Load More', 'leomes' ),
			'dependency' => array(
				'element' => 'pagination',
				'value'   => 'loadmore',
			),
		),

	), Insight_VC::get_vc_spacing_tab() ),
) );
