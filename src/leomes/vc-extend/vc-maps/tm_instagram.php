<?php

class WPBakeryShortCode_TM_Instagram extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		global $insight_shortcode_lg_css;

		$insight_shortcode_lg_css .= Insight_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'                      => esc_html__( 'Instagram', 'leomes' ),
	'base'                      => 'tm_instagram',
	'category'                  => INSIGHT_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-instagram',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'Style', 'leomes' ),
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Grid', 'leomes' ) => 'grid',
			),
			'std'         => 'grid',
		),
		array(
			'heading'    => esc_html__( 'User Name', 'leomes' ),
			'type'       => 'textfield',
			'param_name' => 'username',
		),
		array(
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'Image Size', 'leomes' ),
			'param_name'  => 'size',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Thumbnail 150x150', 'leomes' )   => 'thumbnail',
				esc_html__( 'Small 240x240', 'leomes' )       => 'small',
				esc_html__( 'Small 320x320', 'leomes' )       => 'medium',
				esc_html__( 'Large 480x480', 'leomes' )       => 'large',
				esc_html__( 'Extra Large 640x640', 'leomes' ) => 'extra_large',
				esc_html__( 'Original', 'leomes' )            => 'original',
			),
			'std'         => 'large',
		),
		array(
			'heading'    => esc_html__( 'Number of items', 'leomes' ),
			'type'       => 'number',
			'param_name' => 'number_items',
			'std'        => '6',
		),
		array(
			'heading'     => esc_html__( 'Columns', 'leomes' ),
			'type'        => 'number_responsive',
			'param_name'  => 'columns',
			'min'         => 1,
			'max'         => 10,
			'step'        => 1,
			'suffix'      => 'column (s)',
			'media_query' => array(
				'lg' => 3,
				'md' => '',
				'sm' => '',
				'xs' => '',
			),
		),
		array(
			'heading'    => esc_html__( 'Gutter', 'leomes' ),
			'type'       => 'number',
			'param_name' => 'gutter',
		),
		array(
			'heading'    => esc_html__( 'Show User Name', 'leomes' ),
			'type'       => 'checkbox',
			'param_name' => 'show_user_name',
			'value'      => array( esc_html__( 'Yes', 'leomes' ) => '1' ),
		),
		array(
			'heading'    => esc_html__( 'Show overlay likes and comments', 'leomes' ),
			'type'       => 'checkbox',
			'param_name' => 'overlay',
			'value'      => array( esc_html__( 'Yes', 'leomes' ) => '1' ),
		),
		array(
			'heading'    => esc_html__( 'Open links in a new tab.', 'leomes' ),
			'type'       => 'checkbox',
			'param_name' => 'link_target',
			'value'      => array(
				esc_html__( 'Yes', 'leomes' ) => '1',
			),
			'std'        => '1',
		),
		Insight_VC::extra_class_field(),
	), Insight_VC::get_vc_spacing_tab() ),
) );
