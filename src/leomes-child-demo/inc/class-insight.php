<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @package   InsightFramework
 */
class Insight_Child {

	public function __construct() {
		add_action( 'wp_footer', array( $this, 'demo_options_template' ) );
	}

	function demo_options_template() {
		$theme_description  = 'Leomes\'s a modern & flexible multipurpose theme brings the young and vibrant look to your website with high flexibility in customization.';
		$support_link       = 'http://support.thememove.com/helpdesk/tickets/new';
		$documentation_link = 'http://document.thememove.com/leomes/';
		$purchase_link      = 'https://themeforest.net/item/arden-a-sharp-modern-multipurpose-wordpress-theme/19710416?Ref=ThemeMove';
		$image_dir          = get_stylesheet_directory_uri() . '/assets/images/';
		$links              = array(
			array(
				'url'       => home_url( '/homepage/agency/' ),
				'thumbnail' => $image_dir . 'home-agency-preview.jpg',
				'title'     => 'Agency',
			),
			array(
				'url'       => home_url( '/homepage/startup/' ),
				'thumbnail' => $image_dir . 'home-startup-preview.jpg',
				'title'     => 'Startup 01',
			),
			array(
				'url'       => home_url( '/homepage/startup-02/' ),
				'thumbnail' => $image_dir . 'home-startup-02-preview.jpg',
				'title'     => 'Startup 02',
			),
			array(
				'url'       => home_url( '/homepage/startup-03/' ),
				'thumbnail' => $image_dir . 'home-startup-03-preview.jpg',
				'title'     => 'Startup 03',
			),
			array(
				'url'       => home_url( '/homepage/onepage/' ),
				'thumbnail' => $image_dir . 'home-onepage-preview.jpg',
				'title'     => 'Onepage',
			),
			array(
				'url'       => home_url( '/homepage/business/' ),
				'thumbnail' => $image_dir . 'home-business-preview.jpg',
				'title'     => 'Business',
			),
			array(
				'url'       => home_url( '/homepage/freelancer/' ),
				'thumbnail' => $image_dir . 'home-freelancer-preview.jpg',
				'title'     => 'Freelancer',
			),
			array(
				'url'       => home_url( '/homepage/conference/' ),
				'thumbnail' => $image_dir . 'home-conference-preview.jpg',
				'title'     => 'Conference',
			),
			array(
				'url'       => home_url( '/homepage/app-presentation-01/' ),
				'thumbnail' => $image_dir . 'home-app-presentation-01-preview.jpg',
				'title'     => 'App Presentation 01',
			),
			array(
				'url'       => home_url( '/homepage/app-presentation-02/' ),
				'thumbnail' => $image_dir . 'home-app-presentation-02-preview.jpg',
				'title'     => 'App Presentation 02',
			),
			array(
				'url'       => home_url( '/homepage/corporate/' ),
				'thumbnail' => $image_dir . 'home-corporate-preview.jpg',
				'title'     => 'Corporate',
			),
			array(
				'url'       => home_url( '/homepage/creative-agency/' ),
				'thumbnail' => $image_dir . 'home-creative-agency-preview.jpg',
				'title'     => 'Creative Agency',
			),
			array(
				'url'       => home_url( '/homepage/medical-clinic/' ),
				'thumbnail' => $image_dir . 'home-medical-clinic-preview.jpg',
				'title'     => 'Medical Clinic',
			),
			array(
				'url'       => home_url( '/homepage/finance/' ),
				'thumbnail' => $image_dir . 'home-finance-preview.jpg',
				'title'     => 'Finance',
			),
			array(
				'url'       => home_url( '/homepage/seo/' ),
				'thumbnail' => $image_dir . 'home-seo-preview.jpg',
				'title'     => 'SEO',
			),
			array(
				'url'       => home_url( '/homepage/support-center/' ),
				'thumbnail' => $image_dir . 'home-support-center-preview.jpg',
				'title'     => 'Support Center',
			),
			array(
				'url'       => home_url( '/homepage/blog-homepage/' ),
				'thumbnail' => $image_dir . 'home-blog-preview.jpg',
				'title'     => 'Agency',
			),
			array(
				'url'       => home_url( '/homepage/shop-01/' ),
				'thumbnail' => $image_dir . 'home-shop-01-preview.jpg',
				'title'     => 'Shop 01',
			),
			array(
				'url'       => home_url( '/homepage/shop-02/' ),
				'thumbnail' => $image_dir . 'home-shop-02-preview.jpg',
				'title'     => 'Shop 02',
			),
			array(
				'url'       => home_url( '/homepage/product/' ),
				'thumbnail' => $image_dir . 'home-product-preview.jpg',
				'title'     => 'Product',
			),
			array(
				'url'       => home_url( '/homepage/portfolio-homepage/' ),
				'thumbnail' => $image_dir . 'home-portfolio-preview.jpg',
				'title'     => 'Portfolio',
			),
		);
		?>
		<div class="tm-demo-options-wrapper">
			<div class="tm-demo-options-toolbar">
				<a id="toggle-quick-options" href="#" class="hint--bounce hint--left"
				   aria-label="<?php echo esc_attr__( 'Quick Options', 'leomes' ); ?>">
					<i class="ion-ios-gear"></i>
				</a>
				<a href="<?php echo esc_url( $support_link ); ?>"
				   target="_blank"
				   class="hint--bounce hint--left"
				   aria-label="<?php echo esc_attr__( 'Support Center', 'leomes' ); ?>">
					<i class="ion-help-buoy"></i>
				</a>
				<a href="<?php echo esc_url( $documentation_link ); ?>"
				   target="_blank"
				   class="hint--bounce hint--left"
				   aria-label="<?php echo esc_attr__( 'Documentation', 'leomes' ); ?>">
					<i class="ion-document-text"></i>
				</a>
				<a href="<?php echo esc_url( $purchase_link ); ?>"
				   target="_blank"
				   class="hint--bounce hint--left"
				   aria-label="<?php echo esc_attr__( 'Purchase Leomes', 'leomes' ); ?>">
					<i class="ion-ios-cart"></i>
				</a>
			</div>
			<div id="tm-demo-panel" class="tm-demo-panel">
				<div class="tm-demo-panel-header">
					<a class="tm-button style-outline tm-button-sm tm-button-secondary tm-btn-purchase has-icon icon-left"
					   href="<?php echo $purchase_link; ?>" target="_blank">
						<span class="button-icon">
							<i class="ion-android-cart"></i>
						</span>
						<span class="button-text">
							<?php esc_html_e( 'Buy Leomes Now' ); ?>
						</span>
					</a>
					<div class="demo-option-desc">
						<?php echo $theme_description; ?>
					</div>
				</div>
				<div class="quick-option-list">
					<?php foreach ( $links as $link ) : ?>
						<a href="<?php echo $link['url']; ?>" target="_blank"
						   class="hint--bounce hint--top"
						   aria-label="<?php echo esc_html( $link['title'] ); ?>"
						>
							<img src="<?php echo $link['thumbnail']; ?>" alt="<?php echo $link['title']; ?>" width="140" height="168"/>
						</a>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<script type='text/javascript'>
            jQuery( document ).ready( function ( $ ) {
                'use strict';
                $( '#toggle-quick-options' ).on( 'click', function ( e ) {
                    e.preventDefault();
                    $( this ).parents( '.tm-demo-options-wrapper' ).toggleClass( 'open' );
                } );
            } );
		</script>
		<?php
	}
}

new Insight_Child();
