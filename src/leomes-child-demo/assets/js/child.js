jQuery( document ).ready( function( $ ) {
	'use strict';

	$( '#toggle-quick-options' ).on( 'click', function( e ) {
		e.preventDefault();
		$( this ).parents( '.tm-demo-options-wrapper' ).toggleClass( 'open' );
	} );
} );
