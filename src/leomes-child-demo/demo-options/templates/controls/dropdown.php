<script type="text/template" id="do-control-dropdown-template">
	<label><%= label %></label>
	<select>
		<% for ( key in choices ) { %>
			<option value="<%= key %>"><%= choices[ key ] %></option>
		<% } %>
	</select>
</script>