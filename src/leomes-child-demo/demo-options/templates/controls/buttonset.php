<script type="text/template" id="do-control-buttonset-template">
	<label><%= label %></label>
	<ul class="do-buttonset">
		<% for ( key in choices ) { %>
			<li><a href="#" class="do-button <% console.log(value, key); if (key == value) { %>active<% } %>" data-value="<%= key %>"><%= choices[ key ] %></a></li>
		<% } %>
	</ul>
</script>
