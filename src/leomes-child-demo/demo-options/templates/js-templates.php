<script type="text/template" id="do-section-template">
	<h3 data-toggle="collapse" data-parent="#do-section-list" data-target="#do-section-<%= id %>" aria-expanded="false">
		<%= label %></h3>
	<div class="control-list collapse" id="do-section-<%= id %>">
	</div>
</script>

<script type="text/template" id="do-control-dropdown-template">
	<% if (label != '') { %>
	<label><%= label %></label>
	<% } %>
	<select>
		<% for ( key in choices ) { %>
		<option value="<%= key %>"
		<% if ( key === value ) { %>selected<% } %>><%= choices[ key ] %></option>
		<% } %>
	</select>
</script>

<script type="text/template" id="do-control-color-template">
	<label><%= label %></label>
	<input type="text" class="do-color-input"/>
</script>

<script type="text/template" id="do-control-buttonset-template">
	<% if (label != '') { %>
	<label><%= label %></label>
	<% } %>
	<ul class="do-buttonset">
		<% for ( key in choices ) { %>
		<li><a href="#" class="do-button <% if (key == value) { %>active<% } %>" data-value="<%= key %>"><%= choices[
				key ] %></a></li>
		<% } %>
	</ul>
	<div style="clear: both;"></div>
</script>

<script type="text/template" id="do-control-colorpattern-template">
	<% if (label != '') { %>
	<label><%= label %></label>
	<% } %>
	<ul class="do-pattern">
		<% for ( key in choices ) { %>
		<li><a href="#" class="do-button <% if (key == value) { %>active<% } %>" data-value="<%= key %>"
		       style="background-color: <%= key %>;"></a></li>
		<% } %>
	</ul>
	<div style="clear: both;"></div>
</script>

<script type="text/template" id="do-control-imagepattern-template">
	<% if (label != '') { %>
	<label><%= label %></label>
	<% } %>
	<ul class="do-pattern">
		<% for ( key in choices ) { %>
		<li><a href="<%= key %>" class="do-button <% if (key == value) { %>active<% } %>" data-value="<%= key %>" style="background-image: url('<%= choices[key] %>'); background-size: cover;"></a></li>
		<% } %>
	</ul>
	<div style="clear: both;"></div>
</script>
