<div id="do-container">
	<h3 class="do-header">DEMO OPTIONS</h3>
	<div class="do-section-list" id="do-section-list"></div>
	<a href="#" class="do-toggle-panel"><i class="fa fa-cog fa-spin"></i></a>
</div>
<?php
include( DEMO_OPTIONS_PATH . 'templates/js-templates.php' );
?>
<script>
	var do_sections = <?php echo json_encode( $settings ); ?>;
</script>
