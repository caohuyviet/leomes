<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Enqueue child scripts
 */
if ( ! function_exists( 'leomes_child_enqueue_scripts' ) ) {
	function leomes_child_enqueue_scripts() {
		$min = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG == true ? '' : '.min';

		wp_enqueue_style( 'insight-style', INSIGHT_THEME_URI . "/style{$min}.css" );
		//wp_enqueue_style( 'insight-child-style', get_stylesheet_directory_uri() . '/style.css', array( 'insight-style' ), wp_get_theme()->get( 'Version' ) );
	}
}
add_action( 'wp_enqueue_scripts', 'leomes_child_enqueue_scripts' );
