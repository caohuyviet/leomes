'use strict';
var $         = require( 'gulp-load-plugins' )(),
	mainTheme = require( './paths' ).mainTheme;

module.exports = function( error ) {
	$.notify( {
		title: mainTheme + ' | ' + error.plugin,
		subtitle: 'Failed!',
		message: 'See console for more info.',
		sound: true
	} ).write( error );

	$.util.log( $.util.colors.red( error.message ) );

	// Prevent the 'watch' task from stopping
	this.emit( 'end' );
};
