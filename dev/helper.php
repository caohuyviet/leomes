<?php
// Check post meta off all pages
add_action( 'init', 'test' );
function test() {
	$pages   = get_pages();
	if ( $pages ) {
		foreach ( $pages as $page ) {
			$post_options = unserialize( get_post_meta( $page->ID, 'insight_page_options', true ) );
			if ( $post_options !== false && isset( $post_options['custom_logo'] ) && $post_options['custom_logo'] !== '' ) {
				Insight_Helper::d( $page->post_title );
			}
			/*if ( strpos( $page->post_content, 'tm_product_categories' ) !== false) {
				Insight_Helper::d( $page->post_title );
			}*/
		}
	}
}
